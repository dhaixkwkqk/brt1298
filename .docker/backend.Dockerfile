FROM php:8.2.9-fpm-alpine

ENV PHP_MEMORY_LIMIT=512M
ENV PHP_UPLOAD_MAX_FILESIZE: 512M
ENV PHP_POST_MAX_SIZE: 512M


RUN apk update \
    && apk upgrade \
    && apk add --update  libpq-dev openssl gnupg ca-certificates zip unzip supervisor libpng-dev oniguruma-dev libxml2-dev libzip-dev icu-dev openldap-dev pcre-dev $PHPIZE_DEPS freetype-dev libpng-dev libjpeg-turbo-dev libxml2-dev autoconf g++ imagemagick-dev libtool make mpdecimal-dev \
    && docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg \
    && pecl install redis imagick decimal \
    && docker-php-ext-install pdo_mysql gd mbstring xml zip bcmath soap intl ldap gd \
    && docker-php-ext-enable pdo_mysql gd mbstring xml zip bcmath soap intl ldap redis imagick decimal \
    && apk del autoconf g++ libtool make \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apk/*


COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
