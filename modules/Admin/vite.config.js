import { defineConfig } from "vite"
import laravel from "laravel-vite-plugin"
import { fileURLToPath } from "node:url"
import vue from "@vitejs/plugin-vue"
import vueJsx from "@vitejs/plugin-vue-jsx"
import vuetify from "vite-plugin-vuetify"
import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import VueI18nPlugin from "@intlify/unplugin-vue-i18n/vite"
import DefineOptions from "unplugin-vue-define-options/vite"
import dotenv from "dotenv"
import dotenvExpand from "dotenv-expand"

dotenvExpand(dotenv.config({ path: "../../.env"/* , debug: true */ }))

export default defineConfig({
  build: {
    outDir: "../../public/build-admin",
    emptyOutDir: true,
    manifest: true,
    chunkSizeWarningLimit: 5000,
  },

  plugins: [
    laravel({
      hotFile: "../../storage/vite.hot",
      publicDirectory: "../../public",
      buildDirectory: "build-admin",
      input: `${__dirname}/Resources/ts/app.ts`,
      refresh: true,
    }),
    vue({
      template: {
        transformAssetUrls: {
          base: null,
          includeAbsolute: false,
        },
      },
    }),
    vueJsx(),
    vuetify({
      styles: {
        configFile: `${__dirname}/Resources/styles/variables/_vuetify.scss`,
      },
    }),
    Components({
      dirs: [
        `${__dirname}/Resources/ts/@core/components`,
        `${__dirname}/Resources/ts/views/demos`,
        `${__dirname}/Resources/ts/components`,
      ],
      dts: `${__dirname}/Resources/ts/types/components.d.ts`,
    }),
    AutoImport({
      imports: [
        "vue",
        "vue-router",
        "@vueuse/core",
        "@vueuse/math",
        "vue-i18n",
        "pinia",
        {
          "@inertiajs/vue3": ["Link", "useForm", "router", "usePage"],
        },
      ],
      vueTemplate: true,
      dts: `${__dirname}/Resources/ts/types/auto-imports.d.ts`,
    }),
    VueI18nPlugin({
      runtimeOnly: true,
      compositionOnly: true,
      include: [
        fileURLToPath(new URL(`${__dirname}/Resources/ts/plugins/i18n/locales/**`, import.meta.url)),
      ],
    }),
    DefineOptions(),
  ],
  define: { "process.env": {} },
  resolve: {
    alias: {
      "@core-scss": fileURLToPath(new URL(`${__dirname}/Resources/styles/@core`, import.meta.url)),
      "@": fileURLToPath(new URL(`${__dirname}/Resources/ts`, import.meta.url)),
      "@themeConfig": fileURLToPath(new URL(`${__dirname}/themeConfig.ts`, import.meta.url)),
      "@core": fileURLToPath(new URL(`${__dirname}/Resources/ts/@core`, import.meta.url)),
      "@layouts": fileURLToPath(new URL(`${__dirname}/Resources/ts/@layouts`, import.meta.url)),
      "@images": fileURLToPath(new URL(`${__dirname}/Resources/images/`, import.meta.url)),
      "@styles": fileURLToPath(new URL(`${__dirname}/Resources/styles/`, import.meta.url)),
      "@configured-variables": fileURLToPath(new URL(`${__dirname}/Resources/styles/variables/_template.scss`, import.meta.url)),
      "@axios": fileURLToPath(new URL(`${__dirname}/Resources/ts/plugins/axios`, import.meta.url)),
      "@validators": fileURLToPath(new URL(`${__dirname}/Resources/ts/@core/utils/validators`, import.meta.url)),
      "apexcharts": fileURLToPath(new URL("../../node_modules/apexcharts-clevision", import.meta.url)),
      "ziggy": fileURLToPath(new URL("../../vendor/tightenco/ziggy/dist/vue.es.js", import.meta.url)),
    },
  },
  optimizeDeps: {
    exclude: ["vuetify"],
    entries: [
      `${__dirname}/Resources/ts/**/*.vue`,
    ],
  },
  server: {
    host: true,
    hmr: {
      host: "localhost",
    },
    watch: {
      usePolling: true,
    },
  },
})
