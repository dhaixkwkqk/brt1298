export interface UserParams {
  start: number;
  length: number;
  search?: {
    value?: string;
    regex?: boolean;
  }
  order?: Record<number, {
    column?: number
    dir?: 'asc' | 'desc'
  }>
  columns?: Record<number, {
    data?: string
    name?: string
    searchable?: boolean
    orderable?: boolean
    search?: {
      value?: string
      regex?: boolean
    }
  }>
}
