import type { UnwrapNestedRefs } from "vue"

interface DatatableColumn {
  title?: string
  name: string
  manual?: boolean
  searchable?: boolean
  sortable?: boolean
}

interface DataTable {
  url: string
  params: UnwrapNestedRefs<{ search: string; refresh: number }>
  search?: UnwrapNestedRefs<Record<string, string>>
  columns: Array<DatatableColumn>
  defaultSort: Array<DataTableSort>
  refresh: () => void
}

interface DataTableSort {
  key: string
  order: "asc" | "desc"
}

type DatatableColumns = Array<DatatableColumn>
