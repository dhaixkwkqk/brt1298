import { breakpointsVuetify } from "@vueuse/core"
import { AppContentLayoutNav, ContentWidth, FooterType, NavbarType } from "@layouts/enums"
import type { Config } from "@layouts/types"
import { VIcon } from "vuetify/components/VIcon"
import logo from "@images/logo.svg?raw"

export const config: Config = {
  app: {
    title: "b2b request",
    logo: h("div", { innerHTML: logo, style: "line-height:0; color: rgb(var(--v-global-theme-primary))" }),
    contentWidth: ref(ContentWidth.Boxed),
    contentLayoutNav: ref(AppContentLayoutNav.Vertical),
    overlayNavFromBreakpoint: breakpointsVuetify.md,
    enableI18n: true,
    isRtl: ref(false),
    iconRenderer: VIcon,
  },
  navbar: {
    type: ref(NavbarType.Sticky),
    navbarBlur: ref(true),
  },
  footer: { type: ref(FooterType.Static) },
  verticalNav: {
    isVerticalNavCollapsed: ref(false),
    defaultNavItemIconProps: { icon: "tabler-circle" },
  },
  horizontalNav: {
    type: ref("sticky"),
  },
  icons: {
    chevronDown: { icon: "tabler-chevron-down" },
    chevronRight: { icon: "tabler-chevron-right" },
    close: { icon: "tabler-x" },
    verticalNavPinned: { icon: "tabler-circle-dot" },
    verticalNavUnPinned: { icon: "tabler-circle" },
    sectionTitlePlaceholder: { icon: "tabler-minus" },
  },
}
