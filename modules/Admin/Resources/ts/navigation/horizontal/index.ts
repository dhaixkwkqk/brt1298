import type { HorizontalNavItems } from "@/@layouts/types"
import route from "ziggy-js"

export default [
  {
    title: "Pages",
    icon: { icon: "tabler-file" },
    children: [
      {
        title: "Dashboard",
        icon: { icon: "tabler-dashboard" },
        to: route("admin"),
      },
      {
        title: "Admins",
        icon: { icon: "tabler-user-star" },
        to: route("admin.assistant.index"),
      },
      {
        title: "Clients",
        icon: { icon: "tabler-users" },
        to: route("admin.client.index"),
      },
      {
        title: "Tariffs",
        icon: { icon: "tabler-coin" },
        to: route("admin.tariff.index"),
      },
      {
        title: "Locales",
        icon: { icon: "tabler-language" },
        to: route("admin.locale.index"),
      },
      {
        title: "Translations",
        icon: { icon: "tabler-world" },
        to: route("admin.translation.index"),
      },
      {
        title: "Pages",
        icon: { icon: "tabler-files" },
        to: route("admin.page.index"),
      },
    ],
  },
] as HorizontalNavItems
