import type { VerticalNavItems } from "@/@layouts/types"
import route from "ziggy-js"

export default [
  { heading: "Navigation" },
  {
    title: "Clients",
    icon: { icon: "tabler-users" },
    to: route("admin.client.index"),
  },
  {
    title: "Admins",
    icon: { icon: "tabler-user-star" },
    to: route("admin.assistant.index"),
  },
  {
    title: "Tariffs",
    icon: { icon: "tabler-coin" },
    to: route("admin.tariff.index"),
  },
  {
    title: "Locales",
    icon: { icon: "tabler-language" },
    to: route("admin.locale.index"),
  },
  // {
  //   title: "Translations",
  //   icon: { icon: "tabler-world" },
  //   to: route("admin.translation.index"),
  // },
  {
    title: "Pages",
    icon: { icon: "tabler-files" },
    to: route("admin.page.index"),
  },
  {
    title: "Notifications",
    icon: { icon: "tabler-bell" },
    to: route("admin.notification.index"),
  },
  {
    title: "Customer requests",
    icon: { icon: "tabler-circuit-resistor" },
    to: route("admin.requests.index"),
  },
  {
    title: "Settings",
    icon: { icon: "tabler-settings" },
    to: route("admin.setting.index", { section: "agent" }),
  },
] as VerticalNavItems
