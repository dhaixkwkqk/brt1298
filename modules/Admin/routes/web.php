<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Modules\Admin\Http\Controllers\AssistantController;
use Modules\Admin\Http\Controllers\ClientController;
use Modules\Admin\Http\Controllers\ClientRequestsController;
use Modules\Admin\Http\Controllers\DashboardController;
use Modules\Admin\Http\Controllers\LocaleController;
use Modules\Admin\Http\Controllers\NotificationController;
use Modules\Admin\Http\Controllers\PageController;
use Modules\Admin\Http\Controllers\SettingController;
use Modules\Admin\Http\Controllers\TariffController;
use Modules\Admin\Http\Controllers\TranslationController;
use Modules\Admin\Http\Controllers\LocalTranslatorController;

Route::get('/admin/login', function (){
    if (Auth::check()) {
        return redirect()->back();
    }else{
        return Inertia::render('Auth/Login');
    }
} )->name('admin.login');

Route::prefix('admin')
    ->name('admin.')
    ->middleware(['admin.only', 'auth.admin.redirect:sanctum'])
    ->group(function () {
        Route::get('/', [ClientController::class, 'index']);

        Route::post('/locale', [LocaleController::class, 'datatable']);


        Route::get('/locale/edit/datatable', [LocalTranslatorController::class, 'datatable'])
            ->name('locale.edit.datatable');

        Route::get('/locale/edit/{id}/', [LocalTranslatorController::class, 'index']);
        Route::patch('/locale/edit/update/', [LocalTranslatorController::class, 'update']);

        Route::get('/assistant/datatable', [AssistantController::class, 'datatable'])->name('assistant.datatable');
        Route::resource('assistant', AssistantController::class);

        Route::get('/client/datatable', [ClientController::class, 'datatable'])->name('client.datatable');
        Route::resource('client', ClientController::class);

        Route::get('/requests/datatable', [ClientRequestsController::class, 'datatable'])->name('requests.datatable');
        Route::resource('/requests', ClientRequestsController::class);

        Route::get('/tariff/datatable', [TariffController::class, 'datatable'])->name('tariff.datatable');
        Route::resource('tariff', TariffController::class);
        Route::post('/tariff/{id}', [TariffController::class, 'activeTariff'])->name('tariff.active');

        Route::get('/locale/datatable', [LocaleController::class, 'datatable'])->name('locale.datatable');
        Route::resource('locale', LocaleController::class);

//        Route::resource('translation', TranslationController::class);
//        Route::get('/translation/datatable', [TranslationController::class, 'datatable'])->name('translation.datatable');

        Route::get('/page/datatable', [PageController::class, 'datatable'])->name('page.datatable');
        Route::resource('page', PageController::class);

        Route::get('/setting/{section}', [SettingController::class, 'index'])
            ->name('setting.index');
        Route::post('/setting', [SettingController::class, 'save'])
            ->name('setting.save');

        Route::get('/notification/datatable', [NotificationController::class, 'datatable'])
            ->name('notification.datatable');
        Route::get('/notification', [NotificationController::class, 'index'])
            ->name('notification.index');
        Route::post('/notification', [NotificationController::class, 'store'])
            ->name('notification.store');
        Route::delete('/notification/{id}', [NotificationController::class, 'destroy'])
            ->name('notification.destroy');
    });

Route::fallback(fn() => Inertia::render('404'));
