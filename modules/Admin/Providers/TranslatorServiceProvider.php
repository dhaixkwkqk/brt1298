<?php

namespace Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Admin\Services\Contracts\TranslatorServiceContract;
use Modules\Admin\Services\TranslatorService;

class TranslatorServiceProvider extends ServiceProvider
{
    public $bindings = [
        TranslatorServiceContract::class => TranslatorService::class,
    ];
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind('Modules\Admin\Services\TranslatorService', function ($app) {
            return new TranslatorService();
        });

    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
