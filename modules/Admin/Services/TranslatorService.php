<?php


namespace Modules\Admin\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use Modules\Admin\Services\Contracts\TranslatorServiceContract;

class TranslatorService implements TranslatorServiceContract
{
    const staticPath = 'modules';
    const separator = '__';
    protected string $currentModule = 'Client';

    public function getTranslationLang($recipient, $code):array
    {
        $this->currentModule = $recipient;
        $lang_files = $this->getTranslationLangFile($code);

        $languages = $this->flattenArrayKeys($lang_files);

        $res = [];
        foreach ($languages as $key => $val){
            $index = strrpos($key, self::separator);
            $res[] = [
                'realName'  =>  $key,
                'name'  =>  substr($key, $index + strlen(self::separator)),
                'word'  =>  $val,
            ];
        }
        return $res;
    }

    private function flattenArrayKeys($array, $separator = self::separator) :array
    {
        $result = [];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $nestedKeys = $this->flattenArrayKeys($value, $separator);
                foreach ($nestedKeys as $nestedKey => $nestedValue) {
                    $result[$key . $separator . $nestedKey] = $nestedValue;
                }
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    private function getTranslationLangFile($code):object
    {
        $path = base_path(self::staticPath."/".$this->currentModule);
        $langPath = $path.'/lang/'.$code.".json";
        if(!File::exists($path."/lang")){
            File::makeDirectory($path."/lang");
        }
        if(!File::exists($langPath)){
            //перемещаю дефолтный с именем кода
            File::copy($path.'/lang/en.json', $langPath);
        }
//        стягиваю файл и ретерню
        return collect($this->getFileByPath($langPath));
    }

    public function updateJson($jsonData): array
    {
        $res = [
            'result'    =>  false,
        ];
        if($jsonData['type'] == "update"){
            $res['result'] = $this->setNewValue($jsonData['key'], $jsonData['value'], $jsonData['lang']['code']);
        }else{
            $res['result'] = $this->deleteKeyFromJson($jsonData['key'], $jsonData['value'], $jsonData['lang']['code']);
        }
        return $res;
    }

    private function deleteKeyFromJson($key, $value, $code){
        $path = base_path(self::staticPath."/".$this->currentModule."/lang/".$code.'.json');
        $file = File::json($path);
        $keys = explode(self::separator, $key);
        $newJson = $this->deleteKeyByPath($file, $keys);
        if(!empty($newJson)){
            $res = File::put($path,json_encode($newJson));
            if($res){
                return 'success delete';
            }
        }else{
            return 'error delete';
        }
    }


    private function setNewValue($key, $value, $code)
    {
        $path = base_path(self::staticPath."/".$this->currentModule."/lang/".$code.'.json');
        $file = File::json($path);
        $keys = explode(self::separator, $key);
        $newJson = $this->setValueInArray($file, $keys, $value);
        if(!empty($newJson)){
            $res = File::put($path,json_encode($newJson));
            if($res){
                return 'success update';
            }
        }else{
            return 'error update';
        }
    }

    private function deleteKeyByPath(&$array, $keysList) {
        $currentArray = &$array;
        $lastKey = array_pop($keysList);

        foreach ($keysList as $key) {
            if (!isset($currentArray[$key]) || !is_array($currentArray[$key])) {
                // Key not found or is not an array, nothing to delete
                return 'not found key';
            }

            // Move to the next level in the array
            $currentArray = &$currentArray[$key];
        }

        // Check if the last key exists in the final level of the array and delete it
        if (isset($currentArray[$lastKey])) {
            unset($currentArray[$lastKey]);
        }

        return  $array;
    }

    private function setValueInArray($array, $keysList, $value){
        $currentArray = &$array;

        foreach ($keysList as $key) {
            if (!isset($currentArray[$key])) {
                // Key not found, create an empty array for the missing key
                $currentArray[$key] = [];
            }

            // Move to the next level in the array
            $currentArray = &$currentArray[$key];
        }

        // Assign the new value to the final key in the path
        $currentArray = $value;

        return $array;
    }

    private function findValueInArray($array, $keysList) {
        $currentArray = $array;

        foreach ($keysList as $key) {
            if (isset($currentArray[$key])) {
                $currentArray = $currentArray[$key];
            } else {
                // Key not found in the current array
                return null;
            }
        }

        return $currentArray;
    }

    private function getFileByPath($path): array
    {
        return File::json($path);
    }

    public function deleteLang($recipient, $code){
        File::delete(base_path(self::staticPath."/".$recipient."/lang/".$code.".json"));
    }
}
