<?php
namespace Modules\Admin\Services\Contracts;

interface TranslatorServiceContract {

    public function getTranslationLang($recipient, $code): array;
    public function deleteLang($recipient, $code);
    public function updateJson($jsonData): array;
}
