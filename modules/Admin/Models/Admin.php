<?php

namespace Modules\Admin\Models;

use App\Models\User;
use Parental\HasParent;

class Admin extends User
{
    use HasParent;
}
