<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Yajra\DataTables\Facades\DataTables;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Pages->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index()
    {
        return Inertia::render('Page/Index');
    }

    public function datatable()
    {
        $query = Page::query();

        return DataTables::eloquent($query)
            ->toArray();
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => ['required'],
            'description' => ['required'],
            'show' => ['required', 'boolean'],
        ]);

        Page::create($validated);

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => ['required'],
            'description' => ['required'],
            'show' => ['required', 'boolean'],
        ]);


        $Page = Page::findOrFail($id);
        $Page->update($validated);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $Page = Page::destroy($id);

        return redirect()->back();
    }
}
