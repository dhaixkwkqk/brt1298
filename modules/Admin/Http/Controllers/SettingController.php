<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\SettingItem;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Settings->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index($section)
    {
        $settings = SettingItem::whereSection($section)
            ->orderBy('position')
            ->get();

        $section = Str::title($section);

        return Inertia::render('Setting/Index', compact('section', 'settings'));
    }

    public function save(Request $request)
    {
        $validated = $request->validate([
            'settings' => ['required', 'array'],
            'settings.*' => ['nullable'],
        ]);

        foreach ($validated['settings'] as $key => $value) {
            try {
                $setting = SettingItem::whereName($key)->firstOrFail();

                switch ($setting->options['type'] ?? null) {
                    case 'number':
                        $value = intval($value);
                        if (($setting['options']['min'] ?? null) && $value < $setting['options']['min']) {
                            throw new \Exception('Минимальное значение '.$setting['options']['min']);
                        }
                        if (($setting['options']['max'] ?? null) && $value > $setting['options']['max']) {
                            throw new \Exception('Максимальное значение '.$setting['options']['max']);
                        }
                        break;

                    case 'boolean':
                        $value = $value ? 1 : 0;
                        break;
                }

                $setting->update(['value' => $value]);
            } catch (\Exception $e) {
                throw ValidationException::withMessages([
                    'settings.'.$key => $e->getMessage()
                ]);
            }
        }

        return redirect()
            ->back()
            ->with('success', 'Настройки успешно сохранены!');
    }
}
