<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\Tariff;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Unique;
use Inertia\Inertia;
use Modules\Client\Models\Client;
use Yajra\DataTables\Facades\DataTables;


class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Clients->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index()
    {
        $clientID = Str::uuid()->toString();
        $accessKey = Str::random(32);
        $login = fake()->name();

        $allTariffs = Tariff::all(['id', 'name', 'active'])->where('active',true);

        $allTariffsSel = [];
        foreach ($allTariffs as $value){
            $allTariffsSel[] = [
                'name'  =>  $value['name'],
                'value'  =>  $value['id'],
            ];
        }


        return Inertia::render(
            'Client/Index',
            compact(
                'clientID',
                'accessKey',
                'login',
                'allTariffs',
                'allTariffsSel',
            )
        );
    }

    public function datatable()
    {
        $query = Client::with(['tariff']);

        return DataTables::eloquent($query)
            ->editColumn(
                'tariff',
                fn(Client $client) => $client->tariff ? $client->tariff->only(['name', 'price']) : null
            )
            ->toArray();
    }

    public function store(Request $request)
    {

        $validated = $request->validate([
            'name' => ['required', 'string'],
            'client_id' => ['required', 'string', new Unique(Client::class)],
            'access_key' => ['required', 'string'],
            'tariff_id' => ['required', 'exists:tariffs,id']
        ]);

        Client::create([
            ...$validated,
            'client_id' => $validated['client_id'],
            'access_key' => Hash::make($validated['access_key']),
        ]);

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        $validated = $request->validate([
            'name' => ['required', 'string'],
            'username' => ['nullable', 'string'],
            'password' => ['nullable', 'string'],
            'tariff_id' => ['required', 'exists:tariffs,id']
        ]);

        $client->name = $validated['name'];
        $client->tariff_id = $validated['tariff_id'];

        if (($validated['username'] ?? null) || ($validated['password'] ?? null)) {
            $client->client_id = null;
            $client->access_key = null;
        }

        if ($validated['password'] ?? null) {
            $client->password = Hash::make($validated['password']);
        }

        $client->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        Client::find($id)?->delete();

        return redirect()->back();
    }
}
