<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Modules\Client\Models\Client;
use Yajra\DataTables\Facades\DataTables;

class ClientRequestsController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Requests->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render(
            'Requests/Index'
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    public function datatable()
    {
        $query = Company::query()->where('status','inactive')
            ->orWhere('status','decline');

        return DataTables::eloquent($query)
            ->editColumn(
                'user',
                fn(Company $company) => $company->user ? $company->user->only(['name']) : null
            )
            ->editColumn(
                'tariff',
                fn(Company $company) => $company->tariff ? $company->tariff->only(['name','price']) : null
            )
            ->toArray();
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id)
    {
        $company = Company::with('tariff')->where([
            'id'  =>  $id
        ])->first();

        return Inertia::render(
            'Requests/Detail',
            compact('company')
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validated = $request->validate([
            'status' => ['required', 'string'],
        ]);
        $update = Company::where('id', $id)->update($validated);

        if($update){
            return response()->json(['message' => 'Company updated successfully', 'status' => true]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
