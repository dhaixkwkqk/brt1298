<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\NotificationMessage;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Modules\Client\Models\Client;
use Yajra\DataTables\Facades\DataTables;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Notifications->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }
    public function index()
    {
        $clients = Client::get()
            ->map(fn(Client $client) => [
                'name' => $client->name.' ('.$client->username.')',
                'value' => $client->id,
            ])
            ->all();

        return Inertia::render('Notification/Index', compact('clients'));
    }

    public function datatable(Request $request)
    {
        $query = Notification::with('message', 'recipient');

        return DataTables::eloquent($query)
            ->toArray();
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'clients' => ['required', 'array'],
            'message' => ['required', 'string'],
        ]);

        $message = NotificationMessage::create([
            'creator_id' => $request->user()->id,
            'text' => trim($validated['message']),
        ]);

        foreach( $validated['clients'] as $clientId ) {
            Notification::create([
                'recipient_id' => $clientId,
                'message_id' => $message->id,
            ]);
        }

        return redirect()->back();
    }
    public function destroy($id){
        NotificationMessage::find($id)?->delete();
    }
}
