<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Unique;
use Inertia\Inertia;
use Modules\Admin\Services\Contracts\TranslatorServiceContract;
use Modules\Admin\Services\TranslatorService;
use Yajra\DataTables\Facades\DataTables;

class LocalTranslatorController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Translations->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index($locale)
    {

        $locale = Locale::with('lang')->where('id',$locale)->get()->first();

        return Inertia::render('Locale/Translator/Index', compact('locale'));
    }

    /**
     * @throws \Exception
     */
    public function datatable(Request $request, TranslatorServiceContract $service)
    {
        $validated = $request->validate([
            'locale_id' => ['required', 'exists:locales,id'],
        ]);

        $lang = Locale::with('lang')->where('id',$validated['locale_id'])->get()->first();
        $langs = $service->getTranslationLang($lang->current_recipient, $lang->lang->code);
        return DataTables::of($langs)->toJson();

    }

    public function update(Request $request, TranslatorServiceContract $service)
    {
        $validated = $request->validate([
            'locale_id' => ['required', 'exists:locales,id'],
        ]);

        $lang = Locale::query()->with(['lang' => function ($query) {
            $query->select('id','code');
        }])->where('id',$validated['locale_id'])->get();

        $result = $service->updateJson([...$request->all() , ...$lang->first()->toArray()]);
        return $result;
    }

}
