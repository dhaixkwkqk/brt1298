<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\Locale;
use App\Models\Translation;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Yajra\DataTables\Facades\DataTables;

class TranslationController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Translations->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index()
    {
        $locales = Locale::all();

        return Inertia::render('Translation/Index', compact('locales'));
    }

    public function datatable()
    {
        $query = Translation::query();

        return DataTables::eloquent($query)
            ->toArray();
    }

    public function store(Request $request)
    {
        $locales = Locale::all();

        $rules = [
            'group' => ['required', 'string'],
            'key' => ['required', 'string'],
        ];

        foreach ($locales as $locale) {
            $rules['text_'.$locale->name] = ['required', 'string'];
        }

        $validated = $request->validate($rules);

        $validated['text'] = [];

        foreach ($locales as $locale) {
            $validated['text'][$locale->name] = $validated['text_'.$locale->name];
            unset($validated['text_'.$locale->name]);
        }

        Translation::create($validated);

        return redirect()->back();
    }

    public function destroy($id)
    {
        Translation::find($id)?->delete();

        return redirect()->back();
    }
}
