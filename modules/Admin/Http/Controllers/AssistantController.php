<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\Rules\Unique;
use Inertia\Inertia;
use Modules\Admin\Models\Admin;
use Yajra\DataTables\Facades\DataTables;

class AssistantController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Admins->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index()
    {
        $adminPermissions = AdminPermission::getAll();

        return Inertia::render('Assistant/Index', compact('adminPermissions'));
    }

    public function datatable()
    {
        $query = Admin::query();
        $data = DataTables::eloquent($query)->addColumn('adminIsOnline', function ($query){
            if (Cache::has('admin-is-online-' . $query->id)){
                return true;
            }else{
                return false;
            }
        })->toArray();

        return $data;
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', new Unique(User::class)],
            'password' => ['required', 'string'],
            'admin_permissions' => [
                'required',
                'array',
                new In(array_map(fn(AdminPermission $item) => $item->value, AdminPermission::cases()))
            ],
        ]);

        Admin::create([
            ...$validated,
            'password' => Hash::make($validated['password']),
            'parent_id' => $request->user()->id,
        ]);

        return redirect()
            ->back()
            ->with('success', 'sex');
    }

    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);

        $validated = $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', (new Unique(Admin::class))->ignoreModel($admin)],
            'password' => ['nullable', 'string'],
            'admin_permissions' => ['nullable', 'array'],
        ]);

        $admin->fill([
            'name' => $validated['name'],
            'email' => $validated['email'],
        ]);

        if ($admin->parent_id) {
            $admin->admin_permissions = $validated['admin_permissions'] ?? [];
        }

        if ($validated['password'] ?? null) {
            $admin->password = Hash::make($validated['password']);
        }

        $admin->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        Admin::find($id)?->delete();

        return redirect()->back();
    }
}
