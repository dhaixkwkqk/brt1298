<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Http\Controllers\Controller;
use App\Models\Tariff;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\DatabaseRule;
use Illuminate\Validation\Rules\Unique;
use Inertia\Inertia;
use Yajra\DataTables\Facades\DataTables;

class TariffController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Tariffs->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index()
    {
        return Inertia::render('Tariff/Index');
    }

    public function datatable()
    {
        $query = Tariff::query();

        return DataTables::eloquent($query)
            ->editColumn('price', fn(Tariff $tariff) => $tariff->price->toString())
            ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
    }

    public function activeTariff(Request $request , $id){
        $validated = $request->validate([
            'currentActive' => ['required', 'integer'],
        ]);
        $tariff = Tariff::where('id', $id)->firstOrFail();
        $tariff->active = $validated['currentActive'];
        $res = $tariff->update($validated);
        return response()->json([
            'result'    =>  $res
        ]);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string', new Unique(Tariff::class)],
            'price' => ['required', 'decimal:0,2', 'min:0.01'],
            'period' => ['required', 'integer', 'min:1'],
            'users' => ['required', 'integer'],
            'fields' => ['required', 'integer'],
        ]);

        Tariff::create($validated);

        return redirect()->back();
    }

    /**
     * Show the specified resource.
     * @param  int  $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $tariff = Tariff::where('id', $id)->firstOrFail();

        $validated = $request->validate([
            'name' => ['required', 'string', new Unique(Tariff::class)],
            'price' => ['required', 'decimal:0,2', 'min:0.01'],
            'period' => ['required', 'integer', 'min:1'],
            'users' => ['required', 'integer'],
            'fields' => ['required', 'integer'],
        ]);

        $tariff->update($validated);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     */
    public function destroy($id)
    {
        Tariff::find($id)->delete();

        return redirect()->back();
    }
}
