<?php

namespace Modules\Admin\Http\Controllers;

use App\Enums\AdminPermission;
use App\Enums\RecipietsTranslations;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Unique;
use Inertia\Inertia;
use Modules\Admin\Services\Contracts\TranslatorServiceContract;
use Modules\Admin\Services\TranslatorService;
use Yajra\DataTables\Facades\DataTables;


class LocaleController extends Controller
{
    public function __construct()
    {
        $this->middleware(function (Request $request, \Closure $next) {
            if ($request->user()?->parent_id && !in_array(
                    AdminPermission::Locales->value,
                    $request->user()?->admin_permissions ?? []
                )) {
                return redirect()->route('admin.');
            }

            return $next($request);
        });
    }

    public function index()
    {
        $languagesList = Language::all(['id', 'description'])->toArray();
        $recipietsList = RecipietsTranslations::getAll();

        return Inertia::render('Locale/Index', compact('recipietsList', 'languagesList'));
    }

    public function getAllLanguages(){
        return response()->json([
            'languages'    =>  Language::all(['id', 'description'])->toArray()
        ]);
    }

    public function datatable()
    {
        $query = Locale::with(['lang']);

        return DataTables::eloquent($query)
            ->editColumn(
                'lang',
                fn(Locale $locale) => $locale->lang ? $locale->lang->only(['code', 'description']) : null
            )
            ->toArray();
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'language_id' => ['required', 'exists:languages,id'],
            'is_default' => ['nullable', 'boolean'],
            'current_recipient' => ['required'],
        ]);
        if ($validated['is_default'] ?? null) {
            Locale::whereIsDefault(true)->update(['is_default' => false]);
        }

        Locale::create($validated);

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'is_default' => ['nullable', 'boolean'],
            'current_recipient' => ['required'],
        ]);

        if ($validated['is_default'] ?? null) {
            Locale::whereIsDefault(true)->update(['is_default' => false]);
        }

        $locale = Locale::findOrFail($id);
        $locale->update($validated);

        return redirect()->back();
    }

    public function destroy($id, TranslatorServiceContract $service)
    {
        $locale = Locale::with('lang')->where('id',$id)->get()->first();
        //удаляю файл с переводами
        $service->deleteLang($locale->current_recipient, $locale->lang->code);
        //удаляю локаль
        Locale::find($id)?->delete();

        return redirect()->back();
    }
}
