<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Models\Admin;

class AdminOnlyMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if (!($user instanceof Admin)) {
            Auth::guard('web')->logout();
            throw new AuthorizationException('Admin zone');
        }else{
            return $next($request);
        }

    }
}
