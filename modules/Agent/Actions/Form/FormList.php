<?php

namespace Modules\Agent\Actions\Form;

use Modules\Agent\Models\Agent;
use Modules\Agent\Http\Resources\FormCollection;

class FormList
{
    public function __construct(protected readonly Agent $agent)
    {
    }

    public function run(): FormCollection
    {
        $forms = [];

        $companies = $this->agent->companies()->with('forms.fields')->get();
        foreach ($companies as $company) {
            foreach ($company->forms as $form) {
                if ($form->active) {
                    $forms[] = $form;
                }
            }
        }

        return new FormCollection($forms);
    }
}
