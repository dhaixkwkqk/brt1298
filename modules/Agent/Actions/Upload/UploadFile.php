<?php

namespace Modules\Agent\Actions\Upload;

use Modules\Agent\Models\Agent;
use Illuminate\Support\Facades\Date;
use Modules\Agent\Http\Requests\Upload\UploadRequest;
use Modules\Agent\Http\Resources\UploadCollection;

readonly class UploadFile
{
    public function __construct(protected Agent $agent)
    {
    }

    public function run(UploadRequest $request): UploadCollection
    {
        $fileEncrypted = $request->file('file_encrypted');
        $checksum = $request->get('checksum');
        $dirPath = 'agent-'.$this->agent->id.'-'.$this->agent->username.'/'.uniqid();

        $fileName = $fileEncrypted->getClientOriginalName();
        if (mb_substr($fileName, -4) !== '.pgp') {
            $fileName .= '.pgp';
        }

        $upload = $this->agent
            ->uploads()
            ->whereChecksum($checksum)
            ->first();
        if ($upload) {
            if ($upload->status !== 1) {
                $path = $fileEncrypted->storePubliclyAs($dirPath, $fileName, ['disk' => 'public']);

                $upload->update([
                    'path' => $path,
                    'expired_at' => Date::now()->addHour(),
                    'status' => 1,
                ]);
            } elseif ($upload->expired_at) {
                $upload->update([
                    'expired_at' => Date::now()->addHour(),
                ]);
            }
        } else {
            $path = $fileEncrypted->storePubliclyAs($dirPath, $fileName, ['disk' => 'public']);

            $upload = $this->agent->uploads()->create([
                'checksum' => $checksum,
                'disk' => 'public',
                'path' => $path,
                'expired_at' => Date::now()->addHour(),
                'status' => 1,
            ]);
            $upload->save();
        }

        return new UploadCollection([$upload]);
    }
}
