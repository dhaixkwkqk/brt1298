<?php

namespace Modules\Agent\Actions\FormRequest;

use App\Models\FormRequest\FormRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Date;

class FormRequestBeforeCreate
{
    public function run(): array
    {
        $min = 10000;
        $max = 999999;
        $i = 0;

        do {
            $i++;
            $max = $max * $i;
            $uid = 'F' . mt_rand($min, $max);

        } while (FormRequest::whereUid($uid)->count() > 0 || Cache::has('FormRequest_' . $uid));

        Cache::set('FormRequest_' . $uid, true, 3 * 60 * 60);

        return [
            'uid' => $uid,
        ];
    }
}
