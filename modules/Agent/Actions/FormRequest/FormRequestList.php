<?php

namespace Modules\Agent\Actions\FormRequest;

use Modules\Agent\Models\Agent;
use App\Models\FormRequest\FormRequest;
use Modules\Agent\Http\Requests\FormRequest\ListRequest;
use Modules\Agent\Http\Resources\FormRequestCollection;


readonly class FormRequestList
{
    public function __construct(protected Agent $agent, protected ListRequest $request)
    {
    }

    public function run(): FormRequestCollection
    {
        $query = $this->agent
            ->formRequests()
            ->with(['form.fields', 'chat']);

        // Фильтр по статусу
        $filterStatus = $this->request->validated('filter.status');
        if ($filterStatus && $filterStatus !== 'all') {
            $filterStatusIndex = array_search($filterStatus, FormRequest::$statuses);
            $query->whereStatus($filterStatusIndex);
        }

        return new FormRequestCollection(
            $query->get()->all()
        );
    }
}
