<?php

namespace Modules\Agent\Actions\FormRequest;

use Modules\Agent\Models\Agent;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMember;
use App\Models\Form\Form;
use App\Models\FormRequest\FormRequest;
use App\Services\Element\ValidatorInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Modules\Agent\Http\Requests\FormRequest\CreateRequest;
use Modules\Agent\Http\Resources\FormRequestCollection;
use Modules\Client\Models\Client;

class FormRequestCreate
{
    protected function createChat(Agent $agent, Client $client): Chat
    {
        $chat = Chat::create([
            'uid' => Str::uuid()->toString(),
        ]);

        $chatMember = new ChatMember();
        $chatMember->chat()->associate($chat);
        $chatMember->user()->associate($agent);
        $chatMember->save();

        $chatMember = new ChatMember();
        $chatMember->chat()->associate($chat);
        $chatMember->user()->associate($client);
        $chatMember->save();

        return $chat;
    }

    public function run(CreateRequest $request): FormRequestCollection
    {
        /** @var Form $form */
        $form = $request->getForm();
        $agent = $request->getAgent();

        $company = $agent->companies()->whereRelation('forms', 'id', $form->id)->first();
        $chat = $this->createChat($agent, $form->client);

        $formRequest = new FormRequest([
            'uid' => $request->validated('uid'),
            'fields_encrypted' => $request->validated('fields_encrypted'),
            'status' => FormRequest::STATUS_ACTIVE,
        ]);
        $formRequest->agent()->associate($agent);
        $formRequest->company()->associate($company);
        $formRequest->form()->associate($form);
        $formRequest->chat()->associate($chat);
        $formRequest->save();

        return new FormRequestCollection(
            FormRequest::whereId($formRequest->id)->with(['form.fields'])->get()->all()
        );
    }
}
