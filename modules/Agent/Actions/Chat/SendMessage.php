<?php

namespace Modules\Agent\Actions\Chat;

use App\Models\Chat\ChatMessage;
use Modules\Agent\Http\Requests\Chat\SendMessageRequest;
use Modules\Agent\Http\Resources\ChatMessageCollection;

class SendMessage
{
    public function __construct(protected readonly SendMessageRequest $request)
    {
    }

    public function run(): ChatMessageCollection
    {
        $chat = $this->request->getChat();
        $chatMember = $this->request->getChatMember();

        $chatMessage = new ChatMessage([
            'message_encrypted' => $this->request->validated('message_encrypted')
        ]);
        $chatMessage->chat()->associate($chat);
        $chatMessage->member()->associate($chatMember);
        $chatMessage->save();


        return new ChatMessageCollection([$chatMessage]);
    }
}
