<?php

namespace Modules\Agent\Models;

use App\Models\Chat\Chat;
use App\Models\Chat\ChatSupport;
use App\Models\Company\Company;
use App\Models\Form\Form;
use App\Models\FormRequest\FormRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Modules\Client\Models\Client;
use Parental\HasParent;

class Agent extends User
{
    use HasParent;

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'parent_id');
    }

    public function formRequests(): HasMany
    {
        return $this->hasMany(FormRequest::class);
    }

    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class, AgentCompany::class);
    }

    public function chatSupport(): HasOne
    {
        return $this->hasOne(ChatSupport::class);
    }

    public function chat(): HasOneThrough
    {
        return $this->hasOneThrough(Chat::class, ChatSupport::class, 'user_id', 'id', 'id', 'chat_id');
    }

    public function forms(): HasManyThrough
    {
        return $this->hasManyThrough(Form::class, FormRequest::class, 'user_id', 'id', 'id', 'form_id');
    }
}
