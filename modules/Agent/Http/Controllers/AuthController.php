<?php

namespace Modules\Agent\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\NewAccessToken;
use Modules\Agent\Http\Requests\Auth\LoginRequest;

class AuthController extends Controller
{
    /**
     * Метод API для авторизации пользователя в агентском приложении.
     * @param  LoginRequest  $request
     * @return true[]
     */
    public function login(LoginRequest $request)
    {
        // Попытка авторизации
        $request->authenticate();

        // Получение авторизированного агента
        $user = Auth::user();

        // Генерация токена (используемого для авторизации в API)
        /** @var NewAccessToken $token */
        $token = $user->createToken('API');

        return [
            'success' => true,
            'data' => [
                'token' => $token->plainTextToken,
            ]
        ];
    }

    /**
     * Метод API для выхода пользователя с авторизованного приложения.
     * @param  Request  $request
     * @return true[]
     */
    public function logout(Request $request)
    {
        // Удаляем токен (используемый для авторизации в API)
        $request->user()->currentAccessToken()->delete();

        return ['success' => true];
    }
}
