<?php

namespace Modules\Agent\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Exists;
use Modules\Agent\Models\Agent;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Agent\Actions\Upload\UploadFile;
use Modules\Agent\Http\Requests\Upload\UploadRequest;

class UploadController extends Controller
{
    /**
     * Метод загрузки зашифрованного файла.
     * @param  Agent  $agent
     * @param  UploadRequest  $request
     */
    public function index(Authenticatable $agent, UploadRequest $request): array
    {
        $uploadImage = new UploadFile($agent);

        return [
            'success' => true,
            'data' => $uploadImage->run($request)->collection->first(),
        ];
    }

    /**
     * Метод подтверждения загрузки файла.
     * Устанавливает метку что бы файл не был удалён сборщиком мусора.
     * @param  Agent  $agent
     */
    public function complete(Authenticatable $agent, Request $request): array
    {
        $validated = $request->validate([
            'uuid' => [
                'required',
                'array',
                function ($attribute, $value, $fail) {
                    foreach ($value as $uuid) {
                        if (!Validator::make(['uuid' => $uuid], [
                            'uuid' => ['required', 'uuid', new Exists(Upload::class, 'uuid')]
                        ])->passes()) {
                            $fail('The '.$attribute.' contains invalid UUID(s).');
                        }
                    }
                },
            ]
        ]);

        foreach ($validated['uuid'] as $uuid) {
            Upload::find($uuid)->update(['expired_at' => null]);
        }

        return [
            'success' => true,
        ];
    }
}
