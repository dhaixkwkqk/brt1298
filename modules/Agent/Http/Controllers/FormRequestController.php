<?php

namespace Modules\Agent\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Agent\Models\Agent;
use App\Models\FormRequest\FormRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Str;
use Modules\Agent\Actions\FormRequest\FormRequestBeforeCreate;
use Modules\Agent\Actions\FormRequest\FormRequestCreate;
use Modules\Agent\Actions\FormRequest\FormRequestList;
use Modules\Agent\Http\Requests\FormRequest\CreateRequest;
use Modules\Agent\Http\Requests\FormRequest\ListRequest;

class FormRequestController extends Controller
{
    /**
     * Метод API получения списка текущих запросов пользователя моб приложения.
     * @param  Agent  $agent
     */
    public function list(Authenticatable $agent, ListRequest $listRequest)
    {
        $formRequestList = new FormRequestList($agent, $listRequest);

        return $formRequestList->run();
    }

    /**
     * Метод API получения списка возможных статусов Запроса.
     * @return true[]
     */
    public function statuses()
    {
        $data = [];

        foreach (FormRequest::$statuses as $status) {
            $data[$status] = Str::ucfirst($status);
        }

        return [
            'success' => true,
            'data' => $data,
        ];
    }

    /**
     * Метод API для генерации уникального номера запроса (используется при создании запроса).
     * @param  FormRequestBeforeCreate  $action
     * @return true[]
     */
    public function beforeCreate(FormRequestBeforeCreate $action)
    {
        return [
            'success' => true,
            'data' => $action->run(),
        ];
    }

    /**
     * Метод API для создания запроса.
     * @param  CreateRequest  $request
     */
    public function create(CreateRequest $request, FormRequestCreate $action)
    {
        $request->run();

        return $action->run($request);
    }
}
