<?php

namespace Modules\Agent\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;
use Modules\Agent\Models\Agent;
use OpenPGP\Message\EncryptedMessage;
use OpenPGP\OpenPGP;

class OpenPGPController extends Controller
{
    /**
     * Метод проверки работоспособности шифрования OpenPGP.
     * @param  Agent  $agent
     * @return array
     */
    public function index(Authenticatable $agent): array
    {
        if( !$agent->openpgp_public_key ) {
            return [
                'success' => false,
                'message' => 'OpenPGP Public key is not set',
                'code' => 'OPENPGP_PUBLIC_KEY_NOT_SET',
            ];
        }

        $publicKey = OpenPGP::readPublicKey($agent->openpgp_public_key);
        $cleartext = json_encode(['username' => $agent->username, 'time' => time(), 'rand' => mt_rand()]);
        $encrypted = (string)OpenPGP::encrypt(OpenPGP::createLiteralMessage($cleartext), [$publicKey]);

        return [
            'success' => true,
            'data' => [
                'public_key' => $agent->openpgp_public_key,
                'cleartext' => $cleartext,
                'encrypted' => $encrypted
            ],
        ];
    }

    /**
     * Метод изменения публичного ключа OpenPGP шифрования.
     * @param  Agent  $agent
     * @param  Request  $request
     * @return array
     */
    public function change(Authenticatable $agent, Request $request): array
    {
        $validated = $request->validate([
            'openpgp_public_key' => ['required', 'string']
        ]);

        try {
            OpenPGP::readPublicKey($validated['openpgp_public_key']);
        } catch (\Exception $e) {
            throw ValidationException::withMessages([
                'openpgp_public_key' => [
                    "It's not valid a public key"
                ]
            ]);
        }

        $agent->update($validated);

        return [
            'success' => true,
        ];
    }
}
