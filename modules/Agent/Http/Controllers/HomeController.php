<?php

namespace Modules\Agent\Http\Controllers;

use App\Facades\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Agent\Models\Agent;

class HomeController extends Controller
{
    /**
     * Метод получение основной информации по авторизованному Агенту.
     * @param  Agent  $agent
     * @return array
     */
    public function index(Authenticatable $agent): array
    {
        $technicalWork = (bool)Setting::get('agent_technical_work', false);

        return [
            'technical_work' => [
                'enable' => $technicalWork,
                'message' => $technicalWork ? Setting::get('agent_technical_work_message') : null,
            ],
            'agent' => $agent->only(['name', 'openpgp_public_key']),
            'client' => $agent->client->only(['name', 'openpgp_public_key'])
        ];
    }

    public function status(): array
    {
        $technicalWork = (bool)Setting::get('agent_technical_work', false);

        return [
            'technical_work' => [
                'enable' => $technicalWork,
                'message' => $technicalWork ? Setting::get('agent_technical_work_message') : null,
            ],
        ];
    }
}
