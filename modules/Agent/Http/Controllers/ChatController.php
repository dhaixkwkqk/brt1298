<?php

namespace Modules\Agent\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMember;
use App\Models\Chat\ChatMessage;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;
use Modules\Agent\Actions\Chat\SendMessage;
use Modules\Agent\Http\Requests\AgentRequest;
use Modules\Agent\Http\Requests\Chat\ChatRequest;
use Modules\Agent\Http\Requests\Chat\SendMessageRequest;
use Modules\Agent\Http\Resources\ChatMessageCollection;

class ChatController extends Controller
{
    public function support(AgentRequest $request)
    {
        $agent = $request->getAgent();
        $agent->load(['chat', 'client']);

        $chat = $agent->chat;
        if (!$chat) {
            $chat = $agent->chat()->create([
                'uid' => Str::uuid()->toString()
            ]);

            $chatMember = new ChatMember();
            $chatMember->chat()->associate($chat);
            $chatMember->user()->associate($agent);
            $chatMember->save();

            $chatMember = new ChatMember();
            $chatMember->chat()->associate($chat);
            $chatMember->user()->associate($agent->client);
            $chatMember->save();

            $agent->chatSupport()->create([
                'chat_id' => $chat->id
            ]);
        }

        return [
            'success' => true,
            'data' => [
                'uid' => $chat->uid,
            ]
        ];
    }

    public function messages(ChatRequest $request)
    {
        $chat = $request->getChat();
        $chatMember = $request->getChatMember();

        $chatMessages = $chat
            ->chatMessages()
            ->with(['member.user'])
            ->orderBy('id', 'desc')
            ->cursorPaginate(10);

        /** @var ChatMessage $chatMessage */
        foreach ($chatMessages->items() as $chatMessage) {
            if (!$chatMessage->viewed_at) {
                if (!$chatMember->is($chatMessage->member)) {
                    $chatMessage->update([
                        'viewed_at' => Date::now()
                    ]);
                }
            }
        }

        return new ChatMessageCollection($chatMessages);
    }

    public function send(SendMessageRequest $request)
    {
        $action = new SendMessage($request);

        return $action->run();
    }
}
