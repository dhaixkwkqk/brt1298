<?php

namespace Modules\Agent\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Agent\Models\Agent;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Agent\Actions\Form\FormList;
use Modules\Agent\Http\Resources\FormCollection;

class FormController extends Controller
{
    /**
     * Метод API получение списка доступных для пользователя форм (для создания запроса).
     * @param  Agent  $agent
     * @return FormCollection
     */
    public function list(Authenticatable $agent)
    {
        $formList = new FormList($agent);

        return $formList->run();
    }
}
