<?php

namespace Modules\Agent\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Modules\Agent\Models\Agent;

class AgentOnlyMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if (!($user instanceof Agent)) {
            throw new AuthorizationException('Agent zone');
        }
        return $next($request);
    }
}
