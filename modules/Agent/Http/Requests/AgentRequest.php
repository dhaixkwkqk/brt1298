<?php

namespace Modules\Agent\Http\Requests;

use Modules\Agent\Models\Agent;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::user() instanceof Agent;
    }

    public function getAgent(): Agent
    {
        /** @var Agent $agent */
        $agent = Auth::user();

        return $agent;
    }
}
