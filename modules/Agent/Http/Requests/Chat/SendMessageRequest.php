<?php

namespace Modules\Agent\Http\Requests\Chat;

use App\Models\Upload;
use App\Rules\PGPMessageRule;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\ValidationException;

class SendMessageRequest extends ChatRequest
{
    public function rules(): array
    {
        return [
            ...parent::rules(),
            'message_encrypted' => [
                'required',
                'string',
                new PGPMessageRule(),
            ],
        ];
    }
}
