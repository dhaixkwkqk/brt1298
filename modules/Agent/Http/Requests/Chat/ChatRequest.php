<?php

namespace Modules\Agent\Http\Requests\Chat;

use Modules\Agent\Models\Agent;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMember;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\ValidationException;
use Modules\Agent\Http\Requests\AgentRequest;

class ChatRequest extends AgentRequest
{
    public function rules(): array
    {
        return [
            'uid' => [
                'required',
                'string',
                new Exists(Chat::class, 'uid')
            ]
        ];
    }

    protected ?Chat $chat = null;
    protected ?ChatMember $chatMember = null;

    public function getChat(): Chat
    {
        return $this->chat;
    }

    public function getChatMember(): ChatMember
    {
        return $this->chatMember;
    }

    public function authorize(): bool
    {
        if (!parent::authorize()) {
            return false;
        }

        $this->chat = Chat::whereUid($this->get('uid'))->first();
        if( !$this->chat ) {
            throw ValidationException::withMessages(['uid' => ['Chat not found.']]);
        }
        $this->chatMember = $this->chat->chatMembers()->where('user_id', $this->getAgent()->id)->first();
        if (!$this->chatMember) {
            return false;
        }

        return true;
    }
}
