<?php

namespace Modules\Agent\Http\Requests\FormRequest;

use App\Models\Form\Form;
use App\Rules\PGPMessageRule;
use App\Services\Element\ValidatorException;
use App\Services\Element\ValidatorInterface;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\Rules\Unique;
use Illuminate\Validation\ValidationException;
use Modules\Agent\Http\Requests\AgentRequest;

class CreateRequest extends AgentRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'uid' => [
                'required',
                new Unique(\App\Models\FormRequest\FormRequest::class),
                function (string $attribute, mixed $value, Closure $fail) {
                    if (!Cache::has('FormRequest_'.$value)) {
                        $fail("The {$attribute} is invalid.");
                    }
                },
            ],
            'form_id' => [
                'required',
                (new Exists(Form::class, 'uid'))->where('active', true),
            ],
            'fields_encrypted' => [
                'required',
                'string',
                new PGPMessageRule(),
            ]
        ];
    }

    protected ?Form $form = null;

    public function getForm(): ?Form
    {
        return $this->form;
    }

    public function run(): void
    {
        // Получаем объект формы
        $this->form = Form::whereUid($this->validated('form_id'))
            ->whereActive(true)
            ->with(['client', 'fields.element'])
            ->firstOrFail();

        // Проверяем, есть ли у пользователя право на работу с этой формой
        $allowed = $this->getAgent()
                ->companies()
                ->whereHas('forms', function (Builder $query) {
                    $query->where('id', $this->form->id);
                })
                ->count() > 0;
        if (!$allowed) {
            throw ValidationException::withMessages(['form_id' => 'Form not allowed for you.']);
        }
    }
}
