<?php

namespace Modules\Agent\Http\Requests\FormRequest;

use Illuminate\Validation\Rule;
use Modules\Agent\Http\Requests\AgentRequest;

class ListRequest extends AgentRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'filter' => ['array:status'],
            'filter.status' => ['string', 'max:255', Rule::in([
                'all',
                ...array_values(\App\Models\FormRequest\FormRequest::$statuses),
            ]), 'nullable'],
        ];
    }
}
