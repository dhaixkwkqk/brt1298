<?php

namespace Modules\Agent\Http\Requests\Upload;

use Illuminate\Validation\Rules\File;
use Modules\Agent\Http\Requests\AgentRequest;

class UploadRequest extends AgentRequest
{
    public function rules(): array
    {
        return [
            'file_encrypted' => [
                'required',
                File::types(['pgp'])->max(1024 * 5 * 10)
            ],
            'checksum' => [
                'required',
                'string',
                'regex:/^([a-f0-9]{64})$/',
            ],
        ];
    }
}
