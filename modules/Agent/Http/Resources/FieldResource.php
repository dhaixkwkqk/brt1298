<?php

namespace Modules\Agent\Http\Resources;

use App\Services\Element\ValidatorInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

/** @mixin \App\Models\Field */
class FieldResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        $data = [
            'uid' => $this->uid,
            'label' => $this->label,
            'icon' => $this->icon,
            'type' => $this->type,
            'element' => $this->element,
        ];
        if( $this->pivot && $this->pivot->value ) {
            /** @var ValidatorInterface $validator */
            $validator = App::make($this->element->getValidator(), [$this->element]);
            $value = @json_decode($this->pivot->value, true);
            $value = $validator->cast($value);
            $data[is_array($value) ? 'value_array' : 'value'] = $value;
        }
        return $data;
    }
}
