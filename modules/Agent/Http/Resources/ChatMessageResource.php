<?php

namespace Modules\Agent\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Chat\ChatMessage */
class ChatMessageResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray(Request $request): array
    {
        $data = [
            'id' => $this->id,
            'message_encrypted' => $this->message_encrypted,
            'created_at' => $this->created_at,
            'viewed_at' => $this->viewed_at,
        ];
        if( $this->whenLoaded('member') ) {
            if( $request->user() ) {
                $data['im'] = $this->member->user->is($request->user());
            }
            $data['member'] = new ChatMemberResource($this->whenLoaded('member'));
        }
        return $data;
    }
}
