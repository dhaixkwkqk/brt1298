<?php

namespace Modules\Agent\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Chat\ChatMember */
class ChatMemberResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->user->name,
        ];
    }
}
