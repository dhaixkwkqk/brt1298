<?php

namespace Modules\Agent\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\FormRequest\FormRequest */
class FormRequestResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'uid' => $this->uid,
            'chat' => $this->whenLoaded('chat', fn() => $this->chat->only(['uid'])),
            'status' => $this->status,

            'form' => new FormResource($this->whenLoaded('form')),
            'fields_encrypted' => $this->fields_encrypted,
        ];
    }
}
