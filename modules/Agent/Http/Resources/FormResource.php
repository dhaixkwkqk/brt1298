<?php

namespace Modules\Agent\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Form\Form */
class FormResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'uid' => $this->uid,
            'name' => $this->name,
            'price' => $this->price,
            'fields' => FieldResource::collection($this->whenLoaded('fields')),
        ];
    }
}
