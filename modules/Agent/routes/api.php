<?php

use Illuminate\Http\Request;
use Modules\Agent\Http\Controllers\AuthController;
use Modules\Agent\Http\Controllers\ChatController;
use Modules\Agent\Http\Controllers\FormController;
use Modules\Agent\Http\Controllers\FormRequestController;
use Modules\Agent\Http\Controllers\HomeController;
use Modules\Agent\Http\Controllers\OpenPGPController;
use Modules\Agent\Http\Controllers\UploadController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('agent')->group(function () {
    Route::get('/status', [HomeController::class, 'status']);
    Route::post('/auth', [AuthController::class, 'login']);

    Route::middleware(['auth:sanctum', 'agent.only'])->group(function () {
        Route::get('/', [HomeController::class, 'index']);
        Route::delete('/auth', [AuthController::class, 'logout']);

        Route::get('/openpgp', [OpenPGPController::class, 'index']);
        Route::post('/openpgp', [OpenPGPController::class, 'change']);

        Route::get('/form', [FormController::class, 'list']);

        Route::get('/form-request', [FormRequestController::class, 'list']);
        Route::get(
            '/form-request/statuses',
            [FormRequestController::class, 'statuses']
        );
        Route::post(
            '/form-request/before-create',
            [FormRequestController::class, 'beforeCreate']
        );
        Route::post('/form-request/create', [FormRequestController::class, 'create']
        );

        Route::post('/upload', [UploadController::class, 'index']);
        Route::post('/upload/complete', [UploadController::class, 'complete']);

        Route::get('/chat/messages', [ChatController::class, 'messages']);
        Route::post('/chat/send', [ChatController::class, 'send']);
        Route::get('/chat/support', [ChatController::class, 'support']);
    });
});
