import { fileURLToPath } from "node:url"
import vue from "@vitejs/plugin-vue"
import vueJsx from "@vitejs/plugin-vue-jsx"
import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import { defineConfig } from "vite"
import Pages from "vite-plugin-pages"
import Layouts from "vite-plugin-vue-layouts"
import vuetify from "vite-plugin-vuetify"
import DefineOptions from "unplugin-vue-define-options/vite"
import laravel from "laravel-vite-plugin"

export default defineConfig({
  build: {
    outDir: "../../public/build-client",
    emptyOutDir: true,
    manifest: true,
    chunkSizeWarningLimit: 5000,
    target: 'esnext'
  },
  css: {
    postcss: {
      plugins: [
        require("tailwindcss"), // Используйте плагин Tailwind CSS
      ],
    },
  },
  plugins: [
    laravel({
      hotFile: "../../storage/vite.hot",
      publicDirectory: "../../public",
      buildDirectory: "build-client",
      input: `${__dirname}/Resources/js/app.js`,
      refresh: true,
    }),
    vue({
      template: {
        transformAssetUrls: {
          base: null,
          includeAbsolute: false,
        },
      },
    }),
    vueJsx(),

    // https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin
    vuetify({
      styles: {
        configFile: "Resources/styles/variables/_vuetify.scss",
      },
    }),
    Pages({
      dirs: ["./Resources/js/pages"],
    }),
    Layouts({
      layoutsDirs: "./Resources/js/layouts/",
    }),
    Components({
      dirs: ["Resources/js/@core/components", "Resources/js/views/demos", "Resources/js/components"],
      dts: true,
    }),
    AutoImport({
      eslintrc: {
        enabled: true,
        filepath: "./.eslintrc-auto-import.json",
      },
      imports: ["vue", "@vueuse/core", "@vueuse/math", "pinia"],
      vueTemplate: true,
    }),
    DefineOptions(),
  ],
  define: { "process.env": {} },
  resolve: {
    alias: {
      "@core-scss": fileURLToPath(new URL("./Resources/styles/@core", import.meta.url)),
      "@": fileURLToPath(new URL("./Resources/js", import.meta.url)),
      "@themeConfig": fileURLToPath(new URL("./themeConfig.js", import.meta.url)),
      "@core": fileURLToPath(new URL("./Resources/js/@core", import.meta.url)),
      "@layouts": fileURLToPath(new URL("./Resources/js/@layouts", import.meta.url)),
      "@images": fileURLToPath(new URL("./Resources/images/", import.meta.url)),
      "@styles": fileURLToPath(new URL("./Resources/styles/", import.meta.url)),
      "@configured-variables": fileURLToPath(new URL("./Resources/styles/variables/_template.scss", import.meta.url)),
      "@axios": fileURLToPath(new URL("./Resources/js/plugins/axios", import.meta.url)),
      "@validators": fileURLToPath(new URL("./Resources/js/@core/utils/validators", import.meta.url)),
      "apexcharts": fileURLToPath(new URL("../../node_modules/apexcharts-clevision", import.meta.url)),
      "@ziggy": fileURLToPath(new URL("../../vendor/tightenco/ziggy/dist/vue.es.js", import.meta.url)),
    },
  },
  optimizeDeps: {
    exclude: ["vuetify"],
    entries: [
      "./Resources/js/**/*.vue",
    ],
  },
})
