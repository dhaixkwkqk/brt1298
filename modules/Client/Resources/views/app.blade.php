<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    @routes()
    {{ @module_vite('build-client', 'Resources/js/app.js') }}
    {{ @module_vite('build-client', "Resources/js/pages/{$page['component']}.vue") }}
    @inertiaHead
</head>
<body class="font-sans antialiased">
@inertia
</body>
</html>
