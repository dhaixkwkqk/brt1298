export const ChatFields =  [
  {
    name: 'Active',
    chatId: "35123",
    fields: [
      { icon: 'bx bx-coffee' },
      { icon: 'bx bx-knife' },
      { icon: 'bx bx-info-circle' },
      { icon: 'bx bx-task' }
    ]
  },
  {
    name: 'Listed',
    chatId: "1235",
    fields: [{ icon: '' }, { icon: '' }, { icon: '' }]
  },
  {
    name: 'Listed',
    chatId: "4363",
    fields: [{ icon: '' }, { icon: '' }, { icon: '' }]
  },
  {
    name: 'Listed',
    chatId: "34634",
    fields: [{ icon: '' }, { icon: '' }, { icon: '' }]
  },
  {
    name: 'Closed',
    chatId: "8672",
    fields: [
      { icon: 'bx bx-coffee' },
      { icon: 'bx bx-knife' },
      { icon: 'bx bx-info-circle' },
      { icon: '' },
      { icon: '' },
      { icon: '' },
      { icon: 'bx bx-task' }
    ]
  },
  {
    name: 'Closed',
    chatId: "8219",
    fields: [
      { icon: 'bx bx-coffee' },
      { icon: 'bx bx-knife' },
      { icon: 'bx bx-info-circle' },
      { icon: '' },
      { icon: '' },
      { icon: '' },
      { icon: 'bx bx-task' }
    ]
  },
  {
    name: 'Deleted',
    chatId: "8324",
    color: 'text-red-500',
    bg: 'bg-red-500/10',
    bgActive: 'bg-red-500',
    fields: [{ icon: '' }, { icon: '' }, { icon: '' }]
  },
  {
    name: 'Issue',
    chatId: "19293",
    color: 'text-white',
    bg: 'bg-red-500/10',
    bgActive: 'bg-red-500',
    fields: [{ icon: '' }, { icon: '' }]
  }
]
