import "./bootstrap"
import Vue3Toastify from 'vue3-toastify';
import "@/@iconify/icons-bundle"
import vuetify from "@/plugins/vuetify"
import { loadFonts } from "@/plugins/webfontloader"
import "@styles/@core/template/index.scss"
import "@styles/build.css"
import "@styles/styles.scss"
import { createApp, h } from "vue"
import { createInertiaApp } from "@inertiajs/vue3"
import { ZiggyVue } from "@ziggy"
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers"
import "vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css"
import Default from "@/layouts/default.vue"
import layoutsPlugin from "@/plugins/layouts"
import { vClickOutside } from "./directives/ClickOutsideDirective"
import { vDynamicDropdown } from "./directives/DynamicDropdown"
import { vMobileTable } from "./directives/MobileTable"
import 'vue3-toastify/dist/index.css';
import i18n from '@/plugins/i18n/i18n';

(async () => {
  await loadFonts()

  const appName = window.document.getElementsByTagName("title")[0]?.innerText || "Laravel"

  await createInertiaApp({
    title: title => `${title} - ${appName}`,
    resolve: async name => {
      const page = await resolvePageComponent(`./pages/${name}.vue`, import.meta.glob("./pages/**/*.vue"))
      if (!page.default.layout) {
        page.default.layout = Default
      }

      return page
    },
    setup({ el, App, props, plugin }) {

      createApp({ render: () => h(App, props) })
        .use(plugin)
        .use(vuetify)
        .use(ZiggyVue)
        .use(i18n)
        .use(Vue3Toastify, {})
        .use(layoutsPlugin)
        .directive('click-outside', vClickOutside)
        .directive('dynamic-dropdown', vDynamicDropdown)
        .directive('mobile-table', vMobileTable)
        .mount(el)
    },
    progress: {
      color: "#4B5563",
    },
  })
})()



