export const vMobileTable = {
  mounted: (el, binding) => {
    // Функция обработки таблицы
    const processTable = (data) => {
      console.log('data', el, binding)
      if (!el) return;

      const titlesEls = el.querySelectorAll('thead tr th');
      const titles = [];
      titlesEls.forEach((item) => {
        if(!item.textContent) return titles.push('')
        if(binding.value.titleBase)  titles.push(formatString(item.textContent + ':'))
        else titles.push(item.textContent + ':')

      });

      const trs = el.querySelectorAll('tbody tr');
      trs.forEach((tr) => {
        const tds = tr.querySelectorAll('td');
        tds.forEach((td, idx) => {
          td.setAttribute('data-title', titles[idx]);
        });
      });
      el.classList.add('mobileTable');
    };
    processTable()
    // Воспользуемся watch для реакции на изменения в binding.value.data
    const unwatch = binding.instance.$watch(() => binding.value.data, processTable, { immediate: true });

    // Внедрение стилей
    const style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    style.classList.add('mobileTableStyles');
    document.head.appendChild(style);

    // Удаление наблюдателя при размонтировании
    el._unwatch = unwatch;
  },
  unmounted(el) {
    const styleElement = document.querySelector('.mobileTableStyles');
    if (styleElement) {
      document.head.removeChild(styleElement);
    }

    // Удаляем watch
    if (el._unwatch) {
      el._unwatch();
    }
  },
}

const css = `@media (max-width: 1400px) {
 .mobileTable thead {
   display: none;
  }
  .mobileTable .whitespace-nowrap{
    white-space: initial !important;
  }
  .mobileTable tr {
       display: grid;
       grid-template-columns: 1fr 1fr;
       margin-bottom: 10px;
  }
  .mobileTable td {
      position: relative;
      display: flex !important;
      flex-wrap: wrap;
      min-height: 52px;
      justify-content: flex-start !important;
      align-items: flex-start;
      padding: 10px 20px 10px 105px !important;
      grid-gap: 6px !important;
      border-bottom: 1px solid rgba(6, 75, 6, 0.04);
      border-right: 1px solid rgba(6, 75, 6, 0.04);
  }
  .mobileTable td:before {
    content: attr(data-title);
    display: block;
    width: 150px;
    position: absolute;
    top: 12px;
    left: 15px;
    font-weight: bold;
    flex-shrink: 0;
    font-size: 13px;
      line-height: 1.2;
    }
}
 @media (max-width: 1100px) {
   .mobileTable tr {
     grid-template-columns: 1fr;
    }
   .mobileTable td {
   border-right: none;
  }
  .mobileTable td:before {
       width: 95px;
    }
  }
 `

function formatString(str) {
  if (!str) return ''; // Возвращаем пустую строку, если передана пустая строка или null

  const lowerCaseStr = str.toLowerCase(); // Преобразование в нижний регистр
  const formattedStr = lowerCaseStr.charAt(0).toUpperCase() + lowerCaseStr.slice(1); // Сделать первую букву большой

  return formattedStr;
}
