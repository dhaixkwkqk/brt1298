export const vDynamicDropdown = {
  mounted: (el, binding) => {
    console.log(el, binding)
    checkPosition(el)
  },
}


export function checkPosition(el){
  const rect = el.getBoundingClientRect();
  const bottomPosition = rect.bottom;
  const windowHeight = window.innerHeight;

  if (bottomPosition > windowHeight - 60) {
    el.style.bottom = '43px';
  } else {
    el.style.top = '43px';
  }
}
