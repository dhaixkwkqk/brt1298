// usePagination.js
import { ref, computed, watch } from 'vue';

export function usePagination(data, currentPage = 1, itemsPerPage = 6) {
  const { currentPage: currentPageRef, itemsPerPage: itemsPerPageRef } = toRefs({
    currentPage: ref(currentPage),
    itemsPerPage: ref(itemsPerPage),
  });

  const paginatedData = computed(() => {
    console.log(currentPageRef.value)
    const startIndex = (currentPageRef.value - 1) * itemsPerPageRef.value;
    const endIndex = startIndex + itemsPerPageRef.value;
    return data.slice(startIndex, endIndex);
  });

  const totalPages = computed(() => Math.ceil(data.length / itemsPerPageRef.value));


  watch(currentPageRef, (newPage, oldPage) => {
    if (newPage !== oldPage) {
      // Вы можете выполнить действия при изменении текущей страницы, если это необходимо
    }
  });

  return {
    currentPage: currentPageRef,
    itemsPerPage: itemsPerPageRef,
    paginatedData,
    totalPages,
  };
}
