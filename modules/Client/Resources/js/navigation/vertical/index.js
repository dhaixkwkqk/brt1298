import {projectPath} from '../../vars'

export default [
  {
    title: 'Information',
    to: projectPath + '/',
    icon: { icon: 'bx bx-info-circle' }
  },
  {
    title: 'companies',
    to: projectPath + '/companies',
    icon: { icon: 'bx bx-home-circle' }
  },
  {
    title: 'Forms',
    to: projectPath + '/forms',
    icon: { icon: 'bx bx-list-check' }
  },
  {
    title: 'Requests',
    to: projectPath + '/requests',
    icon: { icon: 'bx bx-receipt' },
    notif: 11
  },
  {
    title: 'Agents',
    to: projectPath + '/users',
    icon: { icon: 'bx bx-user-circle' }
  },
  {
    title: 'Chats',
    to: projectPath + '/chats',
    icon: { icon: 'bx bx-conversation' }
  },
  {
    title: 'Alerts',
    to: projectPath + '/alerts',
    icon: { icon: 'bx bx-bell' }
  }
];
