import { createI18n } from 'vue-i18n';

const messages = Object.fromEntries(Object.entries(
    import.meta.glob('../../../../lang/*.json', { eager: true }))
    .map(([key, value]) => [key.slice(17, -5), value.default]))

export default {
    install: async (app, options) => {
        // Проверяем, есть ли сохраненный язык в localStorage
        const savedLocale = localStorage.getItem('locale');


        const i18n = createI18n({
            legacy: false,
            locale: savedLocale || 'en', // Используем сохраненный язык или английский по умолчанию
            missingWarn: false,
            fallbackWarn: false,
            silentTranslationWarn: true,
            messages
        });

        app.config.globalProperties.$localStorage = localStorage;
        app.config.globalProperties.$i18n = i18n;

        app.config.globalProperties.$changeLocale = (locale) => {
            i18n.global.locale.value = locale;
            localStorage.setItem('locale', locale);
        };

        app.use(i18n);
    }
}
