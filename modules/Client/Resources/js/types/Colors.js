export const tagColors = {
  "All": ["bg-blue-500 text-white", "bg-blue-500/10 text-blue-900"],
  "Active": ["bg-green-500 text-white", "bg-green-500/10 text-green-900"],
  "Listed": ["bg-blue-500 text-white", "bg-blue-500/10 text-blue-900"],
  "Closed": ["bg-purple-500 text-white", "bg-purple-500/10 text-purple-900"],
  "Deleted": ["bg-red-500 text-white", "bg-red-500/10 text-red-900"],
  "Issue": ["bg-orange-500 text-white", "bg-orange-500/10 text-orange-900"]
}

export const getColorsWithoutAll = () => Object.keys(tagColors).filter(el => el !== 'All')
export const getColorsKeys = () => Object.keys(tagColors)

export const getColorByName = (name, index) => {
  console.log('getColorByName', name)
  try{
    return tagColors[name][index]
  }catch (err){
    return 'bg-dark-400 text-white hover:bg-gray-500 hover:text-white focus:bg-gray-500 focus:text-white '
  }
}
