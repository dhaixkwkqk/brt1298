export const fieldTypes = {
  ImageElement: 'ImageElement',
  CheckboxElement: 'CheckboxElement',
  InputElement: 'InputElement',
  TextAreaElement: 'TextareaElement',
  InputNumberElement: 'InputNumberElement',
  SelectElement: 'SelectElement',
  RadioElement: 'RadioElement',
}

export const fields = {
  [fieldTypes.InputElement]: {
    type: fieldTypes.InputElement,
    uid: null,
    label: '',
    icon: null,
    value: null,
    element: {
      "min_length": 1,
      "max_length": 30,
      "pattern": null,
      "value_type": 'text',
      "placeholder": null
    }
  },
  [fieldTypes.TextAreaElement]: {
    type: fieldTypes.TextAreaElement,
    uid: null,
    label: '',
    icon: null,
    value: null,
    element: {
      "min_length": 1,
      "max_length": 30,
      "pattern": null,
      "value_type": 'text',
      "placeholder": null
    }
  },
  [fieldTypes.InputNumberElement]: {
    type: fieldTypes.InputNumberElement,
    uid: null,
    label: '',
    icon: null,
    value: null,
    element: {
      "min_length": null,
      "max_length": null,
      "pattern": null,
      "value_type": 'number',
      "placeholder": null
    }
  },
  [fieldTypes.ImageElement]: {
    uid: null,
    label: null ,
    type: fieldTypes.ImageElement,
    element: {
      "min_count": 1,
      "max_count": 1,
      "max_file_size": 10240
    }
  },
  [fieldTypes.SelectElement]: {
    uid: null,
    label: null,
    multi: false,
    type: fieldTypes.SelectElement,
    element: {
      "variants": [""]
    }
  },
  [fieldTypes.CheckboxElement]: {
    uid: null,
    label: null,
    type: fieldTypes.CheckboxElement,
    element: {
      "variants": [""]
    }
  },
  [fieldTypes.RadioElement]: {
    uid: null,
    label: null,
    type: fieldTypes.RadioElement,
    element: {
      "variants": [""]
    }
  },
}
