export const IconsList = [
  {
    "name": "activity",
    "link": "https://b2brqst.com/icons/activity.svg",
    "searchArray": [
      "pulse",
      "health",
      "action",
      "motion"
    ]
  },
  {
    "name": "airplay",
    "link": "https://b2brqst.com/icons/airplay.svg",
    "searchArray": [
      "stream",
      "cast",
      "mirroring"
    ]
  },
  {
    "name": "alert-circle",
    "link": "https://b2brqst.com/icons/alert-circle.svg",
    "searchArray": [
      "warning",
      "alert",
      "danger"
    ]
  },
  {
    "name": "alert-octagon",
    "link": "https://b2brqst.com/icons/alert-octagon.svg",
    "searchArray": [
      "warning",
      "alert",
      "danger"
    ]
  },
  {
    "name": "alert-triangle",
    "link": "https://b2brqst.com/icons/alert-triangle.svg",
    "searchArray": [
      "warning",
      "alert",
      "danger"
    ]
  },
  {
    "name": "align-center",
    "link": "https://b2brqst.com/icons/align-center.svg",
    "searchArray": [
      "text alignment",
      "center"
    ]
  },
  {
    "name": "align-justify",
    "link": "https://b2brqst.com/icons/align-justify.svg",
    "searchArray": [
      "text alignment",
      "justified"
    ]
  },
  {
    "name": "align-left",
    "link": "https://b2brqst.com/icons/align-left.svg",
    "searchArray": [
      "text alignment",
      "left"
    ]
  },
  {
    "name": "align-right",
    "link": "https://b2brqst.com/icons/align-right.svg",
    "searchArray": [
      "text alignment",
      "right"
    ]
  },
  {
    "name": "anchor",
    "link": "https://b2brqst.com/icons/anchor.svg",
    "searchArray": []
  },
  {
    "name": "archive",
    "link": "https://b2brqst.com/icons/archive.svg",
    "searchArray": [
      "index",
      "box"
    ]
  },
  {
    "name": "at-sign",
    "link": "https://b2brqst.com/icons/at-sign.svg",
    "searchArray": [
      "mention",
      "at",
      "email",
      "message"
    ]
  },
  {
    "name": "award",
    "link": "https://b2brqst.com/icons/award.svg",
    "searchArray": [
      "achievement",
      "badge"
    ]
  },
  {
    "name": "aperture",
    "link": "https://b2brqst.com/icons/aperture.svg",
    "searchArray": [
      "camera",
      "photo"
    ]
  },
  {
    "name": "bar-chart",
    "link": "https://b2brqst.com/icons/bar-chart.svg",
    "searchArray": [
      "statistics",
      "diagram",
      "graph"
    ]
  },
  {
    "name": "bar-chart-2",
    "link": "https://b2brqst.com/icons/bar-chart-2.svg",
    "searchArray": [
      "statistics",
      "diagram",
      "graph"
    ]
  },
  {
    "name": "battery",
    "link": "https://b2brqst.com/icons/battery.svg",
    "searchArray": [
      "power",
      "electricity"
    ]
  },
  {
    "name": "battery-charging",
    "link": "https://b2brqst.com/icons/battery-charging.svg",
    "searchArray": [
      "power",
      "electricity"
    ]
  },
  {
    "name": "bell",
    "link": "https://b2brqst.com/icons/bell.svg",
    "searchArray": [
      "alarm",
      "notification",
      "sound"
    ]
  },
  {
    "name": "bell-off",
    "link": "https://b2brqst.com/icons/bell-off.svg",
    "searchArray": [
      "alarm",
      "notification",
      "silent"
    ]
  },
  {
    "name": "bluetooth",
    "link": "https://b2brqst.com/icons/bluetooth.svg",
    "searchArray": [
      "wireless"
    ]
  },
  {
    "name": "book-open",
    "link": "https://b2brqst.com/icons/book-open.svg",
    "searchArray": [
      "read",
      "library"
    ]
  },
  {
    "name": "book",
    "link": "https://b2brqst.com/icons/book.svg",
    "searchArray": [
      "read",
      "dictionary",
      "booklet",
      "magazine",
      "library"
    ]
  },
  {
    "name": "bookmark",
    "link": "https://b2brqst.com/icons/bookmark.svg",
    "searchArray": [
      "read",
      "clip",
      "marker",
      "tag"
    ]
  },
  {
    "name": "box",
    "link": "https://b2brqst.com/icons/box.svg",
    "searchArray": [
      "cube"
    ]
  },
  {
    "name": "briefcase",
    "link": "https://b2brqst.com/icons/briefcase.svg",
    "searchArray": [
      "work",
      "bag",
      "baggage",
      "folder"
    ]
  },
  {
    "name": "calendar",
    "link": "https://b2brqst.com/icons/calendar.svg",
    "searchArray": [
      "date"
    ]
  },
  {
    "name": "camera",
    "link": "https://b2brqst.com/icons/camera.svg",
    "searchArray": [
      "photo"
    ]
  },
  {
    "name": "cast",
    "link": "https://b2brqst.com/icons/cast.svg",
    "searchArray": [
      "chromecast",
      "airplay"
    ]
  },
  {
    "name": "chevron-down",
    "link": "https://b2brqst.com/icons/chevron-down.svg",
    "searchArray": [
      "expand"
    ]
  },
  {
    "name": "chevron-up",
    "link": "https://b2brqst.com/icons/chevron-up.svg",
    "searchArray": [
      "collapse"
    ]
  },
  {
    "name": "circle",
    "link": "https://b2brqst.com/icons/circle.svg",
    "searchArray": [
      "off",
      "zero",
      "record"
    ]
  },
  {
    "name": "clipboard",
    "link": "https://b2brqst.com/icons/clipboard.svg",
    "searchArray": [
      "copy"
    ]
  },
  {
    "name": "clock",
    "link": "https://b2brqst.com/icons/clock.svg",
    "searchArray": [
      "time",
      "watch",
      "alarm"
    ]
  },
  {
    "name": "cloud-drizzle",
    "link": "https://b2brqst.com/icons/cloud-drizzle.svg",
    "searchArray": [
      "weather",
      "shower"
    ]
  },
  {
    "name": "cloud-lightning",
    "link": "https://b2brqst.com/icons/cloud-lightning.svg",
    "searchArray": [
      "weather",
      "bolt"
    ]
  },
  {
    "name": "cloud-rain",
    "link": "https://b2brqst.com/icons/cloud-rain.svg",
    "searchArray": [
      "weather"
    ]
  },
  {
    "name": "cloud-snow",
    "link": "https://b2brqst.com/icons/cloud-snow.svg",
    "searchArray": [
      "weather",
      "blizzard"
    ]
  },
  {
    "name": "cloud",
    "link": "https://b2brqst.com/icons/cloud.svg",
    "searchArray": [
      "weather"
    ]
  },
  {
    "name": "codepen",
    "link": "https://b2brqst.com/icons/codepen.svg",
    "searchArray": [
      "logo"
    ]
  },
  {
    "name": "codesandbox",
    "link": "https://b2brqst.com/icons/codesandbox.svg",
    "searchArray": [
      "logo"
    ]
  },
  {
    "name": "code",
    "link": "https://b2brqst.com/icons/code.svg",
    "searchArray": [
      "source",
      "programming"
    ]
  },
  {
    "name": "coffee",
    "link": "https://b2brqst.com/icons/coffee.svg",
    "searchArray": [
      "drink",
      "cup",
      "mug",
      "tea",
      "cafe",
      "hot",
      "beverage"
    ]
  },
  {
    "name": "columns",
    "link": "https://b2brqst.com/icons/columns.svg",
    "searchArray": [
      "layout"
    ]
  },
  {
    "name": "command",
    "link": "https://b2brqst.com/icons/command.svg",
    "searchArray": [
      "keyboard",
      "cmd",
      "terminal",
      "prompt"
    ]
  },
  {
    "name": "compass",
    "link": "https://b2brqst.com/icons/compass.svg",
    "searchArray": [
      "navigation",
      "safari",
      "travel",
      "direction"
    ]
  },
  {
    "name": "copy",
    "link": "https://b2brqst.com/icons/copy.svg",
    "searchArray": [
      "clone",
      "duplicate"
    ]
  },
  {
    "name": "corner-down-left",
    "link": "https://b2brqst.com/icons/corner-down-left.svg",
    "searchArray": [
      "arrow",
      "return"
    ]
  },
  {
    "name": "corner-down-right",
    "link": "https://b2brqst.com/icons/corner-down-right.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "corner-left-down",
    "link": "https://b2brqst.com/icons/corner-left-down.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "corner-left-up",
    "link": "https://b2brqst.com/icons/corner-left-up.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "corner-right-down",
    "link": "https://b2brqst.com/icons/corner-right-down.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "corner-right-up",
    "link": "https://b2brqst.com/icons/corner-right-up.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "corner-up-left",
    "link": "https://b2brqst.com/icons/corner-up-left.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "corner-up-right",
    "link": "https://b2brqst.com/icons/corner-up-right.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "cpu",
    "link": "https://b2brqst.com/icons/cpu.svg",
    "searchArray": [
      "processor",
      "technology"
    ]
  },
  {
    "name": "credit-card",
    "link": "https://b2brqst.com/icons/credit-card.svg",
    "searchArray": [
      "purchase",
      "payment",
      "cc"
    ]
  },
  {
    "name": "crop",
    "link": "https://b2brqst.com/icons/crop.svg",
    "searchArray": [
      "photo",
      "image"
    ]
  },
  {
    "name": "crosshair",
    "link": "https://b2brqst.com/icons/crosshair.svg",
    "searchArray": [
      "aim",
      "target"
    ]
  },
  {
    "name": "database",
    "link": "https://b2brqst.com/icons/database.svg",
    "searchArray": [
      "storage",
      "memory"
    ]
  },
  {
    "name": "delete",
    "link": "https://b2brqst.com/icons/delete.svg",
    "searchArray": [
      "remove"
    ]
  },
  {
    "name": "disc",
    "link": "https://b2brqst.com/icons/disc.svg",
    "searchArray": [
      "album",
      "cd",
      "dvd",
      "music"
    ]
  },
  {
    "name": "dollar-sign",
    "link": "https://b2brqst.com/icons/dollar-sign.svg",
    "searchArray": [
      "currency",
      "money",
      "payment"
    ]
  },
  {
    "name": "droplet",
    "link": "https://b2brqst.com/icons/droplet.svg",
    "searchArray": [
      "water"
    ]
  },
  {
    "name": "edit",
    "link": "https://b2brqst.com/icons/edit.svg",
    "searchArray": [
      "pencil",
      "change"
    ]
  },
  {
    "name": "edit-2",
    "link": "https://b2brqst.com/icons/edit-2.svg",
    "searchArray": [
      "pencil",
      "change"
    ]
  },
  {
    "name": "edit-3",
    "link": "https://b2brqst.com/icons/edit-3.svg",
    "searchArray": [
      "pencil",
      "change"
    ]
  },
  {
    "name": "eye",
    "link": "https://b2brqst.com/icons/eye.svg",
    "searchArray": [
      "view",
      "watch"
    ]
  },
  {
    "name": "eye-off",
    "link": "https://b2brqst.com/icons/eye-off.svg",
    "searchArray": [
      "view",
      "watch",
      "hide",
      "hidden"
    ]
  },
  {
    "name": "external-link",
    "link": "https://b2brqst.com/icons/external-link.svg",
    "searchArray": [
      "outbound"
    ]
  },
  {
    "name": "facebook",
    "link": "https://b2brqst.com/icons/facebook.svg",
    "searchArray": [
      "logo",
      "social"
    ]
  },
  {
    "name": "fast-forward",
    "link": "https://b2brqst.com/icons/fast-forward.svg",
    "searchArray": [
      "music"
    ]
  },
  {
    "name": "figma",
    "link": "https://b2brqst.com/icons/figma.svg",
    "searchArray": [
      "logo",
      "design",
      "tool"
    ]
  },
  {
    "name": "file-minus",
    "link": "https://b2brqst.com/icons/file-minus.svg",
    "searchArray": [
      "delete",
      "remove",
      "erase"
    ]
  },
  {
    "name": "file-plus",
    "link": "https://b2brqst.com/icons/file-plus.svg",
    "searchArray": [
      "add",
      "create",
      "new"
    ]
  },
  {
    "name": "file-text",
    "link": "https://b2brqst.com/icons/file-text.svg",
    "searchArray": [
      "data",
      "txt",
      "pdf"
    ]
  },
  {
    "name": "film",
    "link": "https://b2brqst.com/icons/film.svg",
    "searchArray": [
      "movie",
      "video"
    ]
  },
  {
    "name": "filter",
    "link": "https://b2brqst.com/icons/filter.svg",
    "searchArray": [
      "funnel",
      "hopper"
    ]
  },
  {
    "name": "flag",
    "link": "https://b2brqst.com/icons/flag.svg",
    "searchArray": [
      "report"
    ]
  },
  {
    "name": "folder-minus",
    "link": "https://b2brqst.com/icons/folder-minus.svg",
    "searchArray": [
      "directory"
    ]
  },
  {
    "name": "folder-plus",
    "link": "https://b2brqst.com/icons/folder-plus.svg",
    "searchArray": [
      "directory"
    ]
  },
  {
    "name": "folder",
    "link": "https://b2brqst.com/icons/folder.svg",
    "searchArray": [
      "directory"
    ]
  },
  {
    "name": "framer",
    "link": "https://b2brqst.com/icons/framer.svg",
    "searchArray": [
      "logo",
      "design",
      "tool"
    ]
  },
  {
    "name": "frown",
    "link": "https://b2brqst.com/icons/frown.svg",
    "searchArray": [
      "emoji",
      "face",
      "bad",
      "sad",
      "emotion"
    ]
  },
  {
    "name": "gift",
    "link": "https://b2brqst.com/icons/gift.svg",
    "searchArray": [
      "present",
      "box",
      "birthday",
      "party"
    ]
  },
  {
    "name": "git-branch",
    "link": "https://b2brqst.com/icons/git-branch.svg",
    "searchArray": [
      "code",
      "version control"
    ]
  },
  {
    "name": "git-commit",
    "link": "https://b2brqst.com/icons/git-commit.svg",
    "searchArray": [
      "code",
      "version control"
    ]
  },
  {
    "name": "git-merge",
    "link": "https://b2brqst.com/icons/git-merge.svg",
    "searchArray": [
      "code",
      "version control"
    ]
  },
  {
    "name": "git-pull-request",
    "link": "https://b2brqst.com/icons/git-pull-request.svg",
    "searchArray": [
      "code",
      "version control"
    ]
  },
  {
    "name": "github",
    "link": "https://b2brqst.com/icons/github.svg",
    "searchArray": [
      "logo",
      "version control"
    ]
  },
  {
    "name": "gitlab",
    "link": "https://b2brqst.com/icons/gitlab.svg",
    "searchArray": [
      "logo",
      "version control"
    ]
  },
  {
    "name": "globe",
    "link": "https://b2brqst.com/icons/globe.svg",
    "searchArray": [
      "world",
      "browser",
      "language",
      "translate"
    ]
  },
  {
    "name": "hard-drive",
    "link": "https://b2brqst.com/icons/hard-drive.svg",
    "searchArray": [
      "computer",
      "server",
      "memory",
      "data"
    ]
  },
  {
    "name": "hash",
    "link": "https://b2brqst.com/icons/hash.svg",
    "searchArray": [
      "hashtag",
      "number",
      "pound"
    ]
  },
  {
    "name": "headphones",
    "link": "https://b2brqst.com/icons/headphones.svg",
    "searchArray": [
      "music",
      "audio",
      "sound"
    ]
  },
  {
    "name": "heart",
    "link": "https://b2brqst.com/icons/heart.svg",
    "searchArray": [
      "like",
      "love",
      "emotion"
    ]
  },
  {
    "name": "help-circle",
    "link": "https://b2brqst.com/icons/help-circle.svg",
    "searchArray": [
      "question mark"
    ]
  },
  {
    "name": "hexagon",
    "link": "https://b2brqst.com/icons/hexagon.svg",
    "searchArray": [
      "shape",
      "node.js",
      "logo"
    ]
  },
  {
    "name": "home",
    "link": "https://b2brqst.com/icons/home.svg",
    "searchArray": [
      "house",
      "living"
    ]
  },
  {
    "name": "image",
    "link": "https://b2brqst.com/icons/image.svg",
    "searchArray": [
      "picture"
    ]
  },
  {
    "name": "inbox",
    "link": "https://b2brqst.com/icons/inbox.svg",
    "searchArray": [
      "email"
    ]
  },
  {
    "name": "instagram",
    "link": "https://b2brqst.com/icons/instagram.svg",
    "searchArray": [
      "logo",
      "camera"
    ]
  },
  {
    "name": "key",
    "link": "https://b2brqst.com/icons/key.svg",
    "searchArray": [
      "password",
      "login",
      "authentication",
      "secure"
    ]
  },
  {
    "name": "layers",
    "link": "https://b2brqst.com/icons/layers.svg",
    "searchArray": [
      "stack"
    ]
  },
  {
    "name": "layout",
    "link": "https://b2brqst.com/icons/layout.svg",
    "searchArray": [
      "window",
      "webpage"
    ]
  },
  {
    "name": "life-buoy",
    "link": "https://b2brqst.com/icons/life-buoy.svg",
    "searchArray": [
      "help",
      "life ring",
      "support"
    ]
  },
  {
    "name": "link",
    "link": "https://b2brqst.com/icons/link.svg",
    "searchArray": [
      "chain",
      "url"
    ]
  },
  {
    "name": "link-2",
    "link": "https://b2brqst.com/icons/link-2.svg",
    "searchArray": [
      "chain",
      "url"
    ]
  },
  {
    "name": "linkedin",
    "link": "https://b2brqst.com/icons/linkedin.svg",
    "searchArray": [
      "logo",
      "social media"
    ]
  },
  {
    "name": "list",
    "link": "https://b2brqst.com/icons/list.svg",
    "searchArray": [
      "options"
    ]
  },
  {
    "name": "lock",
    "link": "https://b2brqst.com/icons/lock.svg",
    "searchArray": [
      "security",
      "password",
      "secure"
    ]
  },
  {
    "name": "log-in",
    "link": "https://b2brqst.com/icons/log-in.svg",
    "searchArray": [
      "sign in",
      "arrow",
      "enter"
    ]
  },
  {
    "name": "log-out",
    "link": "https://b2brqst.com/icons/log-out.svg",
    "searchArray": [
      "sign out",
      "arrow",
      "exit"
    ]
  },
  {
    "name": "mail",
    "link": "https://b2brqst.com/icons/mail.svg",
    "searchArray": [
      "email",
      "message"
    ]
  },
  {
    "name": "map-pin",
    "link": "https://b2brqst.com/icons/map-pin.svg",
    "searchArray": [
      "location",
      "navigation",
      "travel",
      "marker"
    ]
  },
  {
    "name": "map",
    "link": "https://b2brqst.com/icons/map.svg",
    "searchArray": [
      "location",
      "navigation",
      "travel"
    ]
  },
  {
    "name": "maximize",
    "link": "https://b2brqst.com/icons/maximize.svg",
    "searchArray": [
      "fullscreen"
    ]
  },
  {
    "name": "maximize-2",
    "link": "https://b2brqst.com/icons/maximize-2.svg",
    "searchArray": [
      "fullscreen",
      "arrows",
      "expand"
    ]
  },
  {
    "name": "meh",
    "link": "https://b2brqst.com/icons/meh.svg",
    "searchArray": [
      "emoji",
      "face",
      "neutral",
      "emotion"
    ]
  },
  {
    "name": "menu",
    "link": "https://b2brqst.com/icons/menu.svg",
    "searchArray": [
      "bars",
      "navigation",
      "hamburger"
    ]
  },
  {
    "name": "message-circle",
    "link": "https://b2brqst.com/icons/message-circle.svg",
    "searchArray": [
      "comment",
      "chat"
    ]
  },
  {
    "name": "message-square",
    "link": "https://b2brqst.com/icons/message-square.svg",
    "searchArray": [
      "comment",
      "chat"
    ]
  },
  {
    "name": "mic-off",
    "link": "https://b2brqst.com/icons/mic-off.svg",
    "searchArray": [
      "record",
      "sound",
      "mute"
    ]
  },
  {
    "name": "mic",
    "link": "https://b2brqst.com/icons/mic.svg",
    "searchArray": [
      "record",
      "sound",
      "listen"
    ]
  },
  {
    "name": "minimize",
    "link": "https://b2brqst.com/icons/minimize.svg",
    "searchArray": [
      "exit fullscreen",
      "close"
    ]
  },
  {
    "name": "minimize-2",
    "link": "https://b2brqst.com/icons/minimize-2.svg",
    "searchArray": [
      "exit fullscreen",
      "arrows",
      "close"
    ]
  },
  {
    "name": "minus",
    "link": "https://b2brqst.com/icons/minus.svg",
    "searchArray": [
      "subtract"
    ]
  },
  {
    "name": "monitor",
    "link": "https://b2brqst.com/icons/monitor.svg",
    "searchArray": [
      "tv",
      "screen",
      "display"
    ]
  },
  {
    "name": "moon",
    "link": "https://b2brqst.com/icons/moon.svg",
    "searchArray": [
      "dark",
      "night"
    ]
  },
  {
    "name": "more-horizontal",
    "link": "https://b2brqst.com/icons/more-horizontal.svg",
    "searchArray": [
      "ellipsis"
    ]
  },
  {
    "name": "more-vertical",
    "link": "https://b2brqst.com/icons/more-vertical.svg",
    "searchArray": [
      "ellipsis"
    ]
  },
  {
    "name": "mouse-pointer",
    "link": "https://b2brqst.com/icons/mouse-pointer.svg",
    "searchArray": [
      "arrow",
      "cursor"
    ]
  },
  {
    "name": "move",
    "link": "https://b2brqst.com/icons/move.svg",
    "searchArray": [
      "arrows"
    ]
  },
  {
    "name": "music",
    "link": "https://b2brqst.com/icons/music.svg",
    "searchArray": [
      "note"
    ]
  },
  {
    "name": "navigation",
    "link": "https://b2brqst.com/icons/navigation.svg",
    "searchArray": [
      "location",
      "travel"
    ]
  },
  {
    "name": "navigation-2",
    "link": "https://b2brqst.com/icons/navigation-2.svg",
    "searchArray": [
      "location",
      "travel"
    ]
  },
  {
    "name": "octagon",
    "link": "https://b2brqst.com/icons/octagon.svg",
    "searchArray": [
      "stop"
    ]
  },
  {
    "name": "package",
    "link": "https://b2brqst.com/icons/package.svg",
    "searchArray": [
      "box",
      "container"
    ]
  },
  {
    "name": "paperclip",
    "link": "https://b2brqst.com/icons/paperclip.svg",
    "searchArray": [
      "attachment"
    ]
  },
  {
    "name": "pause",
    "link": "https://b2brqst.com/icons/pause.svg",
    "searchArray": [
      "music",
      "stop"
    ]
  },
  {
    "name": "pause-circle",
    "link": "https://b2brqst.com/icons/pause-circle.svg",
    "searchArray": [
      "music",
      "audio",
      "stop"
    ]
  },
  {
    "name": "pen-tool",
    "link": "https://b2brqst.com/icons/pen-tool.svg",
    "searchArray": [
      "vector",
      "drawing"
    ]
  },
  {
    "name": "percent",
    "link": "https://b2brqst.com/icons/percent.svg",
    "searchArray": [
      "discount"
    ]
  },
  {
    "name": "phone-call",
    "link": "https://b2brqst.com/icons/phone-call.svg",
    "searchArray": [
      "ring"
    ]
  },
  {
    "name": "phone-forwarded",
    "link": "https://b2brqst.com/icons/phone-forwarded.svg",
    "searchArray": [
      "call"
    ]
  },
  {
    "name": "phone-incoming",
    "link": "https://b2brqst.com/icons/phone-incoming.svg",
    "searchArray": [
      "call"
    ]
  },
  {
    "name": "phone-missed",
    "link": "https://b2brqst.com/icons/phone-missed.svg",
    "searchArray": [
      "call"
    ]
  },
  {
    "name": "phone-off",
    "link": "https://b2brqst.com/icons/phone-off.svg",
    "searchArray": [
      "call",
      "mute"
    ]
  },
  {
    "name": "phone-outgoing",
    "link": "https://b2brqst.com/icons/phone-outgoing.svg",
    "searchArray": [
      "call"
    ]
  },
  {
    "name": "phone",
    "link": "https://b2brqst.com/icons/phone.svg",
    "searchArray": [
      "call"
    ]
  },
  {
    "name": "play",
    "link": "https://b2brqst.com/icons/play.svg",
    "searchArray": [
      "music",
      "start"
    ]
  },
  {
    "name": "pie-chart",
    "link": "https://b2brqst.com/icons/pie-chart.svg",
    "searchArray": [
      "statistics",
      "diagram"
    ]
  },
  {
    "name": "play-circle",
    "link": "https://b2brqst.com/icons/play-circle.svg",
    "searchArray": [
      "music",
      "start"
    ]
  },
  {
    "name": "plus",
    "link": "https://b2brqst.com/icons/plus.svg",
    "searchArray": [
      "add",
      "new"
    ]
  },
  {
    "name": "plus-circle",
    "link": "https://b2brqst.com/icons/plus-circle.svg",
    "searchArray": [
      "add",
      "new"
    ]
  },
  {
    "name": "plus-square",
    "link": "https://b2brqst.com/icons/plus-square.svg",
    "searchArray": [
      "add",
      "new"
    ]
  },
  {
    "name": "pocket",
    "link": "https://b2brqst.com/icons/pocket.svg",
    "searchArray": [
      "logo",
      "save"
    ]
  },
  {
    "name": "power",
    "link": "https://b2brqst.com/icons/power.svg",
    "searchArray": [
      "on",
      "off"
    ]
  },
  {
    "name": "printer",
    "link": "https://b2brqst.com/icons/printer.svg",
    "searchArray": [
      "fax",
      "office",
      "device"
    ]
  },
  {
    "name": "radio",
    "link": "https://b2brqst.com/icons/radio.svg",
    "searchArray": [
      "signal"
    ]
  },
  {
    "name": "refresh-cw",
    "link": "https://b2brqst.com/icons/refresh-cw.svg",
    "searchArray": [
      "synchronise",
      "arrows"
    ]
  },
  {
    "name": "refresh-ccw",
    "link": "https://b2brqst.com/icons/refresh-ccw.svg",
    "searchArray": [
      "arrows"
    ]
  },
  {
    "name": "repeat",
    "link": "https://b2brqst.com/icons/repeat.svg",
    "searchArray": [
      "loop",
      "arrows"
    ]
  },
  {
    "name": "rewind",
    "link": "https://b2brqst.com/icons/rewind.svg",
    "searchArray": [
      "music"
    ]
  },
  {
    "name": "rotate-ccw",
    "link": "https://b2brqst.com/icons/rotate-ccw.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "rotate-cw",
    "link": "https://b2brqst.com/icons/rotate-cw.svg",
    "searchArray": [
      "arrow"
    ]
  },
  {
    "name": "rss",
    "link": "https://b2brqst.com/icons/rss.svg",
    "searchArray": [
      "feed",
      "subscribe"
    ]
  },
  {
    "name": "save",
    "link": "https://b2brqst.com/icons/save.svg",
    "searchArray": [
      "floppy disk"
    ]
  },
  {
    "name": "scissors",
    "link": "https://b2brqst.com/icons/scissors.svg",
    "searchArray": [
      "cut"
    ]
  },
  {
    "name": "search",
    "link": "https://b2brqst.com/icons/search.svg",
    "searchArray": [
      "find",
      "magnifier",
      "magnifying glass"
    ]
  },
  {
    "name": "send",
    "link": "https://b2brqst.com/icons/send.svg",
    "searchArray": [
      "message",
      "mail",
      "email",
      "paper airplane",
      "paper aeroplane"
    ]
  },
  {
    "name": "settings",
    "link": "https://b2brqst.com/icons/settings.svg",
    "searchArray": [
      "cog",
      "edit",
      "gear",
      "preferences"
    ]
  },
  {
    "name": "share-2",
    "link": "https://b2brqst.com/icons/share-2.svg",
    "searchArray": [
      "network",
      "connections"
    ]
  },
  {
    "name": "shield",
    "link": "https://b2brqst.com/icons/shield.svg",
    "searchArray": [
      "security",
      "secure"
    ]
  },
  {
    "name": "shield-off",
    "link": "https://b2brqst.com/icons/shield-off.svg",
    "searchArray": [
      "security",
      "insecure"
    ]
  },
  {
    "name": "shopping-bag",
    "link": "https://b2brqst.com/icons/shopping-bag.svg",
    "searchArray": [
      "ecommerce",
      "cart",
      "purchase",
      "store"
    ]
  },
  {
    "name": "shopping-cart",
    "link": "https://b2brqst.com/icons/shopping-cart.svg",
    "searchArray": [
      "ecommerce",
      "cart",
      "purchase",
      "store"
    ]
  },
  {
    "name": "shuffle",
    "link": "https://b2brqst.com/icons/shuffle.svg",
    "searchArray": [
      "music"
    ]
  },
  {
    "name": "skip-back",
    "link": "https://b2brqst.com/icons/skip-back.svg",
    "searchArray": [
      "music"
    ]
  },
  {
    "name": "skip-forward",
    "link": "https://b2brqst.com/icons/skip-forward.svg",
    "searchArray": [
      "music"
    ]
  },
  {
    "name": "slack",
    "link": "https://b2brqst.com/icons/slack.svg",
    "searchArray": [
      "logo"
    ]
  },
  {
    "name": "slash",
    "link": "https://b2brqst.com/icons/slash.svg",
    "searchArray": [
      "ban",
      "no"
    ]
  },
  {
    "name": "sliders",
    "link": "https://b2brqst.com/icons/sliders.svg",
    "searchArray": [
      "settings",
      "controls"
    ]
  },
  {
    "name": "smartphone",
    "link": "https://b2brqst.com/icons/smartphone.svg",
    "searchArray": [
      "cellphone",
      "device"
    ]
  },
  {
    "name": "smile",
    "link": "https://b2brqst.com/icons/smile.svg",
    "searchArray": [
      "emoji",
      "face",
      "happy",
      "good",
      "emotion"
    ]
  },
  {
    "name": "speaker",
    "link": "https://b2brqst.com/icons/speaker.svg",
    "searchArray": [
      "audio",
      "music"
    ]
  },
  {
    "name": "star",
    "link": "https://b2brqst.com/icons/star.svg",
    "searchArray": [
      "bookmark",
      "favorite",
      "like"
    ]
  },
  {
    "name": "stop-circle",
    "link": "https://b2brqst.com/icons/stop-circle.svg",
    "searchArray": [
      "media",
      "music"
    ]
  },
  {
    "name": "sun",
    "link": "https://b2brqst.com/icons/sun.svg",
    "searchArray": [
      "brightness",
      "weather",
      "light"
    ]
  },
  {
    "name": "sunrise",
    "link": "https://b2brqst.com/icons/sunrise.svg",
    "searchArray": [
      "weather",
      "time",
      "morning",
      "day"
    ]
  },
  {
    "name": "sunset",
    "link": "https://b2brqst.com/icons/sunset.svg",
    "searchArray": [
      "weather",
      "time",
      "evening",
      "night"
    ]
  },
  {
    "name": "tablet",
    "link": "https://b2brqst.com/icons/tablet.svg",
    "searchArray": [
      "device"
    ]
  },
  {
    "name": "tag",
    "link": "https://b2brqst.com/icons/tag.svg",
    "searchArray": [
      "label"
    ]
  },
  {
    "name": "target",
    "link": "https://b2brqst.com/icons/target.svg",
    "searchArray": [
      "logo",
      "bullseye"
    ]
  },
  {
    "name": "terminal",
    "link": "https://b2brqst.com/icons/terminal.svg",
    "searchArray": [
      "code",
      "command line",
      "prompt"
    ]
  },
  {
    "name": "thermometer",
    "link": "https://b2brqst.com/icons/thermometer.svg",
    "searchArray": [
      "temperature",
      "celsius",
      "fahrenheit",
      "weather"
    ]
  },
  {
    "name": "thumbs-down",
    "link": "https://b2brqst.com/icons/thumbs-down.svg",
    "searchArray": [
      "dislike",
      "bad",
      "emotion"
    ]
  },
  {
    "name": "thumbs-up",
    "link": "https://b2brqst.com/icons/thumbs-up.svg",
    "searchArray": [
      "like",
      "good",
      "emotion"
    ]
  },
  {
    "name": "toggle-left",
    "link": "https://b2brqst.com/icons/toggle-left.svg",
    "searchArray": [
      "on",
      "off",
      "switch"
    ]
  },
  {
    "name": "toggle-right",
    "link": "https://b2brqst.com/icons/toggle-right.svg",
    "searchArray": [
      "on",
      "off",
      "switch"
    ]
  },
  {
    "name": "tool",
    "link": "https://b2brqst.com/icons/tool.svg",
    "searchArray": [
      "settings",
      "spanner"
    ]
  },
  {
    "name": "trash",
    "link": "https://b2brqst.com/icons/trash.svg",
    "searchArray": [
      "garbage",
      "delete",
      "remove",
      "bin"
    ]
  },
  {
    "name": "trash-2",
    "link": "https://b2brqst.com/icons/trash-2.svg",
    "searchArray": [
      "garbage",
      "delete",
      "remove",
      "bin"
    ]
  },
  {
    "name": "triangle",
    "link": "https://b2brqst.com/icons/triangle.svg",
    "searchArray": [
      "delta"
    ]
  },
  {
    "name": "truck",
    "link": "https://b2brqst.com/icons/truck.svg",
    "searchArray": [
      "delivery",
      "van",
      "shipping",
      "transport",
      "lorry"
    ]
  },
  {
    "name": "tv",
    "link": "https://b2brqst.com/icons/tv.svg",
    "searchArray": [
      "television",
      "stream"
    ]
  },
  {
    "name": "twitch",
    "link": "https://b2brqst.com/icons/twitch.svg",
    "searchArray": [
      "logo"
    ]
  },
  {
    "name": "twitter",
    "link": "https://b2brqst.com/icons/twitter.svg",
    "searchArray": [
      "logo",
      "social"
    ]
  },
  {
    "name": "type",
    "link": "https://b2brqst.com/icons/type.svg",
    "searchArray": [
      "text"
    ]
  },
  {
    "name": "umbrella",
    "link": "https://b2brqst.com/icons/umbrella.svg",
    "searchArray": [
      "rain",
      "weather"
    ]
  },
  {
    "name": "unlock",
    "link": "https://b2brqst.com/icons/unlock.svg",
    "searchArray": [
      "security"
    ]
  },
  {
    "name": "user-check",
    "link": "https://b2brqst.com/icons/user-check.svg",
    "searchArray": [
      "followed",
      "subscribed"
    ]
  },
  {
    "name": "user-minus",
    "link": "https://b2brqst.com/icons/user-minus.svg",
    "searchArray": [
      "delete",
      "remove",
      "unfollow",
      "unsubscribe"
    ]
  },
  {
    "name": "user-plus",
    "link": "https://b2brqst.com/icons/user-plus.svg",
    "searchArray": [
      "new",
      "add",
      "create",
      "follow",
      "subscribe"
    ]
  },
  {
    "name": "user-x",
    "link": "https://b2brqst.com/icons/user-x.svg",
    "searchArray": [
      "delete",
      "remove",
      "unfollow",
      "unsubscribe",
      "unavailable"
    ]
  },
  {
    "name": "user",
    "link": "https://b2brqst.com/icons/user.svg",
    "searchArray": [
      "person",
      "account"
    ]
  },
  {
    "name": "users",
    "link": "https://b2brqst.com/icons/users.svg",
    "searchArray": [
      "group"
    ]
  },
  {
    "name": "video-off",
    "link": "https://b2brqst.com/icons/video-off.svg",
    "searchArray": [
      "camera",
      "movie",
      "film"
    ]
  },
  {
    "name": "video",
    "link": "https://b2brqst.com/icons/video.svg",
    "searchArray": [
      "camera",
      "movie",
      "film"
    ]
  },
  {
    "name": "voicemail",
    "link": "https://b2brqst.com/icons/voicemail.svg",
    "searchArray": [
      "phone"
    ]
  },
  {
    "name": "volume",
    "link": "https://b2brqst.com/icons/volume.svg",
    "searchArray": [
      "music",
      "sound",
      "mute"
    ]
  },
  {
    "name": "volume-1",
    "link": "https://b2brqst.com/icons/volume-1.svg",
    "searchArray": [
      "music",
      "sound"
    ]
  },
  {
    "name": "volume-2",
    "link": "https://b2brqst.com/icons/volume-2.svg",
    "searchArray": [
      "music",
      "sound"
    ]
  },
  {
    "name": "volume-x",
    "link": "https://b2brqst.com/icons/volume-x.svg",
    "searchArray": [
      "music",
      "sound",
      "mute"
    ]
  },
  {
    "name": "watch",
    "link": "https://b2brqst.com/icons/watch.svg",
    "searchArray": [
      "clock",
      "time"
    ]
  },
  {
    "name": "wifi-off",
    "link": "https://b2brqst.com/icons/wifi-off.svg",
    "searchArray": [
      "disabled"
    ]
  },
  {
    "name": "wifi",
    "link": "https://b2brqst.com/icons/wifi.svg",
    "searchArray": [
      "connection",
      "signal",
      "wireless"
    ]
  },
  {
    "name": "wind",
    "link": "https://b2brqst.com/icons/wind.svg",
    "searchArray": [
      "weather",
      "air"
    ]
  },
  {
    "name": "x-circle",
    "link": "https://b2brqst.com/icons/x-circle.svg",
    "searchArray": [
      "cancel",
      "close",
      "delete",
      "remove",
      "times",
      "clear"
    ]
  },
  {
    "name": "x-octagon",
    "link": "https://b2brqst.com/icons/x-octagon.svg",
    "searchArray": [
      "delete",
      "stop",
      "alert",
      "warning",
      "times",
      "clear"
    ]
  },
  {
    "name": "x-square",
    "link": "https://b2brqst.com/icons/x-square.svg",
    "searchArray": [
      "cancel",
      "close",
      "delete",
      "remove",
      "times",
      "clear"
    ]
  },
  {
    "name": "x",
    "link": "https://b2brqst.com/icons/x.svg",
    "searchArray": [
      "cancel",
      "close",
      "delete",
      "remove",
      "times",
      "clear"
    ]
  },
  {
    "name": "youtube",
    "link": "https://b2brqst.com/icons/youtube.svg",
    "searchArray": [
      "logo",
      "video",
      "play"
    ]
  },
  {
    "name": "zap-off",
    "link": "https://b2brqst.com/icons/zap-off.svg",
    "searchArray": [
      "flash",
      "camera",
      "lightning"
    ]
  },
  {
    "name": "zap",
    "link": "https://b2brqst.com/icons/zap.svg",
    "searchArray": [
      "flash",
      "camera",
      "lightning"
    ]
  },
  {
    "name": "zoom-in",
    "link": "https://b2brqst.com/icons/zoom-in.svg",
    "searchArray": [
      "magnifying glass"
    ]
  },
  {
    "name": "zoom-out",
    "link": "https://b2brqst.com/icons/zoom-out.svg",
    "searchArray": [
      "magnifying glass"
    ]
  }
]

const json = {
  "activity": ["pulse", "health", "action", "motion"],
  "airplay": ["stream", "cast", "mirroring"],
  "alert-circle": ["warning", "alert", "danger"],
  "alert-octagon": ["warning", "alert", "danger"],
  "alert-triangle": ["warning", "alert", "danger"],
  "align-center": ["text alignment", "center"],
  "align-justify": ["text alignment", "justified"],
  "align-left": ["text alignment", "left"],
  "align-right": ["text alignment", "right"],
  "anchor": [],
  "archive": ["index", "box"],
  "at-sign": ["mention", "at", "email", "message"],
  "award": ["achievement", "badge"],
  "aperture": ["camera", "photo"],
  "bar-chart": ["statistics", "diagram", "graph"],
  "bar-chart-2": ["statistics", "diagram", "graph"],
  "battery": ["power", "electricity"],
  "battery-charging": ["power", "electricity"],
  "bell": ["alarm", "notification", "sound"],
  "bell-off": ["alarm", "notification", "silent"],
  "bluetooth": ["wireless"],
  "book-open": ["read", "library"],
  "book": ["read", "dictionary", "booklet", "magazine", "library"],
  "bookmark": ["read", "clip", "marker", "tag"],
  "box": ["cube"],
  "briefcase": ["work", "bag", "baggage", "folder"],
  "calendar": ["date"],
  "camera": ["photo"],
  "cast": ["chromecast", "airplay"],
  "chevron-down": ["expand"],
  "chevron-up": ["collapse"],
  "circle": ["off", "zero", "record"],
  "clipboard": ["copy"],
  "clock": ["time", "watch", "alarm"],
  "cloud-drizzle": ["weather", "shower"],
  "cloud-lightning": ["weather", "bolt"],
  "cloud-rain": ["weather"],
  "cloud-snow": ["weather", "blizzard"],
  "cloud": ["weather"],
  "codepen": ["logo"],
  "codesandbox": ["logo"],
  "code": ["source", "programming"],
  "coffee": ["drink", "cup", "mug", "tea", "cafe", "hot", "beverage"],
  "columns": ["layout"],
  "command": ["keyboard", "cmd", "terminal", "prompt"],
  "compass": ["navigation", "safari", "travel", "direction"],
  "copy": ["clone", "duplicate"],
  "corner-down-left": ["arrow", "return"],
  "corner-down-right": ["arrow"],
  "corner-left-down": ["arrow"],
  "corner-left-up": ["arrow"],
  "corner-right-down": ["arrow"],
  "corner-right-up": ["arrow"],
  "corner-up-left": ["arrow"],
  "corner-up-right": ["arrow"],
  "cpu": ["processor", "technology"],
  "credit-card": ["purchase", "payment", "cc"],
  "crop": ["photo", "image"],
  "crosshair": ["aim", "target"],
  "database": ["storage", "memory"],
  "delete": ["remove"],
  "disc": ["album", "cd", "dvd", "music"],
  "dollar-sign": ["currency", "money", "payment"],
  "droplet": ["water"],
  "edit": ["pencil", "change"],
  "edit-2": ["pencil", "change"],
  "edit-3": ["pencil", "change"],
  "eye": ["view", "watch"],
  "eye-off": ["view", "watch", "hide", "hidden"],
  "external-link": ["outbound"],
  "facebook": ["logo", "social"],
  "fast-forward": ["music"],
  "figma": ["logo", "design", "tool"],
  "file-minus": ["delete", "remove", "erase"],
  "file-plus": ["add", "create", "new"],
  "file-text": ["data", "txt", "pdf"],
  "film": ["movie", "video"],
  "filter": ["funnel", "hopper"],
  "flag": ["report"],
  "folder-minus": ["directory"],
  "folder-plus": ["directory"],
  "folder": ["directory"],
  "framer": ["logo", "design", "tool"],
  "frown": ["emoji", "face", "bad", "sad", "emotion"],
  "gift": ["present", "box", "birthday", "party"],
  "git-branch": ["code", "version control"],
  "git-commit": ["code", "version control"],
  "git-merge": ["code", "version control"],
  "git-pull-request": ["code", "version control"],
  "github": ["logo", "version control"],
  "gitlab": ["logo", "version control"],
  "globe": ["world", "browser", "language", "translate"],
  "hard-drive": ["computer", "server", "memory", "data"],
  "hash": ["hashtag", "number", "pound"],
  "headphones": ["music", "audio", "sound"],
  "heart": ["like", "love", "emotion"],
  "help-circle": ["question mark"],
  "hexagon": ["shape", "node.js", "logo"],
  "home": ["house", "living"],
  "image": ["picture"],
  "inbox": ["email"],
  "instagram": ["logo", "camera"],
  "key": ["password", "login", "authentication", "secure"],
  "layers": ["stack"],
  "layout": ["window", "webpage"],
  "life-buoy": ["help", "life ring", "support"],
  "link": ["chain", "url"],
  "link-2": ["chain", "url"],
  "linkedin": ["logo", "social media"],
  "list": ["options"],
  "lock": ["security", "password", "secure"],
  "log-in": ["sign in", "arrow", "enter"],
  "log-out": ["sign out", "arrow", "exit"],
  "mail": ["email", "message"],
  "map-pin": ["location", "navigation", "travel", "marker"],
  "map": ["location", "navigation", "travel"],
  "maximize": ["fullscreen"],
  "maximize-2": ["fullscreen", "arrows", "expand"],
  "meh": ["emoji", "face", "neutral", "emotion"],
  "menu": ["bars", "navigation", "hamburger"],
  "message-circle": ["comment", "chat"],
  "message-square": ["comment", "chat"],
  "mic-off": ["record", "sound", "mute"],
  "mic": ["record", "sound", "listen"],
  "minimize": ["exit fullscreen", "close"],
  "minimize-2": ["exit fullscreen", "arrows", "close"],
  "minus": ["subtract"],
  "monitor": ["tv", "screen", "display"],
  "moon": ["dark", "night"],
  "more-horizontal": ["ellipsis"],
  "more-vertical": ["ellipsis"],
  "mouse-pointer": ["arrow", "cursor"],
  "move": ["arrows"],
  "music": ["note"],
  "navigation": ["location", "travel"],
  "navigation-2": ["location", "travel"],
  "octagon": ["stop"],
  "package": ["box", "container"],
  "paperclip": ["attachment"],
  "pause": ["music", "stop"],
  "pause-circle": ["music", "audio", "stop"],
  "pen-tool": ["vector", "drawing"],
  "percent": ["discount"],
  "phone-call": ["ring"],
  "phone-forwarded": ["call"],
  "phone-incoming": ["call"],
  "phone-missed": ["call"],
  "phone-off": ["call", "mute"],
  "phone-outgoing": ["call"],
  "phone": ["call"],
  "play": ["music", "start"],
  "pie-chart": ["statistics", "diagram"],
  "play-circle": ["music", "start"],
  "plus": ["add", "new"],
  "plus-circle": ["add", "new"],
  "plus-square": ["add", "new"],
  "pocket": ["logo", "save"],
  "power": ["on", "off"],
  "printer": ["fax", "office", "device"],
  "radio": ["signal"],
  "refresh-cw": ["synchronise", "arrows"],
  "refresh-ccw": ["arrows"],
  "repeat": ["loop", "arrows"],
  "rewind": ["music"],
  "rotate-ccw": ["arrow"],
  "rotate-cw": ["arrow"],
  "rss": ["feed", "subscribe"],
  "save": ["floppy disk"],
  "scissors": ["cut"],
  "search": ["find", "magnifier", "magnifying glass"],
  "send": ["message", "mail", "email", "paper airplane", "paper aeroplane"],
  "settings": ["cog", "edit", "gear", "preferences"],
  "share-2": ["network", "connections"],
  "shield": ["security", "secure"],
  "shield-off": ["security", "insecure"],
  "shopping-bag": ["ecommerce", "cart", "purchase", "store"],
  "shopping-cart": ["ecommerce", "cart", "purchase", "store"],
  "shuffle": ["music"],
  "skip-back": ["music"],
  "skip-forward": ["music"],
  "slack": ["logo"],
  "slash": ["ban", "no"],
  "sliders": ["settings", "controls"],
  "smartphone": ["cellphone", "device"],
  "smile": ["emoji", "face", "happy", "good", "emotion"],
  "speaker": ["audio", "music"],
  "star": ["bookmark", "favorite", "like"],
  "stop-circle": ["media", "music"],
  "sun": ["brightness", "weather", "light"],
  "sunrise": ["weather", "time", "morning", "day"],
  "sunset": ["weather", "time", "evening", "night"],
  "tablet": ["device"],
  "tag": ["label"],
  "target": ["logo", "bullseye"],
  "terminal": ["code", "command line", "prompt"],
  "thermometer": ["temperature", "celsius", "fahrenheit", "weather"],
  "thumbs-down": ["dislike", "bad", "emotion"],
  "thumbs-up": ["like", "good", "emotion"],
  "toggle-left": ["on", "off", "switch"],
  "toggle-right": ["on", "off", "switch"],
  "tool": ["settings", "spanner"],
  "trash": ["garbage", "delete", "remove", "bin"],
  "trash-2": ["garbage", "delete", "remove", "bin"],
  "triangle": ["delta"],
  "truck": ["delivery", "van", "shipping", "transport", "lorry"],
  "tv": ["television", "stream"],
  "twitch": ["logo"],
  "twitter": ["logo", "social"],
  "type": ["text"],
  "umbrella": ["rain", "weather"],
  "unlock": ["security"],
  "user-check": ["followed", "subscribed"],
  "user-minus": ["delete", "remove", "unfollow", "unsubscribe"],
  "user-plus": ["new", "add", "create", "follow", "subscribe"],
  "user-x": ["delete", "remove", "unfollow", "unsubscribe", "unavailable"],
  "user": ["person", "account"],
  "users": ["group"],
  "video-off": ["camera", "movie", "film"],
  "video": ["camera", "movie", "film"],
  "voicemail": ["phone"],
  "volume": ["music", "sound", "mute"],
  "volume-1": ["music", "sound"],
  "volume-2": ["music", "sound"],
  "volume-x": ["music", "sound", "mute"],
  "watch": ["clock", "time"],
  "wifi-off": ["disabled"],
  "wifi": ["connection", "signal", "wireless"],
  "wind": ["weather", "air"],
  "x-circle": ["cancel", "close", "delete", "remove", "times", "clear"],
  "x-octagon": ["delete", "stop", "alert", "warning", "times", "clear"],
  "x-square": ["cancel", "close", "delete", "remove", "times", "clear"],
  "x": ["cancel", "close", "delete", "remove", "times", "clear"],
  "youtube": ["logo", "video", "play"],
  "zap-off": ["flash", "camera", "lightning"],
  "zap": ["flash", "camera", "lightning"],
  "zoom-in": ["magnifying glass"],
  "zoom-out": ["magnifying glass"]
}

export function showIconsList(){
  const keys = Object.keys(json)
  const arr = []

  keys.forEach(key => {
    arr.push({
      name: key,
      link: `https://b2brqst.com/icons/${key}.svg`,
      searchArray: json[key]
    })
  })

  console.log(arr)
  return arr
}

