export const menus = [
  {
    name: 'Information',
    path: '/',
    icon: 'bx bx-info-circle'
  },
  {
    name: 'Companies',
    path: '/companies',
    icon: 'bx bx-home-circle'
  },
  {
    name: 'Forms',
    path: '/forms',
    icon: 'bx bx-list-check'
  },
  {
    name: 'Requests',
    path: '/requests',
    icon: 'bx bx-receipt',
    notif: 11
  },
  {
    name: 'Agents',
    path: '/users',
    icon: 'bx bx-user-circle'
  },
  {
    name: 'Chats',
    path: '/chats',
    icon: 'bx bx-conversation'
  },
  {
    name: 'Alerts',
    path: '/alerts',
    icon: 'bx bx-bell'
  }
]

const anotherLinks = [
  {
    name: 'Company Add',
    path: '/companies/create',
    icon: ''
  },
  {
    name: 'Form Add',
    path: '/forms/create',
    icon: ''
  },
  {
    name: 'Agent Add',
    path: '/users/create',
    icon: ''
  },
  {
    name: 'Icons',
    path: '/icons',
    icon: ''
  },
  {
    name: 'Profile',
    path: '/profile',
    icon: ''
  },
]

export const getTitle = () => {
  const defTitle = 'B2B Request'
  let path = window.location.href.toLowerCase()
  if (path.includes('#')) {
    path = path.split('#')[0];
  }

  const arr = [...menus, ...anotherLinks]
  if (path.endsWith('client/') || path.endsWith('client')) {
    const find = arr.find(i => i.path === '/')
    if(find && find.name) return find.name
    return defTitle
  }
  console.log('path', path)


  const find = arr.find(menuItem => path.includes(menuItem.path.toLowerCase()) && menuItem.path !== '/')
  if(find && find?.name) return find.name

  return defTitle
}
