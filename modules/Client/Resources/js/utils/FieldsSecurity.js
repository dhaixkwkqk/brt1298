import { generateKey } from "openpgp"
import * as openpgp from "openpgp"
import { router } from "@inertiajs/vue3"
import route from "ziggy-js"
import { toast } from "vue3-toastify"
import {fields} from "@/types/FormFieldsTypes";

export class FieldsSecurity {
  publicKey = null
  _privateKey = null

  constructor() {
    const res = localStorage.getItem('keys')
    if(res){
      const { publicKey, privateKey } = JSON.parse(res)


      console.log('get public token', publicKey, privateKey)

      this.publicKey = publicKey
      this._privateKey = privateKey
    }
  }

  setKey(publicKey, privateKey, DBKey, silent){
    console.log(publicKey, privateKey, DBKey)
    if(DBKey){
      if(!publicKey) {
        toast.error('Public key not found')
        return null;
      }
      if(DBKey.trim() !== publicKey.trim()){
        toast.error('Public key does not match')
        return null;
      }
    }

    this.publicKey = publicKey
    this._privateKey = privateKey
    localStorage.setItem('keys', JSON.stringify({publicKey, privateKey}))
    if(!silent){
      toast.success('Keys installed')
    }
  }

  async checkKeys({publicKey, user}) {
    let privateKey = null

    if(!publicKey){
      // DB doest have keys - generete new
      const keys = await this.generateKeys(user)
      await router.post(route('client.client.editPublicKey'), {public_key_data: keys.publicKey})
      publicKey = keys.publicKey
      privateKey = keys.privateKey
      this.setKey(publicKey, privateKey)
    }else{
      let localKeys = localStorage.getItem('keys')
      if(localKeys){
        // DB have keys and we have in localstorage
        localKeys = JSON.parse(localKeys)
        if(!localKeys.publicKey || !localKeys.privateKey){
          toast.warn('Incorrect keys in profile settings, please check your keys')
          return null
        }
      }else{
        // DB have keys and we dont have in LS
        toast.warn('Incorrect keys in profile settings, please write your keys')
      }
    }
  }

  async generateKeys(user) {
    try {
      const res =  await generateKey({ curve: 'brainpoolP512r1',  userIDs: [user] });
      toast.success('Your keys have been generated, please save them in your profile');
      return res
    }catch (err){
      toast.error(err);
    }
  }

  async restoreKeys(user){
    try {
      const keys = await this.generateKeys(user)
      await router.post(route('client.client.editPublicKey'), {public_key_data: keys.publicKey})
      const publicKey = keys.publicKey
      const privateKey = keys.privateKey
      this.setKey(publicKey, privateKey)
    }catch (err){
      console.error('restoreKeys:', err)
    }
  }

  makeFieldsToRef(fields){
    return fields.map(el => ({...el, value: ''}))
  }

  getFieldsForForm(fields){
    return fields.map(el => ({
      value: el.value,
      icon: el.icon,
      id: el.id,
      label: el.label,
    }))
  }

  async encryptFieldsInUseForm(form){
    console.log('ff', form)
    if(form.fields){

        let newFields = [];
        form.fields.forEach(function (item, index){
            if(item.value.type === 'image/png' || item.value.type === 'image/jpg'){
                let data = item;
                let file = {
                    'name' : data.value.name,
                    'size' : data.value.size,
                    'type' : data.value.type,
                };
                delete  data.value
                let fields = {
                    ...data,
                    'value' : file
                };
                newFields.push(fields)
            }else {
                newFields.push(item)
            }
        });
      try {
           const text = (newFields.length > 0) ? JSON.stringify(newFields) : JSON.stringify(form.fields)
           // const text =  JSON.stringify(form.fields)
           const res = await this.encryptData( text )
           return {
               ...form,
               fields_encrypted: res
           }
      }catch (err){
        console.log('encryptFieldsInUseForm-error', err)
      }
    }
  }

  getBaseKeys(){
    return {privateKey: this._privateKey, publicKey: this.publicKey}
  }
  async getKeys(){
    console.log('start getKeys', this.publicKey,  this._privateKey)
    const publicKeyEnd = await openpgp.readKey({ armoredKey: this.publicKey });
    console.log('start publicKeyEnd:', publicKeyEnd)
    const privateKeyEnd = await openpgp.readPrivateKey({ armoredKey: this._privateKey })
    console.log('start publicKeyEnd:', privateKeyEnd)

    return {publicKeyEnd, privateKeyEnd}
  }

  errorReadMessage = 'Error read'
  async readData(encrypted){
    try {
      const { publicKeyEnd, privateKeyEnd } = await this.getKeys()

      const message = await openpgp.readMessage({
        armoredMessage: encrypted
      });
      const { data: decrypted, signatures } = await openpgp.decrypt({
        message,
        verificationKeys: publicKeyEnd,
        decryptionKeys: privateKeyEnd
      });

      try {
        await signatures[0].verified;
        return {
          error: false,
          data: JSON.parse(decrypted),
        }
      } catch (e) {
        throw new Error('Signature could not be verified: ' + e.message);
      }
    }catch (err){
      console.log('readData-error', err)
    }
    return {
      error: true,
      message: this.errorReadMessage,
      data: null
    }
  }

  async encryptData(text){
    try {
      const { publicKeyEnd, privateKeyEnd } = await this.getKeys()

      const message = await openpgp.createMessage({ text });

      const encrypted = await openpgp.encrypt({
        message: message, // input as Message object
        encryptionKeys: publicKeyEnd,
        signingKeys: privateKeyEnd // optional
      });

      if(encrypted) return encrypted
      return null
    }catch (err){
      console.log('123',err)
    }
  }

  saveKeys(){
    const blob = new Blob([this.publicKey + '\n\n\n' + this._privateKey], { type: 'text/plain' });
    const a = document.createElement('a');
    a.href = URL.createObjectURL(blob);
    a.download = 'keys';
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(a.href);
  }


  async encryptFile(file) {
    const { publicKeyEnd, privateKeyEnd } = await this.getKeys()

    const options = {
      message: await openpgp.createMessage({ binary: file }),
      publicKeys: publicKeyEnd
    };

    const encrypted = await openpgp.encrypt(options);


  }
  async readFileAsArrayBuffer(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = (event) => {
        resolve(event.target.result);
      };

      reader.onerror = (error) => {
        reject(error);
      };

      reader.readAsArrayBuffer(file);
    });
  }
}

