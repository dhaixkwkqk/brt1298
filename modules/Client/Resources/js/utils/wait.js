const wait = async (milliseconds = 150) => {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
};

export default wait;
