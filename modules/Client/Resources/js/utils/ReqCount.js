import {ref} from "vue"
export class ReqCount {
    objRequestCount = ref(0)
    constructor() {
        const res = localStorage.getItem('RequestCount')
        if(res){
            this.objRequestCount.value = res
        }
    }

    setNewCount(value) {
        localStorage.setItem('RequestCount', value)
        this.objRequestCount.value = value;
    }

    getCount() {
        return this.objRequestCount.value;
    }

}
