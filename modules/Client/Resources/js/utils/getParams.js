export function getAllURLParameters() {
  const searchParams = new URLSearchParams(window.location.search);
  const parameters = {};

  // Переберите все параметры и сохраните их в объект
  for (const [key, value] of searchParams.entries()) {
    parameters[key] = value;
  }

  console.log(searchParams.entries())

  return parameters;
}
