<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Igoshev\Captcha\Facades\Captcha;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Modules\Admin\Http\Controllers\LocaleController;
use Modules\Client\Http\Controllers\ProfileController;

Route::get('/client/auth', function (){
    if (Auth::check()) {
        return redirect()->back();
    }else{
        return Inertia::render('Auth/Login');
    }
} )->name('client.auth');

Route::prefix('client')
    ->name('client.')
    ->middleware(['client.only', 'auth.client.redirect:sanctum'])
    ->group(function () {
        Route::get('/', [\Modules\Client\Http\Controllers\HomeController::class, 'index'])
            ->name('home');

        Route::get('/info', [ProfileController::class, 'getCurUser'])
            ->name('profile.info');
        Route::resource('companies', \Modules\Client\Http\Controllers\CompaniesController::class);
        Route::post('/companies/{company}/changeActiveStatusCompany', [Modules\Client\Http\Controllers\CompaniesController::class, 'changeActiveStatusCompany'])->name('companies.changeActiveStatusCompany');

        Route::resource('forms', \Modules\Client\Http\Controllers\FormsController::class);
        Route::post('/forms/{form}/changeActiveStatusForm', [Modules\Client\Http\Controllers\FormsController::class, 'changeActiveStatusForm'])->name('forms.changeActiveStatusForm');
        Route::resource('requests', \Modules\Client\Http\Controllers\RequestsController::class);
        Route::post('/requests/{formRequest}/changeRequestStatus', [Modules\Client\Http\Controllers\RequestsController::class, 'changeRequestStatus'])->name('requests.changeRequestStatus');
        Route::resource('users', \Modules\Client\Http\Controllers\UsersController::class);
        Route::post('/users/{user}/changeActiveStatusUser', [Modules\Client\Http\Controllers\UsersController::class, 'changeActiveStatusUser'])->name('users.changeActiveStatusUser');

        Route::resource('alerts', \Modules\Client\Http\Controllers\AlertsController::class);
        Route::resource('profile', \Modules\Client\Http\Controllers\ProfileController::class);
        Route::resource('chats', \Modules\Client\Http\Controllers\ChatsController::class);
        Route::resource('icons', \Modules\Client\Http\Controllers\IconsController::class);

        Route::post('/openpgp_public_key', [\Modules\Client\Http\Controllers\ClientController::class, 'addOrUpdatePGPKey'])->name('client.editPublicKey');

    });

Route::fallback(fn() => Inertia::render('404'));
