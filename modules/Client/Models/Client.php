<?php

namespace Modules\Client\Models;

use App\Models\Company\Company;
use App\Models\Form\Form;
use App\Models\Tariff;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Agent\Models\Agent;
use Parental\HasParent;

class Client extends User
{
    use HasParent;

    protected $fillable = [
        'name',
        'username',
        'password',
        'type',
        'parent_id',
        'openpgp_public_key',
        'comment',
        'client_id',
        'access_key',
        'tariff_id',
    ];

    public function agents(): HasMany
    {
        return $this->hasMany(Agent::class, 'parent_id');
    }

    public function companies(): HasMany
    {
        return $this->hasMany(Company::class);
    }

    public function forms(): HasMany
    {
        return $this->hasMany(Form::class);
    }

    public function tariff(): BelongsTo
    {
        return $this->belongsTo(Tariff::class, 'tariff_id');
    }
}
