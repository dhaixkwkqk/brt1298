<?php

namespace Modules\Client\Actions\Upload;

use Illuminate\Support\Facades\Date;
use Modules\Client\Http\Requests\Upload\UploadRequest;
use Modules\Client\Http\Resources\UploadCollection;
use Modules\Client\Models\Client;

readonly class UploadFile
{
    public function __construct(protected Client $client)
    {
    }

    public function run(UploadRequest $request): UploadCollection
    {
        $fileEncrypted = $request->file('file_encrypted');
        $checksum = $request->get('checksum');
        $dirPath = 'client-'.$this->client->id.'-'.$this->client->username.'/'.uniqid();

        $fileName = $fileEncrypted->getClientOriginalName();
        if (mb_substr($fileName, -4) !== '.pgp') {
            $fileName .= '.pgp';
        }

        $upload = $this->client
            ->uploads()
            ->whereChecksum($checksum)
            ->first();
        if ($upload) {
            if ($upload->status !== 1) {
                $path = $fileEncrypted->storePubliclyAs($dirPath, $fileName, ['disk' => 'public']);

                $upload->update([
                    'path' => $path,
                    'expired_at' => Date::now()->addHour(),
                    'status' => 1,
                ]);
            } elseif ($upload->expired_at) {
                $upload->update([
                    'expired_at' => Date::now()->addHour(),
                ]);
            }
        } else {
            $path = $fileEncrypted->storePubliclyAs($dirPath, $fileName, ['disk' => 'public']);

            $upload = $this->client->uploads()->create([
                'checksum' => $checksum,
                'disk' => 'public',
                'path' => $path,
                'expired_at' => Date::now()->addHour(),
                'status' => 1,
            ]);
            $upload->save();
        }

        return new UploadCollection([$upload]);
    }
}
