<?php

namespace Modules\Client\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MatchClientOldPassword implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $captchaValidate = Hash::check($value, Auth::user()->getAuthPassword());
        if(!$captchaValidate){
            $fail('The :attribute does not match the old password.');
        }
    }
}
