<?php

namespace Modules\Client\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Client\Models\Client;

class ClientOnlyMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if (!($user instanceof Client)) {
            Auth::guard('web')->logout();
            throw new AuthorizationException('Client zone');
        }else{
            return $next($request);
        }
    }
}
