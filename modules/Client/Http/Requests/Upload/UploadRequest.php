<?php

namespace Modules\Client\Http\Requests\Upload;

use Illuminate\Validation\Rules\File;
use Modules\Client\Http\Requests\ClientRequest;

class UploadRequest extends ClientRequest
{
    public function rules(): array
    {
        return [
            'file_encrypted' => [
                'required',
                File::types(['pgp'])->max(1024 * 5 * 10)
            ],
            'checksum' => [
                'required',
                'string',
                'regex:/^([a-f0-9]{64})$/',
            ],
        ];
    }
}
