<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Client\Rules\MatchClientOldPassword;

class ClientProfileUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'username' => ['string', 'min:4'],
            'old_password' => [
                new MatchClientOldPassword()
            ],
            'password' => [ 'confirmed'],
        ];
    }

    public function messages()
    {
        return [
            'username.string' => 'The :attribute field must be a string.',
            'password.string' => 'The :attribute field must be a string.',

            'name.min' => 'The :attribute field must be at least 1 characters long.',
            'name.max' => 'The :attribute field must not exceed 30 characters.',

            'username.min' => 'The :attribute field must be at least 4 characters long.',

//            'username.required_with' => 'You did not fill in the Client ID',
//            'password.required_with' => 'You did not fill in the Access KEY',

            'password.min' => 'The :attribute field must be at least 8 characters long.',
            'password.confirmed' => 'You have not confirmed your password correctly',
        ];
    }
}
