<?php

namespace Modules\Client\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function uploadFile(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');

            // Генерируйте уникальное имя для файла
            $fileName = uniqid() . '_' . $file->getClientOriginalName();

            // Переместите файл в директорию для загрузки (например, "public/uploads")
            $file->move(public_path('uploads'), $fileName);

            // Верните путь к загруженному файлу, чтобы сохранить его в базе данных или выполнять другие действия
            $filePath = 'uploads/' . $fileName;

            return response()->json(['message' => 'Файл успешно загружен', 'filePath' => $filePath], 200);
        }

        return response()->json(['message' => 'Файл не был загружен'], 400);
    }
}
