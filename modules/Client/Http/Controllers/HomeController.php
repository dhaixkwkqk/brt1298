<?php

namespace Modules\Client\Http\Controllers;

use App\Models\Notification;
use App\Models\Page;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        $pages = Page::where('show', true)->select('id', 'title', 'description')->get();
        $notification =  Notification::with(['message', 'recipient'])->whereHas('recipient', function ($query) {
            $query->where('recipient_id', Auth::id());
        })->get();

        return Inertia::render('Home/Index', [
            'AuthUser' => Auth::user(),
            'Pages' =>  $pages,
            'Notifications' =>  $notification,
        ]);
    }
}
