<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Chat\Chat;
use App\Models\Company\Company;
use App\Models\FormRequest\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Modules\Agent\Models\Agent;

class ChatsController extends Controller
{
    public function index(Request $request)
    {
        $agents = [];
        $formRequests = [];
        $chat = null;

        if ($request->has('company_id')) {
            $companyId = $request->get('company_id');
            $agents = Agent::whereHas('companies', function ($query) use ($companyId) {
                $query->where('id', $companyId);
            })->get();
        }

        if ($request->has('agent_id')) {
            $agentId = $request->get('agent_id');
            $formRequests = FormRequest::whereHas('agent', function ($query) use ($agentId) {
                $query->where('id', $agentId);
            })->with('form.fields')->get();
        }

        if ($request->has('chat_id')) {
            $chatId = $request->get('chat_id');
            $chat = Chat::whereId($chatId)->first();
        }

        return Inertia::render('Chats/Index', [
            'chats' => Company::whereUserId(Auth::id())->get(),
            'companies' => Company::all(),
            'agents' => $agents,
            'formRequests' => $formRequests,
            'chat' => $chat,
        ]);
    }

    public function create()
    {
        return Inertia::render('Chats/Add');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'company_name' => ['required', 'max:50'],
            'company_comment' => ['required', 'max:50'],
            'logo' => ['required'],
            'tariff' => ['required'],
        ]);

        $originalName = $request->logo->getClientOriginalName();
        $path = "/client/chats/" . $originalName;

        Storage::disk('public')->put($path, file_get_contents($request->logo));

        Company::create([
            'user_id' => Auth::id(),
            'name' => $validated['company_name'],
            'comment' => $validated['company_comment'],
            'logo_path' => $path,
            'tariff_id' => $validated['tariff'],
        ]);

        return to_route('client.Chats.index');
    }
}
