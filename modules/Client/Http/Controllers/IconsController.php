<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class IconsController extends Controller
{
    public function index()
    {
        return Inertia::render('Icons/Index', [
            'icons' => Company::whereUserId(Auth::id())->get(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Icons/Add');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'company_name' => ['required', 'max:50'],
            'company_comment' => ['required', 'max:50'],
            'logo' => ['required'],
            'tariff' => ['required'],
        ]);

        $originalName = $request->logo->getClientOriginalName();
        $path = "/client/icons/" . $originalName;

        Storage::disk('public')->put($path, file_get_contents($request->logo));

        Company::create([
            'user_id' => Auth::id(),
            'name' => $validated['company_name'],
            'comment' => $validated['company_comment'],
            'logo_path' => $path,
            'tariff_id' => $validated['tariff'],
        ]);

        return to_route('client.Icons.index');
    }
}
