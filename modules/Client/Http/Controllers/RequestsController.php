<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Chat\Chat;
use App\Models\Company\Company;
use App\Models\Form\Form;
use App\Models\FormRequest\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Modules\Agent\Models\Agent;

class RequestsController extends Controller
{
    public function index(Request $request)
    {
        $formRequestQuery = FormRequest::query()
            ->with('user')
            ->with('form')->whereUserId(Auth::id())
            ->with('company');

        if ($request->has('uid')) {
            $formRequestQuery = $formRequestQuery->where('uid', 'like', '%' . $request->get('uid') . '%');
        }

        if ($request->has('status')) {
            $formRequestQuery = $formRequestQuery->where(
                'status',
                '=',
                array_search(lcfirst($request->get('status')), FormRequest::$statuses)
            );
        }

        if ($request->has('user_id')) {
            $formRequestQuery = $formRequestQuery->where(
                'user_id',
                '=',
                $request->get('user_id')
            );
        }

        $users = Agent::query();

        if ($request->has('username')) {
            $users = $users->where(
                'name',
                'like',
                '%' . $request->get('username') . '%'
            );
        }

        return Inertia::render('Requests/Index', [
            'requests' => $formRequestQuery->orderBy('created_at', 'DESC')->get(),
            'users' => $users->get(),
        ]);
    }

    /**
     * @param int $id
     * @return \Inertia\Response
     */
    public function show(int $id): \Inertia\Response
    {
        return Inertia::render('Requests/Show', [
            'request' => FormRequest::whereId($id)
                ->with('user')
                ->with('form.fields.element')
                ->with('company')
                ->first(),
        ]);
    }

    public function create(Request $request)
    {
        $forms = [];
        $companyId = null;

        if ($request->has('company_id')) {
            $companyId = $request->get('company_id');
            $forms = Form::whereHas('company', function ($query) use ($companyId) {
                $query->where('id', $companyId);
            })->with('fields.element')->get();
        }

        return Inertia::render('Requests/Add', [
            'companies' => Company::whereUserId(Auth::id())->get(),
            'companyId' => $companyId,
            'forms' => $forms,
            'users' => Agent::all(),
        ]);
    }

    public function store(Request $requests)
    {
        $validated = $requests->validate([
            'company' => ['required', 'integer'],
            'form' => ['required', 'integer'],
            'status' => ['required', 'integer'],
            'price' => ['required', 'numeric'],
            'user' => ['required', 'integer'],
            'fields_encrypted' => ['required'],
        ]);

        $chat = Chat::create([
            'uid' => $this->generateChatUid(),
        ]);

        FormRequest::create([
            'uid' => $this->generateUid(),
            'user_id' => Auth::id(),
            'agent_id' => $validated['user'],
            'company_id' => $validated['company'],
            'form_id' => $validated['form'],
            'price' => $validated['price'],
            'chat_id' => $chat->id,
            'fields_encrypted' => $validated['fields_encrypted'],
            'status' => FormRequest::$statuses[$validated['status']],
        ]);

        return to_route('client.requests.index');
    }

    public function edit(Request $request, $id)
    {
        $forms = [];
        $companyId = null;

        if ($request->has('company_id')) {
            $companyId = $request->get('company_id');
            $forms = Form::whereHas('company', function ($query) use ($companyId) {
                $query->where('id', $companyId);
            })->with('fields.element')->get();
        }

        $formRequest = FormRequest::whereId($id)
            ->with('form.fields.element')
            ->first();

        return Inertia::render('Requests/Edit', [
            'formRequest' => $formRequest,
            'companyId' => $companyId,
            'companies' => Company::whereUserId(Auth::id())->get(),
            'forms' => $forms,
            'users' => Agent::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $form = FormRequest::whereId($id)->first();

        $validated = $request->validate([
            'status' => ['required', 'integer'],
            'price' => ['required', 'numeric'],
            'user' => ['required', 'integer'],
            'fields_encrypted' => ['required'],
        ]);

        $form->update([
            'uid' => $this->generateUid(),
            'user_id' => $validated['user'],
            'price' => $validated['price'],
            'fields_encrypted' => $validated['fields_encrypted'],
            'status' => FormRequest::$statuses[$validated['status']],
        ]);

        return to_route('client.requests.index');
    }

    private function generateUid(): string
    {
        $min = 10000;
        $max = 999999;
        $i = 0;

        do {
            $i++;
            $max = $max * $i;
            $uid = 'F' . mt_rand($min, $max);

        } while (FormRequest::whereUid($uid)->count() > 0);

        return $uid;
    }

    private function generateChatUid(): string
    {
        $min = 10000;
        $max = 999999;
        $i = 0;

        do {
            $i++;
            $max = $max * $i;
            $uid = 'F' . mt_rand($min, $max);

        } while (Chat::whereUid($uid)->count() > 0);

        return $uid;
    }

    public function destroy(FormRequest $request): \Illuminate\Http\RedirectResponse
    {
        $request->delete();

        return redirect()->route('client.requests.index')->with('success', 'Request deleted.');
    }

    public function changeRequestStatus(FormRequest $formRequest, Request $request)
    {
        $validated = $request->validate([
            'status' => ['required', 'string'],
        ]);

        $formRequest->status = lcfirst($validated['status']);
        $formRequest->update();
    }
}
