<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\Form\Form;
use App\Models\Tariff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class CompaniesController extends Controller
{
    public function index(Request $request)
    {
        $formsQuery = Company::whereUserId(Auth::id())
            ->with('formRequests')
            ->with('forms')
            ->with('agents')
            ->with('tariff');

        if ($request->has('name')) {
            $formsQuery = $formsQuery->where('name', 'like', '%' . $request->get('name') . '%');
        }

        return Inertia::render('Companies/Index', [
            'companies' => $formsQuery->orderBy('created_at', 'DESC')->get(),
            'tariffs' => Tariff::query()->where('active', true)->get(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Companies/Add', [
            'tariffs' => Tariff::query()->where('active', true)->get(),
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'company_name' => ['required', 'max:255'],
            'company_comment' => ['required', 'max:1024'],
            'logo' => [],
            'tariff' => ['required'],
        ]);

        $data = [
            'user_id' => Auth::id(),
            'name' => $validated['company_name'],
            'comment' => $validated['company_comment'],
            'tariff_id' => $validated['tariff'],
        ];

        if(!empty($validated['logo'])) {
            $originalName = $request->logo->getClientOriginalName();
            $path = "/client/companies/" . $originalName;

            Storage::disk('public')->put($path, file_get_contents($validated['logo']));

            $data['logo_path'] = $path;
        }

        Company::create($data);

        return to_route('client.companies.index');
    }

    public function show(int $id)
    {
        return Inertia::render('Companies/Show', [
            'company' => Company::whereId($id)
                ->with('formRequests')
                ->with('forms')
                ->with('agents')
                ->with('tariff.companies')
                ->first(),
            'tariffs' => Tariff::query()->where('active', true)->get(),
        ]);
    }

    public function edit($id): \Inertia\Response
    {
        return Inertia::render('Companies/Edit', [
            'company' => Company::whereId($id)->first(),
            'tariffs' => Tariff::query()->where('active', true)->get(),
        ]);
    }

    public function update(Company $company, Request $request)
    {
        $validated = $request->validate([
            'company_name' => ['max:255'],
            'company_comment' => ['max:1024'],
            //'logo' => [],
            'tariff' => ['integer'],
        ]);

        $data = [
            'name' => $validated['company_name'],
            'comment' => $validated['company_comment'],
            'tariff_id' => $validated['tariff'],
        ];

       /* if(!empty($validated['logo'])) {
            $originalName = $request->logo->getClientOriginalName();
            $path = "/client/companies/" . $originalName;

            Storage::disk('public')->put($path, file_get_contents($validated['logo']));

            $data['logo_path'] = $path;
        }*/

        $company->update($data);

        return redirect()->route('client.companies.index')->with('success', 'Company updated.');
    }

    public function destroy(Company $company): \Illuminate\Http\RedirectResponse
    {
        $company->forms()->delete();
        $company->formRequests()->delete();
        $company->delete();

        return redirect()->route('client.companies.index')->with('success', 'Company deleted.');
    }

    public function changeActiveStatusCompany(Company $company): \Illuminate\Http\RedirectResponse
    {
        $company->active = !$company->active;
        $company->update();

        $forms = $company->forms;
        foreach ($forms as $form) {
            $form->active = $company->active;
            $form->update();
        }

        $agents = $company->agents;
        foreach ($agents as $agent) {
            $agent->active = $company->active;
            $agent->update();
        }

        return redirect()->route('client.companies.index')->with('success', 'Company active status changed.');
    }
}
