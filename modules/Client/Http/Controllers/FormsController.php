<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\Element\CheckboxElement;
use App\Models\Element\ImageElement;
use App\Models\Element\InputElement;
use App\Models\Element\RadioElement;
use App\Models\Element\SelectElement;
use App\Models\Element\TextareaElement;
use App\Models\Field;
use App\Models\Form\Form;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Modules\Agent\Models\Agent;

class FormsController extends Controller
{
    public function index(Request $request)
    {
        $formsQuery = Form::query()->whereUserId(Auth::id())
            ->with('companies')
            ->with('fields')
            ->with('agents')
            ->with('users');

        if ($request->has('name')) {
            $name = $request->get('name');
            $formsQuery = $formsQuery
                ->where('name', 'like', '%' . $name . '%')
                ->orWhereHas('company', function ($query) use ($name) {
                    $query->where('name', 'like', '%' . $name . '%');
                })
                ->orWhereHas('users', function ($query) use ($name) {
                    $query->where('name', 'like', '%' . $name . '%');
                });
        }

        $companyId = null;
        if ($request->has('company_id')) {
            $companyId = $request->get('company_id');

            $formsQuery->whereHas('companies', function ($query) use ($companyId) {
                $query->where('id', $companyId);
            });
        }

        $user = null;
        if ($request->has('user_id')) {
            $userId = $request->get('user_id');
//            $user = Agent::find($userId);

            // something wrong
            $formsQuery->whereHas('agents', function ($query) use ($userId) {
                $query->where('id', $userId);
            });
        }

        $company = null;
        if ($companyId !== null) {
            $company = Company::whereId($companyId)->first();
        }

        $formsQuery = $formsQuery->orderBy('created_at', 'DESC')->get();

        return Inertia::render('Forms/Index', [
            'forms' => $formsQuery,
            'company' => $company,
            'user' => $user
        ]);

    }

    public function create(Request $request)
    {
        $availableUsers = [];

        if ($request->has('company_id')) {
            $companyId = $request->get('company_id');
            $company = Company::whereId($companyId)->first();
            $availableUsers = $company->agents;
        }

        return Inertia::render('Forms/Add', [
            'companies' => Company::whereUserId(Auth::id())->get(),
            'availableUsers' => $availableUsers,
            'users' => [],
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'companyId' => ['required', 'integer'],
            'formName' => ['required', 'max:255'],
            'fields.*.label' => 'required|string',
            'fields.*.type' => 'required|string',
            'fields.*.element' => 'array',
            'fields.*.icon' => '',
            'users' => 'array',
        ]);

        $form = Form::create([
            'uid' => $this->generateUid(),
            'user_id' => Auth::id(),
            'name' => $validated['formName'],
            'price' => 100,
            'active' => true,
        ]);

        $company = Company::find($validated['companyId']);

        $company->forms()->attach($form);

        $this->attachFieldsToForm($form, $validated);

        $form->users()->detach();
        foreach ($validated['users'] as $key => $value) {
            if ($value === true) {
                $agent = Agent::whereId($key)->first();
                $form->users()->attach($agent);
            }
        }

        return to_route('client.forms.index');
    }

    public function edit($id, Request $request)
    {
        $form = Form::whereId($id)->with('company')->with('fields.element')->first();

        $companyId = $form->company->id;

        if ($request->has('company_id')) {
            $companyId = $request->get('company_id');
        }

        $company = Company::whereId($companyId)->first();
        $availableUsers = $company->agents;

        return Inertia::render('Forms/Edit', [
            'form' => $form,
            'companies' => Company::whereUserId(Auth::id())->get(),
            'availableUsers' => $availableUsers,
            'users' => $form->users->pluck('id'),
        ]);
    }

    public function update(Form $form, Request $request)
    {
        $validated = $request->validate([
            'companyId' => ['required', 'integer'],
            'formName' => ['required', 'max:255'],
            'fields.*.label' => 'required|string',
            'fields.*.type' => 'required|string',
            'fields.*.element' => 'array',
            'fields.*.icon' => '',
            'fields.*.uid' => '',
            'users' => 'array',
        ]);

        $form->update([
            'name' => $validated['formName'],
        ]);

        $company = Company::find($validated['companyId']);
        $form->companies()->detach();
        $company->forms()->attach($form);

        $this->attachFieldsToForm($form, $validated);

        $form->users()->detach();
        foreach ($validated['users'] as $key => $value) {
            if ($value === true) {
                $agent = Agent::whereId($key)->first();
                $form->users()->attach($agent);
            }
        }

        return redirect()->route('client.forms.index')->with('success', 'Form updated.');
    }

    /**
     * @param Form $form
     * @return \Inertia\Response
     */
    public function show(int $id): \Inertia\Response
    {
        $form = Form::whereId($id)
            ->with('companies')
            ->with('agents.user')
            ->with('fields')
            ->with('client')
            ->first();

        return Inertia::render('Forms/Show', [
            'form' => $form,
        ]);
    }

    /**
     * @param Form $form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Form $form): \Illuminate\Http\RedirectResponse
    {
        $form->delete();

        return redirect()->route('client.forms.index')->with('success', 'Form deleted.');
    }

    /**
     * @param Form $form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeActiveStatusForm(Form $form): \Illuminate\Http\RedirectResponse
    {
        if ($form->company->active) {
            $form->active = !$form->active;
            $form->update();

            return redirect()->route('client.forms.index')->with('success', 'Form active status changed.');
        }

        return redirect()->route('client.forms.index')->with('success', 'Form company is not active.');
    }

    /**
     * @return string
     */
    private function generateUid(): string
    {
        $min = 10000;
        $max = 999999;
        $i = 0;

        do {
            $i++;
            $max = $max * $i;
            $uid = 'F' . mt_rand($min, $max);

        } while (Form::whereUid($uid)->count() > 0);

        return $uid;
    }

    /**
     * @param Form $form
     * @param array $validated
     * @return void
     */
    private function attachFieldsToForm(Form $form, array $validated): void
    {
        $form->fields()->detach();

        if (isset($validated['fields'])) {
            foreach ($validated['fields'] as $field) {
                $elementType = 'App\\Models\\Element\\' . $field['type'];

                if (!(isset($field['uid']))) {
                    $fieldModel = Field::create([
                        'uid' => $this->generateUid(),
                        'label' => $field['label'],
                        'icon' => $field['icon'] ?? null,
                        'element_id' => 1,
                        'element_type' => $elementType,
                    ]);
                } else {
                    $fieldModel = Field::whereUid($field['uid'])->first();
                    $fieldModel->label = $field['label'];
                    $fieldModel->icon = $field['icon'] ?? null;
                }

                $elementFromRequest = $field['element'];

                switch ($field['type']) {
                    case 'InputElement':
                        if (!(isset($field['uid']))) {
                            $element = InputElement::create($elementFromRequest);
                        } else {
                            $element = InputElement::find($fieldModel->element_id);
                            $element->update($elementFromRequest);
                        }
                        break;
                    case 'CheckboxElement':
                        if (!(isset($field['uid']))) {
                            $element = CheckboxElement::create([
                                'variants' => []
                            ]);
                        } else {
                            $element = CheckboxElement::find($fieldModel->element_id);
                            $element->update([
                                'variants' => []
                            ]);
                        }
                        break;
                    case 'ImageElement':
                        if (!(isset($field['uid']))) {
                            $element = ImageElement::create($elementFromRequest);
                        } else {
                            $element = ImageElement::find($fieldModel->element_id);
                            $element->update($elementFromRequest);
                        }
                        break;
                    case 'RadioElement':
                        if (!(isset($field['uid']))) {
                            $element = RadioElement::create($elementFromRequest);
                        } else {
                            $element = RadioElement::find($fieldModel->element_id);
                            $element->update($elementFromRequest);
                        }
                        break;
                    case 'SelectElement':
                        if (!(isset($field['uid']))) {
                            $element = SelectElement::create($elementFromRequest);
                        } else {
                            $element = SelectElement::find($fieldModel->element_id);
                            $element->update($elementFromRequest);
                        }
                        break;
                    case 'TextareaElement':
                        if (!(isset($field['uid']))) {
                            $element = TextareaElement::create($elementFromRequest);
                        } else {
                            $element = TextareaElement::find($fieldModel->element_id);
                            $element->update($elementFromRequest);
                        }
                        break;
                }

                $fieldModel->element()->detach();
                $fieldModel->element()->attach($element);
                $fieldModel->element_id = $element->id;
                $fieldModel->save();

                $form->fields()->attach($fieldModel);
            }
        }
    }
}
