<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\MailServer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use Modules\Client\Http\Requests\ClientProfileUpdate;

class ProfileController extends Controller
{
    public function index()
    {
        return Inertia::render('Profile/Index', [
            'profile' => Company::whereUserId(Auth::id())->get(),
            'AuthUser' => Auth::user(),
        ]);
    }

    public function getCurUser(){
        return response()->json(Auth::user());
    }

    public function create()
    {
        return Inertia::render('Profile/Add');
    }

    public function update(ClientProfileUpdate $request, $id){

        $data = $request->validated();

        if(isset($data['password'])){
            $data['password'] = Hash::make($data['password']);
        }
        if($data['old_password']){
            unset($data['old_password']);
        }
        $res = User::where('id',$id)->update($data);

        if($res) return redirect()->back();
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'company_name' => ['required', 'max:50'],
            'company_comment' => ['required', 'max:50'],
            'logo' => ['required'],
            'tariff' => ['required'],
        ]);

        $originalName = $request->logo->getClientOriginalName();
        $path = "/client/profile/" . $originalName;

        Storage::disk('public')->put($path, file_get_contents($request->logo));

        Company::create([
            'user_id' => Auth::id(),
            'name' => $validated['company_name'],
            'comment' => $validated['company_comment'],
            'logo_path' => $path,
            'tariff_id' => $validated['tariff'],
        ]);

        return to_route('client.Profile.index');
    }
}
