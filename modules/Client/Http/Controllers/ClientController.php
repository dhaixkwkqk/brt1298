<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Modules\Client\Models\Client;

class ClientController extends Controller
{
    public function addOrUpdatePGPKey(Request $request)
    {
        $user = Auth::user();
        $publicKeyData = $request->input('public_key_data');

        if ($user) {
            // Обновление информации пользователя
            $user->openpgp_public_key = $publicKeyData;
            $user->save();
        }
        return to_route('home');

    }
}
