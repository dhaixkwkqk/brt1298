<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Client\Actions\Upload\UploadFile;
use Modules\Client\Http\Requests\Upload\UploadRequest;
use Modules\Client\Models\Client;

class UploadController extends Controller
{
    /**
     * Метод загрузки зашифрованного файла.
     * @param  Client  $client
     * @param  UploadRequest  $request
     */
    public function index(Authenticatable $client, UploadRequest $request): array
    {
        $uploadImage = new UploadFile($client);

        return [
            'success' => true,
            'data' => $uploadImage->run($request)->collection->first(),
        ];
    }

    /**
     * Метод подтверждения загрузки файла.
     * Устанавливает метку что бы файл не был удалён сборщиком мусора.
     * @param  Client  $client
     */
    public function complete(Authenticatable $client, Request $request): array
    {
        $validated = $request->validate([
            'uuid' => [
                'required',
                'array',
                function ($attribute, $value, $fail) {
                    foreach ($value as $uuid) {
                        if (!Validator::make(['uuid' => $uuid], [
                            'uuid' => ['required', 'uuid', new Exists(Upload::class, 'uuid')]
                        ])->passes()) {
                            $fail('The '.$attribute.' contains invalid UUID(s).');
                        }
                    }
                },
            ]
        ]);

        foreach ($validated['uuid'] as $uuid) {
            Upload::find($uuid)->update(['expired_at' => null]);
        }

        return [
            'success' => true,
        ];
    }
}
