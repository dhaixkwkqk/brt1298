<?php

namespace Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\Form\Form;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Modules\Agent\Models\Agent;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $agents =  Agent::query()
            ->with('formRequests')
            ->with('companies');
        $form = null;
        $company = null;

        if($request->has('form_id')) {
            $formId = $request->get('form_id');
            $agents = $agents->whereHas('formRequests', function ($query) use ($request) {
                $query->where('form_id', $request->get('form_id'));
            });
            $form = Form::whereId($request->get('form_id'))->with('users.formRequests')->first();
            $agents = $form->users();

            $company = Company::whereHas('forms', function ($query) use ($formId) {
                $query->where('id', $formId);
            })->first();
        }

        return Inertia::render('Users/Index', [
            'users' => $agents->with('formRequests')->orderBy('created_at', 'DESC')->get(),
            'form' => $form,
            'company' => $company
        ]);
    }

    public function create()
    {
        return Inertia::render('Users/Add');
    }


    public function show($id)
    {
        $agents =  Agent::whereId($id)
            ->with('formRequests')
            ->with('companies')
            ->with('forms');

        return Inertia::render('Users/Show', [
            'user' => $agents->first(),
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'company_name' => ['required', 'max:50'],
            'company_comment' => ['required', 'max:50'],
            'logo' => ['required'],
            'tariff' => ['required'],
        ]);

        $originalName = $request->logo->getClientOriginalName();
        $path = "/client/users/" . $originalName;

        Storage::disk('public')->put($path, file_get_contents($request->logo));

        Company::create([
            'user_id' => Auth::id(),
            'name' => $validated['company_name'],
            'comment' => $validated['company_comment'],
            'logo_path' => $path,
            'tariff_id' => $validated['tariff'],
        ]);

        return to_route('client.Users.index');
    }

    public function edit($id)
    {
//        $user = Form::whereId($id)->with('company')->with('fields.element')->first();

        return Inertia::render('Users/Edit', []);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeActiveStatusUser(User $user): \Illuminate\Http\RedirectResponse
    {
        if ($user->company->active) {
            $user->active = !$user->active;
            $user->update();

            return redirect()->route('client.users.index')->with('success', 'User active status changed.');
        }

        return redirect()->route('client.users.index')->with('success', 'User company is not active.');

    }
}
