<?php

namespace App\Casts;

use App\Models\FormRequest\FormRequest;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;

class FormRequestStatusCast implements CastsAttributes
{
    public function get(Model $model, string $key, mixed $value, array $attributes): string
    {
        return FormRequest::$statuses[$value];
    }

    public function set(Model $model, string $key, mixed $value, array $attributes): int
    {
        return array_search($value, FormRequest::$statuses);
    }
}
