<?php

namespace App\Casts;

use App\Models\User;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;

class UserStatusCast implements CastsAttributes
{
    public function get(Model $model, string $key, mixed $value, array $attributes): string
    {
        return User::$statuses[$value];
    }

    public function set(Model $model, string $key, mixed $value, array $attributes): int
    {
        return array_search($value, User::$statuses);
    }
}
