<?php

namespace App\Services\Element;

class BaseValidator implements ValidatorInterface
{
    public function validate(mixed $value): void
    {
    }

    public function transform(mixed $value): array
    {
        return [
            $value
        ];
    }

    public function cast(array $value): mixed
    {
        return $value;
    }
}
