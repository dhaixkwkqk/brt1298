<?php

namespace App\Services\Element;


/**
 * Интерфейс Валидатора Элемента формы
 */
interface ValidatorInterface
{
    public function validate(mixed $value): void;

    public function transform(mixed $value): array;

    public function cast(array $value): mixed;
}
