<?php

namespace App\Services\Element;

class ValidatorException extends \Exception
{
    public function __construct(string $message, protected readonly mixed $field = null)
    {
        parent::__construct($message);
    }

    public function getField(): mixed
    {
        return $this->field;
    }
}
