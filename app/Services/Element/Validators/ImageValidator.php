<?php

namespace App\Services\Element\Validators;

use Modules\Agent\Models\Agent;
use App\Models\Element\ImageElement;
use App\Models\Upload;
use App\Services\Element\BaseValidator;
use App\Services\Element\ValidatorException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ImageValidator extends BaseValidator
{
    public function __construct(protected readonly ImageElement $element)
    {
    }

    public function validate(mixed $value): void
    {
        if (!is_array($value)) {
            throw new \Exception('Must be array.');
        }

        if ($this->element->min_count && count($value) < $this->element->min_count) {
            throw new \Exception("Minimum {$this->element->min_count}-images.");
        }

        if ($this->element->max_count && count($value) > $this->element->max_count) {
            throw new \Exception("Maximum {$this->element->max_count}-images.");
        }

        /** @var Agent $agent */
        $agent = Auth::user();
        if (!($agent instanceof Agent)) {
            throw new \Exception("You not API User.");
        }

        /** @var Upload[] $uploads */
        $uploads = [];
        foreach ($value as $i => $uploadId) {
            $upload = $agent->uploads()->whereUuid($uploadId)->first();
            if (!$upload) {
                throw new ValidatorException("Upload not found.", $i);
            }
            if ($upload->status !== 1) {
                throw new ValidatorException("Upload expired.", $i);
            }
            $uploads[] = $upload;
        }

        foreach ($uploads as $upload) {
            $upload->update(['expired_at' => null]);
        }
    }

    public function transform(mixed $value): array
    {
        return array_values($value);
    }

    public function cast(array $value): mixed
    {
        return array_map(function(string $uploadId) {
            $upload = Upload::whereUuid($uploadId)->first();
            return $upload ? Storage::disk($upload->disk)->url($upload->path) : null;
        }, $value);
    }
}
