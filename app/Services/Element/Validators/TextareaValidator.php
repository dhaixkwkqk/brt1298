<?php

namespace App\Services\Element\Validators;

use App\Models\Element\InputElement;
use App\Models\Element\TextareaElement;
use App\Services\Element\BaseValidator;
use App\Services\Element\ValidatorInterface;

class TextareaValidator extends BaseValidator
{
    public function __construct(protected readonly TextareaElement $element)
    {
    }

    public function validate(mixed $value): void
    {
        if (!is_string($value)) {
            throw new \Exception("It's not string.");
        }

        $value = trim($value);

        if( $this->element->min_length && mb_strlen($value) < $this->element->min_length ) {
            throw new \Exception("MIN length is {$this->element->min_length} symbols.");
        }

        if( $this->element->max_length && mb_strlen($value) > $this->element->max_length ) {
            throw new \Exception("MAX length is {$this->element->max_length} symbols.");
        }
    }

    public function transform(mixed $value): array
    {
        $value = trim($value);

        return [$value];
    }

    public function cast(array $value): mixed
    {
        return $value[0] ?? null;
    }
}
