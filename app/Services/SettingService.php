<?php

namespace App\Services;

use App\Models\SettingItem;

class SettingService
{
    public function get(string $name, mixed $default = null): mixed
    {
        $setting = SettingItem::whereName($name)->first();

        return $setting && $setting->value !== null ? $setting->value : $default;
    }

    public function set(string $name, mixed $value): void
    {
        $setting = SettingItem::whereName($name)->firstOrFail();
        $setting->update(['value' => $value]);
    }
}
