<?php

namespace App\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Http\Requests\CaptchaValidateRequest;
use App\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Fortify;
use Modules\Client\Models\Client;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
//        Fortify::ignoreRoutes();
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);
        Fortify::authenticateUsing(function (Request $request) {
            $data = $request->all();
            //ДА ПРОСТЯТ БОГИ
            if($data['username'] == "client_id" && $data['password'] == "access_key"){
                $data["username"] = null;
                $data["password"] = null;
            }

            if($data['action'] == 'register_client'){
                if(isset($data['client_id']) && $data['client_id'] !== null)
                    $user = \App\Models\User::query()->where('client_id', $data['client_id'])->first();
                if(Hash::check($data['access_key'], $user->access_key)){
                    if(!empty($user->client_id) && !empty($user->access_key) && empty($user->username) && empty($user->password)){
                        $validated = $request->validate([
                            'username' => ['unique:users,username'],
                        ]);

                        $user->username = $data['username'];
                        $user->password = Hash::make($data['password']);
                        $user->save();

                    }
                    return $user;
                }
            }
            elseif ($data['action'] == 'auth_client_id'){
                if(isset($data['client_id']) && $data['client_id'] !== null)
                    $user = \App\Models\User::query()->where('client_id', $data['client_id'])
                        ->first();
                if(Hash::check($data['access_key'], $user->access_key)){
                    return $user;
                }
            }
            elseif ($data['action'] == 'auth_client_login'){
                if(isset($data['username']) && $data['username'] !== null)
                    $user = \App\Models\User::query()->where('username', $data['username'])->first();
                if(Hash::check($data['password'], $user->password)){
                    return $user;
                }
            }
            elseif ($data['action'] == 'auth_admin'){
                if(isset($data['username']) && $data['username'] !== null)
                    $user = \App\Models\User::query()->where('username', $data['username'])->orWhere('email', $data['username'])->first();
                if(Hash::check($data['password'], $user->password)){
                    return $user;
                }
            }
            elseif ($data['action'] == 'auth_client_id_login'){
                if(isset($data['username']) && $data['username'] !== null && isset($data['client_id']) && $data['client_id'] !== null)
                    $user = \App\Models\User::query()
                        ->where('username', $data['username'])
                        ->where('client_id', $data['client_id'])->first();
                if(Hash::check($data['password'], $user->password) && Hash::check($data['access_key'], $user->access_key)){
                    return $user;
                }
            }

        });

        RateLimiter::for('login', function (Request $request) {
            $throttleKey = Str::transliterate(Str::lower($request->input(Fortify::username())).'|'.$request->ip());

            return Limit::perMinute(5)->by($throttleKey);
        });

        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(5)->by($request->session()->get('login.id'));
        });
    }
}
