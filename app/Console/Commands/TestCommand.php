<?php

namespace App\Console\Commands;

use App\Models\SettingItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Modules\Agent\Models\Agent;
use Modules\Client\Models\Client;
use OpenPGP\Message\EncryptedMessage;
use OpenPGP\OpenPGP;

class TestCommand extends Command
{
    protected $signature = 'test';

    protected $description = 'Command description';

    public function handle(): void
    {

        $client = Client::updateOrCreate([
            'username' => 'client',
        ], [
            'name' => 'Client',
            'password' => Hash::make('client'),
            'openpgp_public_key' => File::get(resource_path('openpgp/client.pub')),
        ]);

        $agent = Agent::updateOrCreate([
            'username' => 'agent_1',
        ], [
            'name' => 'Agent 1',
            'password' => Hash::make('agent_1'),
        ]);
        $agent->parent()->associate($client)->save();

        $agent = Agent::updateOrCreate([
            'username' => 'agent_2',
        ], [
            'name' => 'Agent 2',
            'password' => Hash::make('agent_2'),
        ]);
        $agent->parent()->associate($client)->save();

        die('OK');

        $agentPublicKey = trim(File::get(resource_path('openpgp/agent.pub')));
        $agentPrivateKey = trim(File::get(resource_path('openpgp/agent.key')));
        $agentPublicKey = OpenPGP::readPublicKey($agentPublicKey);
        $agentPrivateKey = OpenPGP::decryptPrivateKey($agentPrivateKey, '123456');

        $clientPublicKey = trim(File::get(resource_path('openpgp/client.pub')));
        $clientPrivateKey = trim(File::get(resource_path('openpgp/client.key')));
        $clientPublicKey = OpenPGP::readPublicKey($clientPublicKey);
        $clientPrivateKey = OpenPGP::decryptPrivateKey($clientPrivateKey, '123456');

        $encrypt = (string)OpenPGP::encrypt(OpenPGP::createLiteralMessage('hello'), [$agentPublicKey]);
        echo $encrypt;
        //echo OpenPGP::decrypt(EncryptedMessage::fromArmored($encrypt), [$clientPrivateKey])->getLiteralData()->getData();
        exit();

        $armoredPublicKey = trim(File::get(resource_path('openpgp/admin.pub')));
        $armoredPrivateKey = trim(File::get(resource_path('openpgp/admin.key')));
        $passphrase = '123456';

        $publicKey = OpenPGP::readPublicKey($armoredPublicKey);
        $privateKey = OpenPGP::decryptPrivateKey($armoredPrivateKey, $passphrase);

        $cleartextMessage = OpenPGP::createLiteralMessage('Hello, PHP Privacy!');
        /** @var EncryptedMessage $message */
        $message = (string)OpenPGP::encrypt($cleartextMessage, [$publicKey]);

        $msg = OpenPGP::decrypt(EncryptedMessage::fromArmored($message), [$privateKey])->getLiteralData()->getData();
        echo $msg;
        echo "\nBRO!";

        exit();
        $signedMessage = $cleartextMessage->sign([$privateKey]);
        $verifications = $signedMessage->verify([$publicKey]);
        echo $signedMessage;
        print_r($verifications);
    }
}
