<?php

namespace App\Console\Commands;

use App\Models\Upload;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;

class UploadsCleanerCommand extends Command
{
    protected $signature = 'uploads:cleaner';

    protected $description = 'Remove old uploaded files';

    public function handle(): void
    {
        $expiredUploads = Upload::whereNotNull('expired_at')
            ->where('expired_at', '>', Date::now())
            ->get();

        $expiredUploads->each(function (Upload $upload) {
            Storage::disk($upload->disk)->delete($upload->path);

            $upload->update([
                'status' => 0,
            ]);
        });
    }
}
