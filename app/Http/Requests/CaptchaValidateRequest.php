<?php

namespace App\Http\Requests;

use App\Rules\ValidateCaptcha;
use Illuminate\Foundation\Http\FormRequest;

class CaptchaValidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
//        ValidateCaptcha
        return [
            'captcha' => [
                'required',
                'string',
                'min:4',
                'max:5',
                new ValidateCaptcha()
            ]
        ];
    }

    public function messages()
    {
        return [
            'captcha.required' => 'The :attribute field is required.',
            'captcha.string' => 'The :attribute field must be a string.',
            'captcha.min' => 'The :attribute field must be at least 4 characters long.',
            'captcha.max' => 'The :attribute field must not exceed 5 characters.',
        ];
    }

}
