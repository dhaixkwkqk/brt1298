<?php

namespace App\Http\Requests;

use App\Rules\ValidateCaptcha;
use Illuminate\Foundation\Http\FormRequest;

class AuthClientValidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
//        ValidateCaptcha
        return [
           'access_key' => ['required' , 'string'],
           'client_id' => ['required' , 'string' , 'min:30', 'exists:users,client_id'],
           'username' => ['required' , 'string' , 'min:2' , 'exists:users,username'],
           'password' => ['required' , 'string' , 'min:5'],
        ];
    }

    public function messages()
    {
        return [
            'access_key.required' => 'The :attribute field is required.',
            'access_key.string' => 'The :attribute field must be a string.',

            'client_id.required' => 'The :attribute field is required.',
            'client_id.string' => 'The :attribute field must be a string.',
            'client_id.min' => 'The :attribute field must be at least 30 characters long.',
            'client_id.exists' => 'user with this :attribute is not registered',

            'username.required' => 'The :attribute field is required.',
            'username.string' => 'The :attribute field must be a string.',
            'username.min' => 'The :attribute field must be at least 2 characters long.',
            'username.exists' => 'user with this login is not registered',


            'password.required' => 'The :attribute field is required.',
            'password.string' => 'The :attribute field must be a string.',
            'password.min' => 'The :attribute field must be at least 5 characters long.',

        ];
    }

}
