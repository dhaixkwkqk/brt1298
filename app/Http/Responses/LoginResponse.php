<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;
use Modules\Admin\Models\Admin;
use Modules\Agent\Models\Agent;
use Modules\Client\Models\Client;

class LoginResponse implements LoginResponseContract
{

    public function toResponse($request)
    {
        if($request->input('module')){
            $redirectRoute = match ($request->input('module')) {
                'agent'  => 'agent',
                'client' => 'client.home',
                'admin' => 'admin.client.index',
            };
            return to_route($redirectRoute);
        }
    }
}
