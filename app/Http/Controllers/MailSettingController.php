<?php

namespace App\Http\Controllers;

use App\Models\MailServer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use Inertia\Testing\Concerns\Has;
use Laravel\Fortify\Features;
use Laravel\Fortify\Rules\Password;
use Laravel\Jetstream\Jetstream;
use Illuminate\Support\Facades\Hash;

class MailSettingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $server = MailServer::first();

        return Inertia::render('Mail/Index', [
            'Server' => $server
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $data = Validator::make($request->all(), [
            'ip_server' => ['required', 'string', 'min:8'],
            'port' => ['required', 'integer', 'min:2'],
            'password' => ['min:6', 'string', 'confirmed'],
            'protocol' => ['required', 'string', 'min:4'],
        ])->validate();
        if(isset($data['password'])){
            $data['password'] = Hash::make($data['password']);
        }
        $res = MailServer::where('id',$request->id)->update($data);

        if($res) return redirect()->back();

    }
}
