<?php

namespace App\Http\Controllers;

use App\Models\MailServer;
use App\Models\TelegramBot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use Inertia\Testing\Concerns\Has;
use Laravel\Fortify\Features;
use Laravel\Fortify\Rules\Password;
use Laravel\Jetstream\Jetstream;
use Illuminate\Support\Facades\Hash;

class TelegramSettingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $botSettings = TelegramBot::first();

        return Inertia::render('Telegram/Index', [
            'TelegramBotData' => $botSettings
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {

        $data = Validator::make($request->all(), [
            'telegram_token' => ['required', 'string', 'min:40'],
            'telegram_bot_name' => ['required', 'string', 'min:2'],
        ])->validate();

        $res = TelegramBot::where('id',$request->id)->update($data);

        if($res) return redirect()->back();

    }
}
