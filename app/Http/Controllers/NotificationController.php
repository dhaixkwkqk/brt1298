<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class NotificationController extends Controller
{
    public function readAll(Request $request)
    {
        Notification::query()
            ->whereNull('viewed_at')
            ->whereRecipientId($request->user()->id)
            ->update(['viewed_at' => Date::now()]);

        return ['success' => true];
    }

    public function delete($id, Request $request)
    {
        Notification::query()
            ->whereId($id)
            ->whereRecipientId($request->user()->id)
            ->delete();

        return ['success' => true];
    }
}
