<?php

namespace App\Enums;

enum RecipietsTranslations: string
{
    case Admin = 'admin';
    case Agent = 'agent';
    case Client = 'client';

    public static function getAll(): array
    {
        $arr = [];
        foreach (self::cases() as $item) {
            $arr[] = [
                'name' => $item->name
            ];
        }
        return $arr;
    }
}
