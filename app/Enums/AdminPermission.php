<?php

namespace App\Enums;

enum AdminPermission: string
{
    case Admins = 'admins';
    case Clients = 'clients';
    case Tariffs = 'tariffs';
    case Locales = 'locales';
    case Translations = 'translations';
    case Pages = 'pages';

    case Settings = 'settings';
    case Notifications = 'notifications';
    case CustomerRequests = 'customer requests';

    public static function getAll(): array
    {
        $arr = [];
        foreach (self::cases() as $item) {
            $arr[] = [
                'name' => $item->name,
                'value' => $item->value
            ];
        }
        return $arr;
    }
}
