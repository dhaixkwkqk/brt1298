<?php
namespace App\Actions\Fortify;

use App\Http\Requests\CaptchaValidateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CaptchaValidation
{
    /**
     * @throws ValidationException
     */
    public function __invoke(Request $request, $next)
    {
        $captchaValidateRequest = new CaptchaValidateRequest();
        $validator = Validator::make($request->all(), $captchaValidateRequest->rules(), $captchaValidateRequest->messages());
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
        return $next($request);
    }
}
