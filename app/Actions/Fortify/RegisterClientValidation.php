<?php
namespace App\Actions\Fortify;

use App\Http\Requests\CaptchaValidateRequest;
use App\Http\Requests\RegisterClientValidateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RegisterClientValidation
{
    /**
     * @throws ValidationException
     */
    public function __invoke(Request $request, $next)
    {
        if($request->input('action') === 'register_client'){
            $regClientReq = new RegisterClientValidateRequest();
            $validator = Validator::make($request->all(), $regClientReq->rules(), $regClientReq->messages());
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            return $next($request);
        }else{
            return $next($request);
        }
    }
}
