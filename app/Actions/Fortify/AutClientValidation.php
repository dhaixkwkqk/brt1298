<?php
namespace App\Actions\Fortify;

use App\Http\Requests\AuthClientValidateRequest;
use App\Http\Requests\CaptchaValidateRequest;
use App\Http\Requests\RegisterClientValidateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AutClientValidation
{
    /**
     * @throws ValidationException
     */
    public function __invoke(Request $request, $next)
    {
        if($request->input('action') === 'auth_client_id_login'){
            $regClientReq = new AuthClientValidateRequest();
            $validator = Validator::make($request->all(), $regClientReq->rules(), $regClientReq->messages());
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            return $next($request);
        }else{
            return $next($request);
        }
    }
}
