<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'language_id',
        'current_recipient',
        'is_default'
    ];

    protected $casts = [
        'is_default' => 'boolean',
    ];

    public function lang()
    {
        return $this->hasOne(Language::class, 'id' , 'language_id');
    }
}
