<?php

namespace App\Models;

use App\Casts\UserStatusCast;
use App\Models\Chat\ChatMember;
use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Modules\Admin\Models\Admin;
use Modules\Agent\Models\Agent;
use Modules\Agent\Models\AgentCompany;
use Modules\Client\Models\Client;
use Parental\HasChildren;

class User extends Authenticatable
{
    use HasChildren;
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    public const STATUS_UNACTIVE = "unactive";
    public const STATUS_ACTIVE = "active";
    public const STATUS_DELETED = "deleted";

    public static array $statuses = [
        self::STATUS_UNACTIVE,
        self::STATUS_ACTIVE,
        self::STATUS_DELETED,
    ];

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'type',
        'parent_id',
        'openpgp_public_key',
        'admin_permissions',
        'comment',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'status' => UserStatusCast::class,
        'email_verified_at' => 'datetime',
        'admin_permissions' => 'array',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [
        'profile_photo_url',
    ];

    protected array $childTypes = [
        'admin' => Admin::class,
        'client' => Client::class,
        'agent' => Agent::class,
    ];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(User::class, 'parent_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(User::class, 'parent_id');
    }

    public function chatMembers(): HasMany
    {
        return $this->hasMany(ChatMember::class);
    }

    public function uploads(): HasMany
    {
        return $this->hasMany(Upload::class);
    }

    public function customNotifications()
    {
        return $this->hasMany(Notification::class, 'recipient_id');
    }

    public function company(): HasOneThrough
    {
        return $this->hasOneThrough(Company::class, AgentCompany::class, 'user_id', 'id', 'id', 'company_id');
    }
}
