<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MailServer extends Model
{
    use HasFactory;

    protected $hidden = ['password_confirmation'];
    protected $fillable = [
        'ip_server',
        'port',
        'protocol',
        'password',
    ];
}
