<?php

namespace App\Models\Form;

use App\Casts\DecimalCast;
use App\Models\Company\Company;
use App\Models\Company\CompanyForm;
use App\Models\Field;
use App\Models\FormRequest\FormRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Modules\Agent\Models\Agent;
use Modules\Agent\Models\AgentCompany;
use Modules\Client\Models\Client;

class Form extends Model
{
    protected $fillable = [
        'uid',
        'user_id',
        'name',
        'price',
        'active'
    ];

    protected $casts = [
        'price' => DecimalCast::class,
        'active' => 'boolean',
    ];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'user_id');
    }

    public function fields(): BelongsToMany
    {
        return $this->belongsToMany(Field::class, FormField::class);
    }

    public function formRequests(): HasMany
    {
        return $this->hasMany(FormRequest::class);
    }

    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class, CompanyForm::class);
    }

    public function company(): HasOneThrough
    {
        return $this->hasOneThrough(Company::class, CompanyForm::class, 'form_id', 'id', 'id', 'company_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(Agent::class, AgentForm::class);
    }

    public function agents(): HasManyThrough
    {
        return $this->hasManyThrough(
            AgentCompany::class,
            CompanyForm::class,
            'form_id',
            'company_id',
            'id',
            'company_id'
        );
    }
}
