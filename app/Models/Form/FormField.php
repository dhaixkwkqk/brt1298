<?php

namespace App\Models\Form;

use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
    protected $fillable = [
        'form_id',
        'field_id',
    ];
}
