<?php

namespace App\Models\Form;

use App\Casts\DecimalCast;
use App\Models\Company\Company;
use App\Models\Company\CompanyForm;
use App\Models\Field;
use App\Models\FormRequest\FormRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Modules\Agent\Models\AgentCompany;
use Modules\Client\Models\Client;

class AgentForm extends Model
{
    protected $table = 'agents_forms';

    protected $fillable = [
        'form_id',
        'user_id',
    ];
}
