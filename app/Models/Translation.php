<?php

namespace App\Models;

use Spatie\TranslationLoader\LanguageLine;

class Translation extends LanguageLine
{
    public $timestamps = false;

    protected $table = 'translations';
}
