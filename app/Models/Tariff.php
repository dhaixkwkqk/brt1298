<?php

namespace App\Models;

use App\Casts\DecimalCast;
use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    protected $fillable = [
        'name',
        'price',
        'period',
        'users',
        'fields',
        'active',
    ];

    protected $casts = [
        'price' => DecimalCast::class,
        'created_at' => 'date:d.m.Y',
    ];

    public function companies(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Company::class);
    }
}
