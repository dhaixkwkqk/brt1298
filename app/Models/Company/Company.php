<?php

namespace App\Models\Company;

use App\Models\Form\Form;
use App\Models\FormRequest\FormRequest;
use App\Models\Tariff;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;
use Modules\Agent\Models\Agent;
use Modules\Agent\Models\AgentCompany;
use Modules\Client\Models\Client;

class Company extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'comment',
        'tariff_id',
        'logo_path',
        'active'
    ];

    protected $appends = [
        'logo_url'
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    public function logoUrl(): Attribute
    {
        return Attribute::get(function () {
            return $this->logo_path
                ? Storage::disk('public')->url($this->logo_path)
                : $this->defaultLogoUrl();
        });
    }

    protected function defaultLogoUrl(): string
    {
        $name = trim(collect(explode(' ', $this->name))->map(function ($segment) {
            return mb_substr($segment, 0, 1);
        })->join(' '));

        return 'https://ui-avatars.com/api/?name=' . urlencode($name) . '&color=7F9CF5&background=EBF4FF';
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    public function tariff(): BelongsTo
    {
        return $this->belongsTo(Tariff::class);
    }

    public function forms(): BelongsToMany
    {
        return $this->belongsToMany(Form::class, CompanyForm::class);
    }

    public function agents(): BelongsToMany
    {
        return $this->belongsToMany(Agent::class, AgentCompany::class);
    }

    public function formRequests(): HasMany
    {
        return $this->hasMany(FormRequest::class);
    }
}
