<?php

namespace App\Models\Company;

use App\Models\Form\Form;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CompanyForm extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'form_id'
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function form(): BelongsTo
    {
        return $this->belongsTo(Form::class);
    }
}
