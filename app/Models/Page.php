<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_HIDDEN = 0;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'show',
    ];
}
