<?php

namespace App\Models\Chat;

use App\Models\Upload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ChatMessage extends Model
{
    protected $fillable = [
        'chat_id',
        'chat_member_id',
        'message_encrypted',
        'viewed_at',
    ];

    protected $casts = [
        'viewed_at' => 'datetime'
    ];

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class);
    }

    public function member(): BelongsTo
    {
        return $this->belongsTo(ChatMember::class, 'chat_member_id');
    }
}
