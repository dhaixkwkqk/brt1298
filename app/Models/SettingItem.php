<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingItem extends Model
{
    protected $table = 'settings';
    
    protected $fillable = [
        'name',
        'value',
        'section',
        'position',
        'options',
    ];

    protected $casts = [
        'options' => 'json',
    ];
}
