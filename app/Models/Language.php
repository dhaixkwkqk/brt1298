<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'code',
        'description',
        'created_at',
        'updated_at',
    ];

    public function locale()
    {
        return $this->belongsTo(Locale::class);
    }
}
