<?php

namespace App\Models;

use App\Models\Element\BaseElement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property BaseElement $element
 */
class Field extends Model
{
    protected $fillable = [
        'uid',
        'label',
        'icon',
        'element_type',
        'element_id'
    ];
    protected $appends = [
        'type'
    ];

    public function getTypeAttribute(): string
    {
        return class_basename($this->element_type);
    }

    public function element(): MorphTo
    {
        return $this->morphTo();
    }
}
