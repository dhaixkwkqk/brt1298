<?php

namespace App\Models\FormRequest;

use App\Casts\FormRequestStatusCast;
use App\Models\User;
use Modules\Agent\Models\Agent;
use App\Models\Chat\Chat;
use App\Models\Company\Company;
use App\Models\Field;
use App\Models\Form\Form;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class FormRequest extends Model
{
    public const STATUS_ACTIVE = "active";
    public const STATUS_LISTED = "listed";
    public const STATUS_CLOSED = "closed";
    public const STATUS_DELETED = "deleted";
    public const STATUS_ISSUE = "issue";

    public static array $statuses = [
        self::STATUS_ACTIVE,
        self::STATUS_LISTED,
        self::STATUS_CLOSED,
        self::STATUS_DELETED,
        self::STATUS_ISSUE
    ];

    protected $fillable = [
        'uid',
        'agent_id',
        'user_id',
        'company_id',
        'form_id',
        'chat_id',
        'fields_encrypted',
        'status',
        'price',
    ];

    protected $casts = [
        'status' => FormRequestStatusCast::class,
        'created_at' => 'datetime:d.m.Y h:i:s',
    ];

    protected $appends = [
        'status_id'
    ];

    public function getStatusIdAttribute(): int
    {
        return array_search($this->status, FormRequest::$statuses);
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function form(): BelongsTo
    {
        return $this->belongsTo(Form::class);
    }

    public function agent(): BelongsTo
    {
        return $this->belongsTo(Agent::class, 'user_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class);
    }
}
