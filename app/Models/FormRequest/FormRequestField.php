<?php

namespace App\Models\FormRequest;

use App\Models\Field;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class FormRequestField extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'form_request_id',
        'field_id',
        'value'
    ];

    protected $casts = [
        'value' => 'json'
    ];

    public function formRequest(): HasOne
    {
        return $this->hasOne(FormRequest::class);
    }

    public function field(): HasOne
    {
        return $this->hasOne(Field::class);
    }
}
