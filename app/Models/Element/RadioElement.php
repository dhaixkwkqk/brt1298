<?php

namespace App\Models\Element;

class RadioElement extends BaseElement
{
    protected $fillable = [
        'variants'
    ];

    protected $casts = [
        'variants' => 'json'
    ];
}
