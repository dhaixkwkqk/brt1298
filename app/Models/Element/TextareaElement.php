<?php

namespace App\Models\Element;

use App\Services\Element\Validators\TextareaValidator;

class TextareaElement extends BaseElement
{
    protected $fillable = [
        'min_length',
        'max_length',
        'placeholder'
    ];

    protected $casts = [
        'min_length' => 'integer',
        'max_length' => 'integer',
    ];

    public function getValidator(): string
    {
        return TextareaValidator::class;
    }
}
