<?php

namespace App\Models\Element;

use App\Services\Element\Validators\InputValidator;

class InputElement extends BaseElement
{
    protected $fillable = [
        'min_length',
        'max_length',
        'pattern',
        'value_type',
        'placeholder',
    ];

    protected $casts = [
        'min_length' => 'integer',
        'max_length' => 'integer'
    ];

    public function getValidator(): string
    {
        return InputValidator::class;
    }
}
