<?php

namespace App\Models\Element;

use App\Services\Element\Validators\ImageValidator;

class ImageElement extends BaseElement
{
    public $timestamps = false;

    protected $fillable = [
        'min_count',
        'max_count',
        'max_file_size',
    ];

    protected $casts = [
        'min_count' => 'integer',
        'max_count' => 'integer',
        'max_file_size' => 'integer',
    ];

    public function getValidator(): string
    {
        return ImageValidator::class;
    }
}
