<?php

namespace App\Models\Element;


class CheckboxElement extends BaseElement
{
    public static function getType(): string
    {
        return 'checkbox';
    }

    protected $fillable = [
        'variants'
    ];

    protected $casts = [
        'variants' => 'json'
    ];
}
