<?php

namespace App\Models\Element;

use App\Models\Field;
use App\Services\Element\BaseValidator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

abstract class BaseElement extends Model
{
    public $timestamps = false;

    public function field(): MorphOne
    {
        return $this->morphOne(Field::class, 'element');
    }

    public function toArray(): array
    {
        $array = parent::toArray();
        unset($array['id']);
        return $array;
    }

    public function getValidator(): string
    {
        return BaseValidator::class;
    }
}
