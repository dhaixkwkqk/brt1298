<?php

namespace App\Models\Element;


class SelectElement extends BaseElement
{
    protected $fillable = [
        'variants'
    ];

    protected $casts = [
        'variants' => 'json'
    ];
}
