<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use OpenPGP\OpenPGP;

class PGPMessageRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        try {
            OpenPGP::readEncryptedMessage($value);
        }
        catch(\Exception $e) {
            $fail("The {$attribute} is not valid PGP Message: {$e->getMessage()}.");
        }
    }
}
