<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Igoshev\Captcha\Facades\Captcha;
use Illuminate\Support\Facades\Log;

class ValidateCaptcha implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $captchaValidate = Captcha::validate($value);
        if(!$captchaValidate){
            $fail('You entered the wrong :attribute value');
        }
    }
}
