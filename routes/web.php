<?php

use App\Http\Controllers\MailSettingController;
use App\Http\Controllers\TelegramSettingController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Laravel\Jetstream\Http\Controllers\Inertia\ApiTokenController;
use Laravel\Jetstream\Http\Controllers\Inertia\CurrentUserController;
use Laravel\Jetstream\Http\Controllers\Inertia\OtherBrowserSessionsController;
use Laravel\Jetstream\Http\Controllers\Inertia\ProfilePhotoController;
use Laravel\Jetstream\Http\Controllers\Inertia\UserProfileController;
use Laravel\Jetstream\Jetstream;
use Modules\Client\Http\Controllers\ProfileController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('welcome');

Route::get('/captcha/new' , function (){
    return response()->json([
        'captcha' => base64_encode(Igoshev\Captcha\Facades\Captcha::getImage())
    ], 200);
})->name('captcha.new');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/home', fn() => new \App\Http\Responses\LoginResponse())->name('home');

    Route::get('/dashboard', fn() => new \App\Http\Responses\LoginResponse())->name('dashboard');

    Route::get('/notification/read-all', [\App\Http\Controllers\NotificationController::class, 'readAll'])
        ->name('notification.read-all');
    Route::get('/notification/delete/{id}', [\App\Http\Controllers\NotificationController::class, 'delete'])
        ->name('notification.delete');
});

Route::group(['middleware' => config('jetstream.middleware', ['web'])], function () {
    $authMiddleware = config('jetstream.guard')
        ? 'auth:'.config('jetstream.guard')
        : 'auth';

    $authSessionMiddleware = config('jetstream.auth_session', false)
        ? config('jetstream.auth_session')
        : null;

    Route::group(['middleware' => array_values(array_filter([$authMiddleware, $authSessionMiddleware]))], function () {
//        Mail server settings
        Route::get('/settings/mail', [MailSettingController::class, 'index'])->name('settings.mail');
        Route::patch('/settings/mail', [MailSettingController::class, 'update'])->name('settings.mail.update');

        Route::get('/settings/telegram', [TelegramSettingController::class, 'index'])->name('settings.telegram');
        Route::patch('/settings/telegram', [TelegramSettingController::class, 'update'])->name('settings.telegram.update');
        // User & Profile...
        Route::get('/settings/profile', [UserProfileController::class, 'show'])
            ->name('profile.show');


        Route::delete('/settings/other-browser-sessions', [OtherBrowserSessionsController::class, 'destroy'])
            ->name('other-browser-sessions.destroy');

        Route::delete('/settings/profile-photo', [ProfilePhotoController::class, 'destroy'])
            ->name('current-user-photo.destroy');

        if (Jetstream::hasAccountDeletionFeatures()) {
            Route::delete('/settings', [CurrentUserController::class, 'destroy'])
                ->name('current-user.destroy');
        }

        Route::group(['middleware' => 'verified'], function () {
            // API...
            if (Jetstream::hasApiFeatures()) {
                Route::get('/settings/api-tokens', [ApiTokenController::class, 'index'])->name('api-tokens.index');
                Route::post('/settings/api-tokens', [ApiTokenController::class, 'store'])->name('api-tokens.store');
                Route::put('/settings/api-tokens/{token}', [ApiTokenController::class, 'update'])->name('api-tokens.update');
                Route::delete('/settings/api-tokens/{token}', [ApiTokenController::class, 'destroy'])->name('api-tokens.destroy');
            }
        });
    });
});
