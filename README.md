# B2B-Request - Личный Кабинет + API

### Требования к серверу
* PHP 8.2 + Decimal extension
* MySQL (MariaDB)
* Redis

### Основные команды для запуска 
* `php artisan key:generate`
* `php artisan migrate`
* `php artisan storage:link`
* `php artisan schedule:work`

### Запуск в локальной среде
* `./vendor/bin/sail up` - запуск Docker'а
* Все команды к artisan через `./vendor/bin/sail artisan command` - выполнение любой команды Artisan внутри контейнера Docker

### Запуск в локальной среде с подключением к рабочей базе
```bash
ssh -t -L 3306:127.0.0.1:3306 -L 6379:127.0.0.1:6379 user@83.97.20.41 -p 44413  "cd web/b2brqst.com/public_html; exec bash"
```

```bash
php artisan serve
```

## Описание моделей
1. **Client** - пользователь Личного Кабинета (Web)
2. **Agent** - пользователь мобильного приложения
3. **Admin** - пользователь Админ-панели
4. **Tariff** - тарифные планы
5. **Company** - компании
6. **Form** - форма
7. **FormFields** - поля форма
8. **CompanyForm** - связи компаний с формами
9. **FormRequest** - запросы
10. **AgentCompany** - связи пользователей мобильного приложения с компаниями

### Команды выполняемые при PUSH'е в репозиторий:
```
composer install
php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan migrate
sudo supervisorctl reload

npm install
npm run build
```

### Helper
* `Спулить изменения с телеграм бота ` php artisan telebot:polling
  Дока телебота https://westacks.github.io/telebot/#/?id=telebot
* 

### Планировщик задач
#### В Production
```
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

#### На локальной машине
```
sail artisan schedule:work
```

### Требует рефакторинга
1. В сущности ImageElement хранение связей с Uploads через отдельную таблицу.

### Документация по API
Доступна в виде POSTMAN коллекции, уже сконфигурирована,  файл `API.postman_collection.json`.

### OpenPGP
Пароль от тестовых ключей: `123456`
