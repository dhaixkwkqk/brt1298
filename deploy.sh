#!/bin/sh

set -e
whoami

force='false'

while getopts 'fp:' flag; do
  case "${flag}" in
    f) force='true' ;;
  esac
done

DOCKER_PATH=/usr/bin/docker


if [ ! -f gitlab-prev-commit.tmp.txt ] ; then
  currentCommit=`git rev-parse --short HEAD`
  echo $currentCommit > gitlab-prev-commit.tmp.txt
fi

prevCommit=`cat gitlab-prev-commit.tmp.txt`
echo "prevCommit => $prevCommit"

git pull origin main
diff=`git --no-pager diff --name-only ${prevCommit}`

echo "$diff"

#if $(echo "$diff" | grep -E -i -q '^composer.*') || [ $force = "true" ] ; then
    $DOCKER_PATH compose exec laravel.test composer install --no-interaction --no-dev --prefer-dist
#fi

$DOCKER_PATH compose exec laravel.test  php artisan storage:link || true

if $(echo "$diff" | grep -E -i -q '^database/migrations') || [ $force = "true" ] ; then
    $DOCKER_PATH compose exec laravel.test  php artisan migrate --force
fi

if $(echo "$diff" | grep -E -i -q '^database/seeders') || [ $force = "true" ] ; then
    $DOCKER_PATH compose exec laravel.test php artisan db:seed --force
fi

$DOCKER_PATH compose exec laravel.test php artisan config:clear
$DOCKER_PATH compose exec laravel.test php artisan queue:restart

if $(echo "$diff" | grep -E -i -q '^package.json') || [ $force = "true" ] ; then
    $DOCKER_PATH compose exec -T node sh -c "npm ci && npm run prod"
elif $(echo "$diff" | grep -E -i -q '^resources/(js|sass)') ; then
    $DOCKER_PATH compose exec -T node sh -c "npm run prod"
fi

currentCommit=`git rev-parse --short HEAD`
echo $currentCommit > gitlab-prev-commit.tmp.txt
