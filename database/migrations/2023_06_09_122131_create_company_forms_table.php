<?php

use App\Models\Company\Company;
use App\Models\Form\Form;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('company_forms', function (Blueprint $table) {
            $table->foreignIdFor(Company::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Form::class)->constrained()->cascadeOnDelete();

            $table->unique(['company_id', 'form_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('company_forms');
    }
};
