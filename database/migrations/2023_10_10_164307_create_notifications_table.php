<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\User::class, 'recipient_id')
                ->constrained('users')
                ->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\NotificationMessage::class, 'message_id')
                ->constrained('notification_messages')
                ->cascadeOnDelete();
            $table->timestamp('viewed_at')
                ->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
};
