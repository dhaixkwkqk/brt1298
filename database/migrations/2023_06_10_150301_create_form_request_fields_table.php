<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('form_request_fields', function (Blueprint $table) {
            $table->foreignIdFor(\App\Models\FormRequest\FormRequest::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\Field::class)->constrained()->cascadeOnDelete();
            $table->json('value')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('form_request_fields');
    }
};
