<?php

use App\Models\Tariff;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreignIdFor(Tariff::class)
                ->nullable()
                ->constrained()
                ->nullOnDelete();
            $table->string('client_id')->nullable();
            $table->string('access_key')->nullable();

            $table->unique(['client_id']);
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropConstrainedForeignIdFor(Tariff::class);
            $table->dropColumn(['client_id', 'access_key']);
        });
    }
};
