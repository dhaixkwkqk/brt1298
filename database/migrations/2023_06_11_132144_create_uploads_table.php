<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->uuid();
            $table->foreignIdFor(\App\Models\User::class)->constrained()->cascadeOnDelete();
            $table->string('checksum');
            $table->string('disk');
            $table->text('path');
            $table->timestamp('expired_at')->nullable();
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamps();

            $table->unique(['user_id', 'checksum']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('uploads');
    }
};
