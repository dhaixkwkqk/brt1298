<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('input_elements', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('min_length')->nullable();
            $table->unsignedInteger('max_length')->nullable();
            $table->string('pattern')->nullable();
            $table->enum('value_type', ['text', 'password'])->nullable();
            $table->string('placeholder')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('input_elements');
    }
};
