<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('form_fields', function (Blueprint $table) {
            $table->foreignIdFor(\App\Models\Form\Form::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\Field::class)->constrained()->cascadeOnDelete();
            $table->timestamps();

            $table->unique(['form_id', 'field_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('form_fields');
    }
};
