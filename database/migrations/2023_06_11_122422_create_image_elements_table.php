<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('image_elements', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('min_count')->nullable();
            $table->unsignedTinyInteger('max_count')->nullable();
            $table->unsignedInteger('max_file_size')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('image_elements');
    }
};
