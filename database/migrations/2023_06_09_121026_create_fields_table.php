<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->unique();
            $table->string('label');
            $table->string('icon')->nullable();
            $table->morphs('element');
            $table->timestamps();

        });
    }

    public function down(): void
    {
        Schema::dropIfExists('fields');
    }
};
