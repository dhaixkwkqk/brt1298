<?php

use App\Models\Chat\Chat;
use App\Models\Company\Company;
use App\Models\Form\Form;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Agent\Models\Agent;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('form_requests', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->unique();
            $table->foreignIdFor(Agent::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Company::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Form::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Chat::class)->constrained()->cascadeOnDelete();
            $table->longText('fields_encrypted');
            $table->unsignedTinyInteger('status')->default(0)->index();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('form_requests');
    }
};
