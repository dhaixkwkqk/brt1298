<?php

namespace Database\Seeders;

use App\Models\Language;
use App\Models\MailServer;
use App\Models\TelegramBot;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TelegramBotSeeder extends Seeder
{

    protected  $telegramSettings = [
        "telegram_token" => "your telegram  token",
        "telegram_bot_name" => "your telegram name",
    ];
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $exist = TelegramBot::query()->firstWhere('telegram_token', $this->telegramSettings['telegram_token']);
        if(!$exist){
            TelegramBot::create($this->telegramSettings);
        }
    }
}
