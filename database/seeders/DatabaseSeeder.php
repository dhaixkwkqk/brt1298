<?php

namespace Database\Seeders;

use App\Models\Chat\Chat;
use App\Models\Element\CheckboxElement;
use App\Models\Element\ImageElement;
use App\Models\Element\InputElement;
use App\Models\Field;
use App\Models\Locale;
use App\Models\SettingItem;
use App\Models\Tariff;
use App\Models\Upload;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Admin\Models\Admin;
use Modules\Agent\Actions\Upload\UploadFile;
use Modules\Agent\Http\Requests\Upload\UploadRequest;
use Modules\Agent\Models\Agent;
use Modules\Client\Models\Client;
use OpenPGP\OpenPGP;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            LanguagesSeeder::class,
            BaseSeeder::class,
            MailServerSeeder::class,
            TelegramBotSeeder::class,
        ]);



    }
}
