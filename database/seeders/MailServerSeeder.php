<?php

namespace Database\Seeders;

use App\Models\Language;
use App\Models\MailServer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MailServerSeeder extends Seeder
{

    protected  $baseServer = [
        "ip_server" => "127.0.0.1",
        "port" => "25",
        "protocol" => "SMTP",
        "password" => "pass",
    ];
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $exist = MailServer::query()->firstWhere('ip_server', $this->baseServer['ip_server']);
        if(!$exist){
            MailServer::create($this->baseServer);
        }
    }
}
