<?php

namespace Database\Seeders;

use App\Models\Chat\Chat;
use App\Models\Element\CheckboxElement;
use App\Models\Element\ImageElement;
use App\Models\Element\InputElement;
use App\Models\Field;
use App\Models\Locale;
use App\Models\SettingItem;
use App\Models\Tariff;
use App\Models\Upload;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Admin\Models\Admin;
use Modules\Agent\Actions\Upload\UploadFile;
use Modules\Agent\Http\Requests\Upload\UploadRequest;
use Modules\Agent\Models\Agent;
use Modules\Client\Models\Client;
use OpenPGP\OpenPGP;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        SettingItem::upsert([
            [
                'name' => 'agent_technical_work',
                'value' => '0',
                'section' => 'agent',
                'position' => 1,
                'options' => json_encode([
                    'title' => 'Technical work in the Agency application',
                    'type' => 'boolean',
                ])
            ],
            [
                'name' => 'agent_technical_work_message',
                'value' => null,
                'section' => 'agent',
                'position' => 2,
                'options' => json_encode([
                    'title' => 'The text of technical works in the Agency application',
                    'type' => 'textarea',
                    'min' => 0,
                    'max' => 1024
                ])
            ],
        ], ['name'], ['section', 'position', 'options']);
        $admin = Admin::updateOrCreate([
            'email' => 'admin@admin.com'
        ], [
            'username' => 'admin',
            'password' => Hash::make('admin'),
            'openpgp_public_key' => File::get(resource_path('openpgp/admin.pub')),
            'admin_permissions' =>  [
                "admins" , "clients", "tariffs", "locales" , "translations", "pages" , "notifications", "customer requests", "settings"
            ]
        ]);

        $tariff = Tariff::updateOrCreate([
            'name' => 'Тестовый',
        ], [
            'price' => 20,
            'period' => 30,
            'users' => 5,
            'fields' => 10,
        ]);

        $client = Client::updateOrCreate([
            'username' => 'client',
        ], [
            'client_id' => 'Client',
            'tariff_id' => 1,
            'access_key' => Hash::make('client'),
            'openpgp_public_key' => File::get(resource_path('openpgp/client.pub')),
        ]);
        $client->parent()->associate($admin)->save();

        $agent = Agent::updateOrCreate([
            'username' => 'agent',
        ], [
            'name' => 'Agent',
            'password' => Hash::make('agent'),
            'openpgp_public_key' => File::get(resource_path('openpgp/agent.pub')),
        ]);
        $agent->parent()->associate($client)->save();


        $company = $client->companies()->updateOrCreate([
            'name' => 'Моя компания ОСНОВНАЯ',
            'comment' => 'Короткое описание компании',
            'tariff_id' => $tariff->id,
        ]);

        $company->agents()->attach($agent->id);

        $inputField = new InputElement([
            'min_length' => 1,
            'max_length' => 30,
        ]);
        $inputField->save();

        $firstName = new Field([
            'uid' => 'first-name',
            'label' => 'Имя',
        ]);
        $firstName->element()->associate($inputField)->save();

        $secondName = new Field([
            'uid' => 'second-name',
            'label' => 'Фамилия',
        ]);
        $secondName->element()->associate($inputField)->save();


        $checkboxElement = new CheckboxElement([
            'variants' => ['Мужчина', 'Женщина']
        ]);
        $checkboxElement->save();

        $sex = new Field([
            'uid' => 'sex',
            'label' => 'Пол',
        ]);
        $sex->element()->associate($checkboxElement)->save();


        $imageElement = new ImageElement([
            'min_count' => 1,
            'max_count' => 1,
            'max_file_size' => 10240,
        ]);
        $imageElement->save();

        $image = new Field([
            'uid' => 'image',
            'label' => 'Фото',
        ]);
        $image->element()->associate($imageElement)->save();

        $form = $client->forms()->updateOrCreate([
            'uid' => 'F000001',
            'name' => 'Моя форма',
            'price' => 100,
        ]);

        $form->fields()->syncWithoutDetaching([
            $firstName->id,
            $secondName->id,
            $sex->id,
            $image->id,
        ]);

        $company->forms()->syncWithoutDetaching([$form->id]);


        $chat = Chat::create([
            'uid' => Str::uuid()->toString(),
        ]);
        $client->chatMembers()->create(['chat_id' => $chat->id]);
        $agent->chatMembers()->create(['chat_id' => $chat->id]);

        $agentPublicKey = File::get(resource_path('openpgp/agent.pub'));
        $agentPublicKey = OpenPGP::readPublicKey($agentPublicKey);
        $clientPublicKey = File::get(resource_path('openpgp/client.pub'));
        $clientPublicKey = OpenPGP::readPublicKey($clientPublicKey);

        $upload = Upload::create([
            'user_id' => $agent->id,
            'checksum' => '8dbbc04807134604521bdd728c9efc4da16e9e66285628607c1e73e233999dc0',
            'disk' => 'public',
            'path' => 'test.pgp',
            'status' => 1,
        ]);

        File::copy(resource_path('openpgp/testing.jpeg.pgp.gpg'), storage_path('app/public/test.pgp'));

        $fields = [
            'first-name' => 'Василий',
            'second-name' => 'Пупкин',
            'sex' => 'Мужчина',
            'image' => [
                $upload->uuid,
            ],
        ];
        $fieldsEncrypted = (string)OpenPGP::encrypt(
            OpenPGP::createLiteralMessage(json_encode($fields)),
            [$agentPublicKey, $clientPublicKey]
        );

        $agent->formRequests()->updateOrCreate([
            'uid' => 'F'.mt_rand(10000, 9999999),
            'form_id' => $form->id,
            'company_id' => $company->id,
            'chat_id' => $chat->id,
            'fields_encrypted' => $fieldsEncrypted
        ]);

        Locale::create([
            'language_id' => 26,
            'is_default' => true
        ]);

    }
}
